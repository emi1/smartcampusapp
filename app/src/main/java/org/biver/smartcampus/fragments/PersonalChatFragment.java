package org.biver.smartcampus.fragments;

import android.app.Activity;
import android.app.Fragment;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;

import com.google.gson.Gson;
import com.melnykov.fab.FloatingActionButton;

import org.biver.smartcampus.R;
import org.biver.smartcampus.activities.MainActivity;
import org.biver.smartcampus.core.MyNotificationManager;
import org.biver.smartcampus.core.SmartSpaceProvider;
import org.biver.smartcampus.services.chat.ChatHistory;
import org.biver.smartcampus.services.chat.PersonalChatAdaper;
import org.biver.smartcampus.services.chat.ChatMessage;
import org.biver.smartcampus.services.chat.MyXMPP;
import org.biver.smartcampus.utils.ALogger;
import org.biver.smartcampus.activities.ActivityManager;
import org.biver.smartcampus.utils.Config;
import org.biver.smartcampus.utils.FullJidResponseListener;
import org.biver.smartcampus.utils.Preferences;
import org.biver.smartcampus.utils.Toaster;
import org.apache.log4j.Logger;
import org.jivesoftware.smackx.muc.Occupant;
import org.jxmpp.jid.EntityFullJid;
import org.jxmpp.jid.Jid;
import org.jxmpp.jid.impl.JidCreate;
import org.jxmpp.stringprep.XmppStringprepException;

import java.util.Random;

/**
 * Created by emi on 7/28/16.
 */
public class PersonalChatFragment extends Fragment implements View.OnClickListener{

    private boolean visible;

    private Logger log;
    private Activity mActivity;

    private EditText editTextInputMessage;
    private Random random;
    private ListView msgListView;
    private PersonalChatAdaper adaper;

    private EntityFullJid receiverJid;
    private String receiverNick;

    /*
    private MessageListener myMessgeListener = new MessageListener() {
        @Override
        public void processMessage(Message message) {
            ChatMessage chatMessage = ChatMessage.fromXmppMessage(message);
            if (receiverJid.toString().equals(ChatMessage.fromXmppMessage(message).getSenderJid())) {
                adaper.addMessage(chatMessage);
                mActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        adaper.notifyDataSetChanged();
                    }
                });
            }
        }
    }
    */

    private static PersonalChatFragment INSTANCE;

    public static PersonalChatFragment getINSTANCE() {
        return INSTANCE;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        log = ALogger.getLogger(this.getActivity(), PersonalChatFragment.class);
        this.mActivity = getActivity();
        INSTANCE = this;
        log.debug("onCreate");

        receiverNick = this.getArguments().getString("receiver_nick");
        String receiverJidStr = this.getArguments().getString("receiver_jid");
        try {
            receiverJid = JidCreate.entityFullFrom(receiverJidStr);
        } catch (XmppStringprepException e) {
            log.error("Creating jid failed.", e);
        }

        //Set the actionbar title
        ((MainActivity) mActivity).setActionBarTitle(mActivity.getString(R.string.title_personal_chat_service, receiverNick), true);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        log.debug("onCreateView");

        View view = inflater.inflate(R.layout.fragment_personal_chat_layout, container, false);

        random = new Random();
        editTextInputMessage = (EditText) view.findViewById(R.id.messageEditText);
        msgListView = (ListView) view.findViewById(R.id.msgListView);
        ImageButton sendButton = (ImageButton) view.findViewById(R.id.sendMessageButton);
        sendButton.setOnClickListener(this);

        // ----Set autoscroll of listview when a new message arrives----//
        msgListView.setTranscriptMode(ListView.TRANSCRIPT_MODE_ALWAYS_SCROLL);
        msgListView.setStackFromBottom(true);

        //Setup chatadapter
        //msgListView.setAdapter(MainActivity.getINSTANCE().getChatAdapter());
        //MainActivity.getINSTANCE().getChatAdapter().notifyDataSetChanged();

        adaper = new PersonalChatAdaper(mActivity, receiverNick);
        msgListView.setAdapter(adaper);

        FloatingActionButton fab = (FloatingActionButton) view.findViewById(R.id.fab_send_attachement);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final Occupant occupant = MyXMPP.getINSTANCE().getSmartSpaceMuc().getOccupant(receiverJid);
                Jid jid = null;
                String nick = null;
                if (occupant != null) {
                    jid = occupant.getJid();
                    nick = occupant.getNick().toString();
                }

                if (occupant == null || jid == null) {
                    log.error("ERROR: jid is null for some reason!");
                    //Toaster.toast(activity, "Error...");
                    //MyXMPP.getINSTANCE().sendPersonalMessage(receiverJid, Config.XMPP_REQUESTS.REQUEST_FULL_JID);
                    MyXMPP.getINSTANCE().requestFullJid(receiverJid, new FullJidResponseListener() {
                        @Override
                        public void onFullJIdReceived(String fullJid) {
                            ((MainActivity) mActivity).setCurrentFileReceiverJidString(fullJid, receiverNick);
                            //TODO move logic elsewhere
                            ActivityManager.launchFileChooserIntent(mActivity, Config.FILE_SELECT_CODE_FILE_SHARING);
                        }
                    });
                } else {
                    ((MainActivity) mActivity).setCurrentFileReceiverJidString(jid.toString(), nick);
                    //TODO move logic elsewhere
                    ActivityManager.launchFileChooserIntent(mActivity, Config.FILE_SELECT_CODE_FILE_SHARING);
                }
            }
        });



        return view;
    }


    @Override
    public void onResume() {
        super.onResume();
        this.visible = true;

        //clear notifications from personal chat
        (new MyNotificationManager(mActivity)).clearPersonalChatNotifications();

    }

    @Override
    public void onPause() {
        super.onPause();
        this.visible = false;
    }

    public boolean visible() {
        return visible;
    }

    public PersonalChatAdaper getAdaper() {
        return adaper;
    }

    public EntityFullJid getReceiverJid() {
        return receiverJid;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.sendMessageButton: {
                final String message = editTextInputMessage.getEditableText().toString();
                this.sendPersonalTextMessage(receiverJid, message);
            }
            default:
                return;
        }
    }


    /**
     * Send a personal ChatMessage to the receiver of this personal chat
     * @param receiverJid The receiver of the message
     * @param message The text message to be send.
     */
    private void sendPersonalTextMessage(final EntityFullJid receiverJid, final String message) {
        log.debug("sendPersonalTextMessage");

        if (!message.isEmpty()) {
            editTextInputMessage.setText("");

            final ChatMessage chatMessage = new ChatMessage(Preferences.getDisplayedChatUserName(getActivity()),
                    this.receiverNick,
                    message);
            chatMessage.setBody(message);

            final String body = new Gson().toJson(chatMessage);

            boolean success = MyXMPP.getINSTANCE().sendPersonalMessage(receiverJid, body);

            //persist message to storage
            if (success) {
                //TODO WIP
                //persist message to storage
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(mActivity);

                        String activeSpaceBeaconId = SmartSpaceProvider.getActiveSmartSpace().getBeaconInstanceId();
                        String chatHistoryKey = Preferences.getChatHistoryKeyOfUserChat(chatMessage.getReceiver());
                        String historyString = prefs.getString(chatHistoryKey, "");

                        ChatHistory chatHistory = new ChatHistory();
                        if (!historyString.isEmpty()) {
                            chatHistory = new Gson().fromJson(historyString, ChatHistory.class);
                        }
                        boolean hasBeenAdded = chatHistory.add(chatMessage);

                        //Purge so that history doesn't grow indefinitely
                        chatHistory.purge();
                        //Save history to SharedPreferences
                        final String newHistory = new Gson().toJson(chatHistory);
                        prefs.edit()
                                .putString(chatHistoryKey, newHistory)
                                .apply();
                    }
                }).start();

            }

            adaper.addMessage(chatMessage);
            adaper.notifyDataSetChanged();
        }

    }

    /**
     * Check if the other end from this personal chat is still online
     * Close the PersonalChatFragment if he is not, do nothing if he still is there
     */
    public void checkIfReceiverStillOnline() {
        if(!MyXMPP.getINSTANCE().getMucParticipantsListWithoutMyself(mActivity).contains(receiverJid)) {
        //    mActivity.getFragmentManager().beginTransaction().remove(this).commit();
            mActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    ((MainActivity) mActivity).onBackPressed();
                }
            });
            Toaster.toast(mActivity, getString(R.string.user_left_hint));
        }

    }
}
