package org.biver.smartcampus.fragments;

import android.app.Activity;
import android.app.Fragment;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import org.biver.smartcampus.R;
import org.biver.smartcampus.activities.MainActivity;
import org.biver.smartcampus.core.MainAdapter;
import org.biver.smartcampus.core.SmartSpaceProvider;
import org.biver.smartcampus.utils.ALogger;
import org.apache.log4j.Logger;
import org.json.JSONException;

import java.io.IOException;

/**
 * Fragment holding the main view of the app that is displayed to user when inside of a Smart Space
 * Should display a list of available locallized services
 */
public class MainFragment extends Fragment{

    private Logger log;
    private Activity activity;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activity = this.getActivity();
        log = ALogger.getLogger(activity, MainFragment.class);
        log.debug("onCreate");
    }

    @Override
    public void onResume() {
        log.debug("onResume");
        super.onResume();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        log.debug("onCreateView");
        return inflater.inflate(R.layout.fragment_main_layout, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        log.debug("onViewCreated");

        ListView listView = (ListView) getActivity().findViewById(R.id.list_view_main_services);
        final MainAdapter mainAdapter = new MainAdapter(activity, SmartSpaceProvider.getActiveSmartSpace());

        listView.setAdapter(mainAdapter);

        mainAdapter.notifyDataSetChanged();

        ((MainActivity) activity).setActionBarTitle(activity.getString(R.string.app_name), false);

        final SwipeRefreshLayout refresher = (SwipeRefreshLayout) view.findViewById(R.id.main_fragment_swip_container);
        refresher.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                new AsyncTask<Void, Void, Void>() {
                    @Override
                    protected Void doInBackground(Void... params) {
                        try {
                            mainAdapter.updateData();
                        } catch (IOException e) {
                            log.error("IOException", e);
                        } catch (JSONException e) {
                            log.error("JSONException", e);
                        }
                        return null;
                    }

                    @Override
                    protected void onPostExecute(Void aVoid) {
                        refresher.setRefreshing(false);
                        mainAdapter.notifyDataSetChanged();

                        ((MainActivity) activity).setSmartSpaceTitle(SmartSpaceProvider.getActiveSmartSpace().getName());
                    }
                }.execute();

            }
        });

        /*
        Button buttonAddSource = (Button) view.findViewById(R.id.button_add_information_source);
        buttonAddSource.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                final View view = activity.getLayoutInflater().inflate((R.layout.dialog_add_information_source), null);

                AlertDialog.Builder builder = new AlertDialog.Builder(activity);

                builder.setView(view);
                builder.setTitle("Add information source url");

                builder.setPositiveButton(R.string.dialog_button_positive, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(final DialogInterface dialog, int which) {
                        //do nothing here, will be overwritten
                    }
                });
                builder.setNegativeButton(R.string.dialog_button_negative, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });

                final AlertDialog dialog = builder.create();

                dialog.show();
                dialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        new Thread(new Runnable() {
                            @Override
                            public void run() {

                                String url = ((EditText) view.findViewById(R.id.edit_text_add_information_source_url)).getText().toString();
                                String name = ((EditText) view.findViewById(R.id.edit_text_add_information_source_name)).getText().toString();


                                if (new UrlValidator().isValid(url) && !name.isEmpty()) {
                                    dialog.dismiss();

                                    //TODO implement real name for user generated service
                                    CampusProximityInformationService newService = new CampusProximityInformationService(url, name, true);

                                    try {
                                        new ServerRequests(activity).addInformationSource(
                                                SmartSpaceProvider.getActiveSmartSpace().getBeaconInstanceId(),
                                                newService,
                                                new Response.Listener<String>() {
                                                    @Override
                                                    public void onResponse(String response) {
                                                        //For some reason onResponse is executed in main thread, start separted thread for updating
                                                        new Thread(new Runnable() {
                                                            @Override
                                                            public void run() {
                                                                try {
                                                                    mainAdapter.updateData();
                                                                    //go back to main thread to update ui
                                                                    activity.runOnUiThread(new Runnable() {
                                                                        @Override
                                                                        public void run() {
                                                                            mainAdapter.notifyDataSetChanged();
                                                                        }
                                                                    });
                                                                } catch (IOException e) {
                                                                    log.error("IOException", e);
                                                                } catch (JSONException e) {
                                                                    log.error("JSONException", e);
                                                                }
                                                            }
                                                        }).start();
                                                    }
                                                });
                                    } catch (JSONException e) {
                                        log.error("JSONException", e);
                                    }

                                } else {
                                    Toaster.toast(activity, "Invalid url");
                                }



                            }
                        }).start();
                    }
                });
            }
        });
        */
        
    }


}
