package org.biver.smartcampus.fragments;

/**
 * Created by emi on 6/7/16.
 */

import android.app.Activity;
import android.app.Fragment;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.Nullable;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import org.biver.smartcampus.R;
import org.biver.smartcampus.activities.MainActivity;
import org.biver.smartcampus.core.MyNotificationManager;
import org.biver.smartcampus.core.SmartSpaceProvider;
import org.biver.smartcampus.services.chat.ChatAdapter;
import org.biver.smartcampus.services.chat.ChatMessage;
import org.biver.smartcampus.services.chat.MyXMPP;
import org.biver.smartcampus.services.chat.UserNamersSpinnerAdapter;
import org.biver.smartcampus.userprofiles.DisplayUserProfileAsyncTask;
import org.biver.smartcampus.utils.ALogger;
import org.biver.smartcampus.utils.FullJidResponseListener;
import org.biver.smartcampus.utils.Preferences;
import org.biver.smartcampus.utils.Toaster;
import org.biver.smartcampus.utils.Utils;
import org.apache.log4j.Logger;
import org.jivesoftware.smack.MessageListener;
import org.jivesoftware.smack.PresenceListener;
import org.jivesoftware.smack.packet.Message;
import org.jivesoftware.smack.packet.Presence;
import org.jivesoftware.smackx.muc.MultiUserChat;
import org.jivesoftware.smackx.muc.Occupant;
import org.jxmpp.jid.EntityFullJid;

import java.util.ArrayList;
import java.util.Random;

import me.srodrigo.androidhintspinner.HintAdapter;
import me.srodrigo.androidhintspinner.HintSpinner;


/**
 * Fragment for the group chat via xmpp
 */
public class ChatFragment extends Fragment implements OnClickListener {

    private static Logger log;


    private EditText editTextInputMessage;
    private Random random;
    private ListView msgListView;

    private static boolean visible;
    private Activity mActivity;

    private PresenceListener myPresenceListener = new PresenceListener() {
        @Override
        public void processPresence(Presence presence) {
            if (ChatFragment.visible()) {
                ChatFragment.this.updateCurrentUsersDisplay();
            }
        }
    };

    private ChatAdapter chatAdapter;

    private String mucRoomName;

    private HintSpinner<Pair<EntityFullJid, String>> hintSpinner;

    private MessageListener myMessageListener = new MessageListener() {
        @Override
        public void processMessage(Message message) {
            if (message.getType() == Message.Type.groupchat && message.getBody() != null) {
                final ChatMessage chatMessage = ChatMessage.fromXmppMessage(message);
                chatAdapter.addChatMessage(chatMessage);
                mActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        chatAdapter.notifyDataSetChanged();
                    }
                });
            }
        }
    };

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        log = ALogger.getLogger(this.getActivity(), ChatFragment.class);
        this.mActivity = getActivity();
        log.debug("onCreate");

        chatAdapter = new ChatAdapter(mActivity, SmartSpaceProvider.getActiveSmartSpace().getBeaconInstanceId());


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        log.debug("onCreateView");

        View view = inflater.inflate(R.layout.fragment_chat_layout, container, false);

        random = new Random();
        editTextInputMessage = (EditText) view.findViewById(R.id.messageEditText);
        msgListView = (ListView) view.findViewById(R.id.msgListView);
        ImageButton sendButton = (ImageButton) view.findViewById(R.id.sendMessageButton);
        sendButton.setOnClickListener(this);

        // ----Set autoscroll of listview when a new message arrives----//
        msgListView.setTranscriptMode(ListView.TRANSCRIPT_MODE_ALWAYS_SCROLL);
        msgListView.setStackFromBottom(true);

        //Setup chatadapter
        //msgListView.setAdapter(MainActivity.getINSTANCE().getChatAdapter());
        //MainActivity.getINSTANCE().getChatAdapter().notifyDataSetChanged();

        msgListView.setAdapter(chatAdapter);
        chatAdapter.notifyDataSetChanged();

        //Set the actionbar title
        ((MainActivity) mActivity).setActionBarTitle(mActivity.getString(R.string.title_chat_service), true);
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        this.updateCurrentUsersDisplay();

        MyXMPP.getINSTANCE().getSmartSpaceMuc().addMessageListener(myMessageListener);

        MyXMPP.getINSTANCE().getSmartSpaceMuc().addParticipantListener(myPresenceListener);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (MyXMPP.getINSTANCE() != null && MyXMPP.getINSTANCE().getSmartSpaceMuc() != null) {
            MyXMPP.getINSTANCE().getSmartSpaceMuc().removeMessageListener(myMessageListener);
            MyXMPP.getINSTANCE().getSmartSpaceMuc().removeParticipantListener(myPresenceListener);
        }
    }

    /**
     * Update the list of currently available users in top of the fragment
     */
    public void updateCurrentUsersDisplay() {
        mActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (getView() == null) {
                    log.error("Tried to update users list but view is null");
                    return;
                }


                MultiUserChat muc = MyXMPP.getINSTANCE().getSmartSpaceMuc();
                ArrayList<EntityFullJid> users = new ArrayList<>(muc.getOccupants());

                //remove own name since getOccupants includes your own username
                EntityFullJid toRemove = null;
                for (EntityFullJid jid : users) {
                    if (muc.getOccupant(jid).getNick().toString().contains(Preferences.getDisplayedChatUserName(mActivity))) {
                        toRemove = jid;
                    }
                }
                users.remove(toRemove);


                ArrayList<Pair<EntityFullJid, String>> jidNickPairs = new ArrayList<>();
                for (EntityFullJid jid : users) {
                    final Occupant occupant = MyXMPP.getINSTANCE().getSmartSpaceMuc().getOccupant(jid);
                    final String nick = occupant.getNick().toString();
                    jidNickPairs.add(new Pair<>(jid, nick));
                }
                final Spinner spinner = (Spinner) getView().findViewById(R.id.available_users_spinner);

                spinner.setAdapter(new UserNamersSpinnerAdapter(mActivity, jidNickPairs));


                HintAdapter<Pair<EntityFullJid, String>> hintAdapter = new HintAdapter<Pair<EntityFullJid, String>>(
                        mActivity,
                        R.layout.user_names_spinner_item,
                        "Select user",
                        jidNickPairs) {

                    @Override
                    protected View getCustomView(int position, View convertView, ViewGroup parent) {
                        final Pair<EntityFullJid, String> jidNickPair = getItem(position);
                        final String nick = jidNickPair.second;
                        final EntityFullJid jid = jidNickPair.first;

                        // Here you inflate the layout and set the value of your widgets
                        View view = inflateLayout(parent, false);
                        ((TextView) view.findViewById(R.id.text_view_user_name_in_spinner)).setText(nick);
                        return view;
                    }
                };

                hintSpinner = new HintSpinner<>(
                        spinner,
                        hintAdapter,
                        new HintSpinner.Callback<Pair<EntityFullJid, String>>() {
                            @Override
                            public void onItemSelected(int position, Pair<EntityFullJid, String> itemAtPosition) {
                                final EntityFullJid entityFullJid = itemAtPosition.first;
                                final String fullJidStr = itemAtPosition.first.toString();
                                final String nick = itemAtPosition.second;
                                //TODO user clicked on a user on the list, what to do? Currently open a PersonalChatFragment
                                //Utils.launchPersonalChatFragment(mActivity, fullJid, nick);
                                //Toaster.toastShort(mActivity, "Joined personal chat with: " + nick);
                                //new DisplayUserProfileAsyncTask(mActivity, fullJidStr, nick).execute();

                                //request the full jid because array only holds jid from muc
                                new Thread(new Runnable() {
                                    @Override
                                    public void run() {
                                        MyXMPP.getINSTANCE().requestFullJid(entityFullJid, new FullJidResponseListener() {
                                            @Override
                                            public void onFullJIdReceived(String fullJidReceived) {
                                                new DisplayUserProfileAsyncTask(mActivity, fullJidReceived, nick, true).execute();
                                            }
                                        });
                                    }
                                }).start();
                                 hintSpinner.selectHint();

                            }
                        });

                hintSpinner.init();

                /*
                //Make ugly empty spinner disappear if there are no users
                if (users.isEmpty()) {
                    spinner.setVisibility(View.GONE);
                    ((TextView) mActivity.findViewById(R.id.text_view_no_users_available)).setVisibility(View.VISIBLE);
                } else {
                    ((TextView) mActivity.findViewById(R.id.text_view_no_users_available)).setVisibility(View.GONE);
                }
                */
            }
        });

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.sendMessageButton: {
                final String message = editTextInputMessage.getEditableText().toString();
                this.sendTextMessage(message);
                break;
            }
            default:
                return;
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        log.debug("onResume");

        visible = true;

        //Clear spinner and reset values because spinner automatically sets selection and then we end up in loop of launching PersonalChatFragment from spinner.onItemSelected
        hintSpinner = null;
        this.updateCurrentUsersDisplay();

        //Clear message notifications when in ChatFragment
        (new MyNotificationManager(this.getActivity())).clearGroupChatMessageNotifications();

        //MainActivity.getINSTANCE().getChatAdapter().notifyDataSetChanged();
        chatAdapter.notifyDataSetChanged();
    }

    @Override
    public void onPause() {
        super.onPause();
        visible = false;
    }

    /**
     * Return true iff this framgent is visible to the user
     */
    public static boolean visible() {
        return visible;
    }


    /**
     * Called when user sends a message via clicking on the send button in user interface
     * @param
     */
    public void sendTextMessage(final String message) {
        log.debug("sendTextMessage");
        if (!message.equalsIgnoreCase("")) {

            final ChatMessage chatMessage = new ChatMessage(
                    Preferences.getDisplayedChatUserName(getActivity()),
                    SmartSpaceProvider.getActiveSmartSpace().getIdentifier(),
                    message);
            chatMessage.setBody(message);
            //chatMessage.setDate(ChatUtils.getCurrentDate());
            //chatMessage.setTime(ChatUtils.getCurrentTime());

            //update is not needed in group chat because we also receive this message via xmpp where the adapter is notified about update
            //for private messages adapter might be updated here
            //chatAdapter.addChatMessage(chatMessage);
            //chatAdapter.notifyDataSetChanged();



            new AsyncTask<Void, Void, Void>() {
                private boolean success = false;
                @Override
                protected Void doInBackground(Void... params) {
                    MyXMPP myXMPP = MyXMPP.getINSTANCE();
                    success = myXMPP.sendMucMessage(myXMPP.getSmartSpaceMuc(), chatMessage);
                    return null;
                }

                @Override
                protected void onPostExecute(Void aVoid) {
                    if (success) {
                        editTextInputMessage.setText("");
                    }
                }
            }.execute();
        }
    }


    /**
     * Reset the text that the user wrote into edittext if the sending of messge was not successful
     * @param message Text to be set inside EditText
     */
    public void setTextInEditText(final String message) {
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                editTextInputMessage.setText(message);
            }
        });
    }

}

