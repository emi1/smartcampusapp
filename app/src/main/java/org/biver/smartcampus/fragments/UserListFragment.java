package org.biver.smartcampus.fragments;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import org.biver.smartcampus.R;
import org.biver.smartcampus.activities.MainActivity;
import org.biver.smartcampus.services.filesharing.UserListAdapter;
import org.biver.smartcampus.utils.ALogger;
import org.apache.log4j.Logger;

/**
 * Created by emi on 6/13/16.
 */
public class UserListFragment extends Fragment {

    private Logger log;

    private ListView userNamesListView;

    private Activity mActivity;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        log = ALogger.getLogger(this.getActivity(), UserListFragment.class);
        log.debug("onCreate");
        this.mActivity = getActivity();

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        log.debug("onCreateView");

        View view = inflater.inflate(R.layout.fragment_file_sharing, container, false);

        userNamesListView = (ListView) view.findViewById(R.id.list_view_file_sharing_names);

        UserListAdapter adapter = new UserListAdapter(this.getActivity());
        userNamesListView.setAdapter(adapter);

        //Setup swipt to refresh
        final SwipeRefreshLayout swipeContainer = (SwipeRefreshLayout) view.findViewById(R.id.swipe_container_file_sending);
        // Setup refresh listener which triggers new data loading
        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                //Your code to refresh the list here.
                //Make sure you call swipeContainer.setRefreshing(false)
                UserListAdapter adapter = new UserListAdapter(UserListFragment.this.getActivity());
                userNamesListView.setAdapter(adapter);
                swipeContainer.setRefreshing(false);
            }
        });


        ((MainActivity) mActivity).setActionBarTitle(mActivity.getString(R.string.title_file_sharing_service), true);

        /*
        ((Button) view.findViewById(R.id.button_test)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              //  new MyWifiP2PManager(mActivity).test();
            }
        });
        */
        return view;

    }
}
