package org.biver.smartcampus.fragments;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import com.android.volley.Response;

import org.biver.smartcampus.R;
import org.biver.smartcampus.services.userprovidedservices.UserServiceAdapter;
import org.biver.smartcampus.activities.MainActivity;
import org.biver.smartcampus.core.MainAdapter;
import org.biver.smartcampus.core.ServerRequests;
import org.biver.smartcampus.core.SmartSpaceProvider;
import org.biver.smartcampus.entities.CampusProximityInformationService;
import org.biver.smartcampus.utils.ALogger;
import org.biver.smartcampus.utils.Toaster;
import org.apache.commons.validator.routines.UrlValidator;
import org.apache.log4j.Logger;
import org.json.JSONException;

import java.io.IOException;

/**
 * Created by emi on 9/26/16.
 */

public class UserServicesFragment extends Fragment {

    private Activity activity;
    private Logger log;

    private UserServiceAdapter adapter;

    class UpdateAsyncTask extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... params) {
            try {
                adapter.updateData();
            } catch (IOException e) {
                log.error("IOException: ", e);
            } catch (JSONException e) {
                log.error("JSONException: ", e);
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            adapter.notifyDataSetChanged();
        }
    };


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.activity = getActivity();
        this.log = ALogger.getLogger(activity, MainAdapter.class);

        ((MainActivity) activity).setActionBarTitle("User services", true);

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_user_sources, container, false);

        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        adapter = new UserServiceAdapter(activity);


        ListView listView = (ListView) view.findViewById(R.id.list_view_user_sources);
        listView.setAdapter(adapter);

        new UpdateAsyncTask().execute();


        final SwipeRefreshLayout swipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.user_sources_fragment_swip_container);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipeRefreshLayout.setRefreshing(false);
                new UpdateAsyncTask().execute();

            }
        });



        Button buttonAddSource = (Button) view.findViewById(R.id.button_add_information_source);
        buttonAddSource.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                final View view = activity.getLayoutInflater().inflate((R.layout.dialog_add_information_source), null);

                AlertDialog.Builder builder = new AlertDialog.Builder(activity);

                builder.setView(view);
                builder.setTitle("Add information source url");

                builder.setPositiveButton(R.string.dialog_button_positive, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(final DialogInterface dialog, int which) {
                        //do nothing here, will be overwritten
                    }
                });
                builder.setNegativeButton(R.string.dialog_button_negative, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });

                final AlertDialog dialog = builder.create();

                dialog.show();
                dialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        new Thread(new Runnable() {
                            @Override
                            public void run() {

                                String url = ((EditText) view.findViewById(R.id.edit_text_add_information_source_url)).getText().toString();
                                String name = ((EditText) view.findViewById(R.id.edit_text_add_information_source_name)).getText().toString();


                                if (new UrlValidator().isValid(url) && !name.isEmpty()) {
                                    dialog.dismiss();

                                    //TODO implement real name for user generated service
                                    CampusProximityInformationService newService = new CampusProximityInformationService(url, name, true);

                                    try {
                                        new ServerRequests(activity).addInformationSource(
                                                SmartSpaceProvider.getActiveSmartSpace().getBeaconInstanceId(),
                                                newService,
                                                new Response.Listener<String>() {
                                                    @Override
                                                    public void onResponse(String response) {
                                                        //For some reason onResponse is executed in main thread, start separted thread for updating
                                                        new Thread(new Runnable() {
                                                            @Override
                                                            public void run() {
                                                                try {
                                                                    adapter.updateData();
                                                                    //go back to main thread to update ui
                                                                    activity.runOnUiThread(new Runnable() {
                                                                        @Override
                                                                        public void run() {
                                                                            adapter.notifyDataSetChanged();
                                                                        }
                                                                    });
                                                                } catch (IOException e) {
                                                                    log.error("IOException", e);
                                                                } catch (JSONException e) {
                                                                    log.error("JSONException", e);
                                                                }
                                                            }
                                                        }).start();
                                                    }
                                                });
                                    } catch (JSONException e) {
                                        log.error("JSONException", e);
                                    }

                                } else {
                                    Toaster.toast(activity, "Invalid url");
                                }



                            }
                        }).start();
                    }
                });
            }
        });

    }
}
