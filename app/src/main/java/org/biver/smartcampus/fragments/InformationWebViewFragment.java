package org.biver.smartcampus.fragments;

import android.app.Activity;
import android.app.Fragment;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.RelativeLayout;

import com.google.gson.Gson;

import org.biver.smartcampus.R;
import org.biver.smartcampus.activities.MainActivity;
import org.biver.smartcampus.entities.CampusProximityInformationService;
import org.biver.smartcampus.utils.ALogger;
import org.biver.smartcampus.utils.Config;
import org.apache.log4j.Logger;

/**
 * Fragment to be used with CampusProximityInformationService if only a web site has to be displayed
 * This fragment displayes website insie of Android's webview, nothing else
 * Functionality related to webview is copyied from Android's WebViewFragment
 */
public class InformationWebViewFragment extends Fragment {

    private Logger log;

    /**
     * WebView element
     */
    private WebView mWebView;
    private boolean mIsWebViewAvailable;

    private CampusProximityInformationService campusService;

    private Activity activity;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        this.log = ALogger.getLogger(this.getActivity(), InformationWebViewFragment.class);
        this.log.debug("onCreate");
        this.activity = getActivity();

        campusService = new Gson().fromJson(this.getArguments().getString(Config.INTENT_EXTRA_CAMPUS_PROXIMITY_INFORMATION_SERVICE), CampusProximityInformationService.class);

        //Set title of activity to name of this CampusService
        ((MainActivity) activity).setActionBarTitle(campusService.getName(), true);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_information_web_view, container, false);

        if (mWebView != null) {
            mWebView.destroy();
        }
        mWebView = (WebView) view.findViewById(R.id.web_view);
        mIsWebViewAvailable = true;
        return view;
    }

    /**
     * Called when the fragment is visible to the user and actively running. Resumes the WebView.
     */
    @Override
    public void onPause() {
        super.onPause();
        mWebView.onPause();
    }

    /**
     * Called when the fragment is no longer resumed. Pauses the WebView.
     */
    @Override
    public void onResume() {
        mWebView.onResume();
        super.onResume();
    }

    /**
     * Called when the WebView has been detached from the fragment.
     * The WebView is no longer available after this time.
     */
    @Override
    public void onDestroyView() {
        mIsWebViewAvailable = false;
        super.onDestroyView();
    }

    /**
     * Called when the fragment is no longer in use. Destroys the internal state of the WebView.
     */
    @Override
    public void onDestroy() {
        if (mWebView != null) {
            mWebView.destroy();
            mWebView = null;
        }
        super.onDestroy();
    }

    /**
     * Called after webview has been created
     * Can now load website
     * @param savedInstanceState
     */
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        log.debug("onActivityCreated");
        super.onActivityCreated(savedInstanceState);

        //Enable webview remote debugging, only possible with api level >= 19
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            WebView.setWebContentsDebuggingEnabled(true);
        }


        log.debug("WebView loading url " + campusService.getUrl());

        WebView webView = this.getWebView();

        //show loading indicator until web page finished loading
        webView.setWebViewClient(new WebViewClient() {
            public void onPageFinished(WebView view, String url) {
                //remove loading indicator when page finished loading
                RelativeLayout progressIndicator = (RelativeLayout) InformationWebViewFragment.this.getView().findViewById(R.id.loading_indicator_web_view);
                progressIndicator.setVisibility(View.GONE);
            }
        });

        //load url to be displayed
        webView.loadUrl(campusService.getUrl());

        //enable pinch-to-zoom and other settings
        WebSettings webSettings = webView.getSettings();
        webSettings.setSupportZoom(true);
        webSettings.setBuiltInZoomControls(true);
        webSettings.setDisplayZoomControls(false);

        //Javascript required at least for yjs shared editing stuff
        webSettings.setJavaScriptEnabled(true);

    }

    /**
     * Gets the WebView.
     */
    private WebView getWebView() {
        return mIsWebViewAvailable ? mWebView : null;
    }
}
