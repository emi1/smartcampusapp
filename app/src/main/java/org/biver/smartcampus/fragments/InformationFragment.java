package org.biver.smartcampus.fragments;

import android.app.Activity;
import android.app.Fragment;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;

import org.biver.smartcampus.R;
import org.biver.smartcampus.activities.MainActivity;
import org.biver.smartcampus.entities.CampusProximityInformationService;
import org.biver.smartcampus.services.information.CourseInformationItem;
import org.biver.smartcampus.services.information.CourseInformationListAdapter;
import org.biver.smartcampus.utils.ALogger;
import org.biver.smartcampus.utils.Config;
import org.apache.log4j.Logger;
import org.biver.smartcampus.utils.Toaster;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;

/**
 * Created by emi on 6/21/16.
 */
public class InformationFragment extends Fragment {

    private Logger log;

    private CourseInformationListAdapter adapter;

    private Activity activity;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        log = ALogger.getLogger(this.getActivity(), InformationFragment.class);
        this.activity = getActivity();

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container, Bundle savedInstanceState) {

        final View view = inflater.inflate(R.layout.fragment_information_layout, container, false);

        final ListView listView = (ListView) view.findViewById(R.id.list_view_information_items);

        adapter = new CourseInformationListAdapter(this.getActivity());
        listView.setAdapter(adapter);


        Calendar c = Calendar.getInstance();
        final int actualDayOfWeek = c.get(Calendar.DAY_OF_WEEK);

        //Make sure that layout only has 5 children and each is a button for one weekday
        final LinearLayout layout = (LinearLayout) view.findViewById(R.id.layout_day_selector);
        for (int i = 0; i < layout.getChildCount(); i++) {
            final int weekDay = i + 2;
            View v = layout.getChildAt(i);
            if (v instanceof Button) {
                final Button button = (Button) v;

                //set background color of button from current day of week
                if ( weekDay == actualDayOfWeek) {
                    button.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                    button.setTextColor(getResources().getColor(android.R.color.white));
                } else {
                    button.setBackgroundColor(getResources().getColor(R.color.colorLightGray));
                }

                //set on click listener
                button.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        //Get weekday of button from button's tag
                        //Weekday of button needs to be saved in tag in xml
                        //Calendar.MONDAY  is 2, therefore we also start at 2
                        //int weekDay = Integer.valueOf(button.getTag().toString());

                        adapter.setDayOfWeek(weekDay);
                        //must reset adapter so that views are cleared
                        listView.setAdapter(adapter);

                        button.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                        button.setTextColor(getResources().getColor(android.R.color.white));

                        //reset color of unselected buttons
                        for (int j = 0; j < layout.getChildCount(); j++) {
                            Button otherButton = (Button) layout.getChildAt(j);
                            //do not do anything if we selected the already selected button
                            if (button.equals(otherButton)) {
                                continue;
                            }
                            otherButton.setBackgroundColor(getResources().getColor(R.color.colorLightGray));
                            otherButton.setTextColor(getResources().getColor(android.R.color.black));
                        }
                    }
                });
            }
        }

        //set actionbar title via MainActivity
        ((MainActivity) activity).setActionBarTitle(activity.getString(R.string.title_information_service), true);

        return view;
    }


    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        log.debug("onActivityCreated");

        CampusProximityInformationService campusService = CampusProximityInformationService.fromGson(this.getArguments().getString(Config.INTENT_EXTRA_CAMPUS_PROXIMITY_INFORMATION_SERVICE));

        final String url = campusService.getUrl();

        new GetCalendarDataTask().execute(url);

    }


    /**
     * Asynctask to get the calendar data and update ui
     */
    private class GetCalendarDataTask extends AsyncTask<String, Void, ArrayList<CourseInformationItem> > {


        @Override
        protected ArrayList<CourseInformationItem> doInBackground(String... params) {
            String url = params[0];
            return getCalendarData(url);
        }

        @Override
        protected void onPreExecute() {
            //Show loading indicator
            RelativeLayout progressIndicator = (RelativeLayout) InformationFragment.this.getView().findViewById(R.id.loading_indicator);
            progressIndicator.setVisibility(View.VISIBLE);
        }

        @Override
        protected void onPostExecute(ArrayList<CourseInformationItem> courseInformationItems) {
            //Hide loading indicator
            if (InformationFragment.this.getView() != null) {
                RelativeLayout progressIndicator = (RelativeLayout) InformationFragment.this.getView().findViewById(R.id.loading_indicator);
                progressIndicator.setVisibility(View.GONE);
            }

            adapter.setAllData(courseInformationItems);
            adapter.notifyDataSetChanged();
        }

        /**
         * Gets the data to be displayed in the calendar from campus web sites
         * @param url The url on the rooms web presence
         * @return list of CourseInformationItem
         */
        private ArrayList<CourseInformationItem> getCalendarData(final String url) {

            ArrayList<CourseInformationItem> courseInformationItems = new ArrayList<CourseInformationItem>();

            try {
                log.debug("Fetching url: " + url);
                Document doc = Jsoup.connect(url).get();
                log.debug("Parsing...");

                //All elements that contain data relevant to us
                ArrayList<Element> elements = doc.getElementsByAttributeValue("class", "showTextTimeTable");

                //To get outer div
                //doc.getElementsByAttributeValueContaining("style", "overflow:hidden")


                int dayOfWeek = Calendar.MONDAY;
                int oldStartTime = 0;

                for (Element element : elements) {
                    String title = element.attr("title");
                    String href = element.attr("href");

                    //href looks like: ../all/event.asp?gguid=0x662C70D721A05E4786E4E6CBAC939787&tguid=0xBEF068CB23A0D94688F03AE6E97A7855
                    String fullUrl = Config.CAMPUS_BASE_URL + href.substring(2, href.length());

                    //Format: Virtuelle Realität, AH VI, 16:00-17:30h
                    //But sometimes: Übung NuMa I für Maschinenb., Gruppe E.1.1, AH VI, 08:30-10:00h
                    String[] titleElements = title.split(", ");

                    String courseTitle = "";
                    String courseRoom = "";
                    String courseTimeString = "";
                    //old method:
                    /*
                    if (titleElements.length == 3) {
                        courseTitle = titleElements[0];
                        courseRoom = titleElements[1];
                        courseTimeString = titleElements[2];
                    } else if (titleElements.length == 4) {
                        courseTitle = titleElements[0] + titleElements[1];
                        courseRoom = titleElements[2];
                        courseTimeString = titleElements[3];
                    } else if (titleElements.length == 5) {
                        courseTitle = titleElements[0] + titleElements[1] + titleElements[2];
                        courseRoom = titleElements[3];
                        courseTimeString = titleElements[4];
                    }
                    */

                    //more general
                    int len = titleElements.length;
                    for (int i = 0; i < len - 2; i++) {
                        courseTitle += titleElements[i];
                    }
                    courseRoom = titleElements[len - 2];
                    courseTimeString = titleElements[len - 1];


                    int startTime = Integer.valueOf(courseTimeString.substring(0, 2));
                    if (startTime < oldStartTime) {
                        dayOfWeek++;
                        //new day of week
                    } else {
                        //same day of week
                    }
                    oldStartTime = startTime;

                    CourseInformationItem item = new CourseInformationItem(courseTitle, courseRoom, courseTimeString, fullUrl, dayOfWeek);
                    courseInformationItems.add(item);

                }


            } catch (IOException e) {
                log.error("IOException ", e);
            }

            //return empty list if request to server failed
            return courseInformationItems;
        }


    }
}
