package org.biver.smartcampus.fragments;

/**
 * Created by emi on 6/7/16.
 */

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.Nullable;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import org.biver.smartcampus.R;
import org.biver.smartcampus.activities.MainActivity;
import org.biver.smartcampus.core.SmartSpaceProvider;
import org.biver.smartcampus.services.chat.ChatAdapter;
import org.biver.smartcampus.services.chat.ChatMessage;
import org.biver.smartcampus.services.chat.MyXMPP;
import org.biver.smartcampus.services.chat.UserNamersSpinnerAdapter;
import org.biver.smartcampus.services.information.CourseInformationItem;
import org.biver.smartcampus.utils.ALogger;
import org.biver.smartcampus.utils.Preferences;
import org.biver.smartcampus.utils.Utils;
import org.apache.log4j.Logger;
import org.jivesoftware.smack.MessageListener;
import org.jivesoftware.smack.PresenceListener;
import org.jivesoftware.smack.packet.Message;
import org.jivesoftware.smack.packet.Presence;
import org.jivesoftware.smackx.muc.MultiUserChat;
import org.jivesoftware.smackx.muc.Occupant;
import org.jxmpp.jid.EntityFullJid;

import java.util.ArrayList;

import me.srodrigo.androidhintspinner.HintAdapter;
import me.srodrigo.androidhintspinner.HintSpinner;


/**
 * Fragment for the group chat via xmpp
 */
public class TopicChatFragment extends Fragment implements OnClickListener {

    private Logger log;

    private ChatAdapter chatAdapter;

    private EditText editTextInputMessage;
    private ListView msgListView;

    private boolean visible;
    private Activity mActivity;

    private String mucRoomName;
    private MultiUserChat topicMuc;

    private CourseInformationItem courseInformationItem;

    private HintSpinner<Pair<EntityFullJid, String>> hintSpinner;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        log = ALogger.getLogger(this.getActivity(), ChatFragment.class);
        this.mActivity = getActivity();
        log.debug("onCreate");


        courseInformationItem = CourseInformationItem.deserialize(this.getArguments().getString("information_item"));

        mucRoomName = courseInformationItem.getMucTopicName();


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        log.debug("onCreateView");

        View view = inflater.inflate(R.layout.fragment_chat_layout, container, false);

        editTextInputMessage = (EditText) view.findViewById(R.id.messageEditText);
        msgListView = (ListView) view.findViewById(R.id.msgListView);
        ImageButton sendButton = (ImageButton) view.findViewById(R.id.sendMessageButton);
        sendButton.setOnClickListener(this);

        // ----Set autoscroll of listview when a new message arrives----//
        msgListView.setTranscriptMode(ListView.TRANSCRIPT_MODE_ALWAYS_SCROLL);
        msgListView.setStackFromBottom(true);

        //Setup chatadapter
        chatAdapter = new ChatAdapter(mActivity, mucRoomName);
        msgListView.setAdapter(chatAdapter);
        chatAdapter.notifyDataSetChanged();


        topicMuc = MyXMPP.getINSTANCE().getTopicBasedMuc(mActivity, mucRoomName);

        topicMuc.addMessageListener(new MessageListener() {
            @Override
            public void processMessage(final Message message) {
                mActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        chatAdapter.addChatMessage(ChatMessage.fromXmppMessage(message));
                        chatAdapter.notifyDataSetChanged();
                    }
                });

            }
        });


        topicMuc.addParticipantListener(new PresenceListener() {
            @Override
            public void processPresence(Presence presence) {
                log.debug("processPresence: " + presence.toString());
                if (TopicChatFragment.this.visible()) {
                    TopicChatFragment.this.updateCurrentUsersDisplay();
                }
            }
        });

        /*
        //mock data for testing scrollview
        for (int i = 0; i<15; i++) {
            View element = inflater.inflate(R.layout.scroll_view_element, null, false);
            TextView textView = (TextView) element.findViewById(R.id.text_view);
            textView.setText("test " + i);


            LinearLayout layout = (LinearLayout) horizontalScrollView.findViewById(R.id.scroll_view_inner_container);
            layout.addView(element);
        }
        */


        //Set the actionbar title
        ((MainActivity) mActivity).setActionBarTitle(courseInformationItem.getTitle(), true);
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        updateCurrentUsersDisplay();
    }

    /**
     * Update the list of currently available users in top of the fragment
     */
    public void updateCurrentUsersDisplay() {
        mActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (getView() == null) {
                    log.warn("Tried to update horizontal_scroll_view but view is null");
                    return;
                }
                /*
                HorizontalScrollView horizontalScrollView = (HorizontalScrollView) getView().findViewById(R.id.horizontal_scroll_view);

                ((LinearLayout) horizontalScrollView.findViewById(R.id.scroll_view_inner_container)).removeAllViews();

                //View availableUsersElement = LayoutInflater.from(mActivity).inflate(R.layout.scroll_view_element, null, false);

                TextView availableUsersTextView = new TextView(mActivity);
                availableUsersTextView.setText("Available users: ");
                LinearLayout availableUsersLayout = (LinearLayout) horizontalScrollView.findViewById(R.id.scroll_view_inner_container);
                availableUsersLayout.addView(availableUsersTextView);

                ArrayList<EntityFullJid> users = new ArrayList<>(topicMuc.getOccupants());

                //remove own name since getOccupants includes your own username
                EntityFullJid toRemove = null;
                for (EntityFullJid jid : users) {
                    if (topicMuc.getOccupant(jid).getNick().toString().contains(Preferences.getDisplayedChatUserName(mActivity))) {
                        toRemove = jid;
                    }
                }
                users.remove(toRemove);


                for (final EntityFullJid roomJid : users) {
                    final Occupant occupant = MyXMPP.getINSTANCE().getTopicBasedMuc(mucRoomName).getOccupant(roomJid);
                    final Jid jid = occupant.getJid();
                    final String nick = occupant.getNick().toString();

                    View element = LayoutInflater.from(mActivity).inflate(R.layout.scroll_view_element, null, false);
                    TextView textView = (TextView) element.findViewById(R.id.text_view);
                    textView.setText(nick);

                    element.setOnClickListener(new OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            MyXMPP.getINSTANCE().requestFullJid(roomJid, new FullJidResponseListener() {
                                @Override
                                public void onFullJIdReceived(final String fullJidString) {
                                    try {
                                        EntityFullJid entityFullJid = JidCreate.entityFullFrom(fullJidString);
                                        Utils.launchPersonalChatFragment(mActivity, entityFullJid, nick);
                                    } catch (XmppStringprepException e) {
                                        log.error("XmppStringprepException", e);
                                    }

                                }
                            });

                        }
                    });

                    LinearLayout layout = (LinearLayout) horizontalScrollView.findViewById(R.id.scroll_view_inner_container);
                    layout.addView(element);
                }
                */

                ArrayList<EntityFullJid> users = new ArrayList<>(topicMuc.getOccupants());

                //remove own name since getOccupants includes your own username
                EntityFullJid toRemove = null;
                for (EntityFullJid jid : users) {
                    if (topicMuc.getOccupant(jid).getNick().toString().contains(Preferences.getDisplayedChatUserName(mActivity))) {
                        toRemove = jid;
                    }
                }
                users.remove(toRemove);

                ArrayList<Pair<EntityFullJid, String>> jidNickPairs = new ArrayList<>();
                for (EntityFullJid jid : users) {
                    final Occupant occupant = topicMuc.getOccupant(jid);
                    final String nick = occupant.getNick().toString();
                    jidNickPairs.add(new Pair<>(jid, nick));
                }
                final Spinner spinner = (Spinner) getView().findViewById(R.id.available_users_spinner);

                spinner.setAdapter(new UserNamersSpinnerAdapter(mActivity, jidNickPairs));


                HintAdapter<Pair<EntityFullJid, String>> hintAdapter = new HintAdapter<Pair<EntityFullJid, String>>(
                        mActivity,
                        R.layout.user_names_spinner_item,
                        "Select user",
                        jidNickPairs) {

                    @Override
                    protected View getCustomView(int position, View convertView, ViewGroup parent) {
                        final Pair<EntityFullJid, String> jidNickPair = getItem(position);
                        final String nick = jidNickPair.second;
                        final EntityFullJid jid = jidNickPair.first;

                        // Here you inflate the layout and set the value of your widgets
                        View view = inflateLayout(parent, false);
                        ((TextView) view.findViewById(R.id.text_view_user_name_in_spinner)).setText(nick);
                        return view;
                    }
                };

                hintSpinner = new HintSpinner<>(
                        spinner,
                        hintAdapter,
                        new HintSpinner.Callback<Pair<EntityFullJid, String>>() {
                            @Override
                            public void onItemSelected(int position, Pair<EntityFullJid, String> itemAtPosition) {
                                // Here you handle the on item selected event (this skips the hint selected event)
                                Utils.launchPersonalChatFragment(mActivity, itemAtPosition.first.toString(), itemAtPosition.second);

                            }
                        });
                hintSpinner.init();

            }
        });

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.sendMessageButton: {
                final String message = editTextInputMessage.getEditableText().toString();
                this.sendTextMessage(message);
            }
            default:
                return;
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        log.debug("onResume");

        visible = true;

        //Clear message notifications when in ChatFragment
        //TODO

        chatAdapter.notifyDataSetChanged();
    }

    @Override
    public void onPause() {
        super.onPause();
        visible = false;

        MyXMPP.getINSTANCE().leaveMuc(topicMuc);
    }

    /**
     * Return true iff this framgent is visible to the user
     */
    public boolean visible() {
        return visible;
    }


    /**
     * Called when user sends a message via clicking on the send button in user interface
     * @param
     */
    public void sendTextMessage(final String message) {
        log.debug("sendTextMessage");
        if (!message.equalsIgnoreCase("")) {
            editTextInputMessage.setText("");

            final ChatMessage chatMessage = new ChatMessage(
                    Preferences.getDisplayedChatUserName(getActivity()),
                    SmartSpaceProvider.getActiveSmartSpace().getIdentifier(),
                    message);
            chatMessage.setBody(message);

            //update is not needed in group chat because we also receive this message via xmpp where the adapter is notified about update
            //for private messages adapter might be updated here
            //chatAdapter.addChatMessage(chatMessage);
            //chatAdapter.notifyDataSetChanged();


            new Thread(new Runnable() {
                @Override
                public void run() {
                    MyXMPP myXMPP = MyXMPP.getINSTANCE();
                    MultiUserChat topicMuc = myXMPP.getTopicBasedMuc(mActivity, mucRoomName);
                    myXMPP.sendMucMessage(topicMuc, chatMessage);

                }
            }).start();
        }
    }

    /**
     * Reset the text that the user wrote into edittext if the sending of messge was not successful
     * @param message Text to be set inside EditText
     */
    public void setTextInEditText(final String message) {
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                editTextInputMessage.setText(message);
            }
        });
    }

}

