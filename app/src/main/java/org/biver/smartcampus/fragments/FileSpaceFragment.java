package org.biver.smartcampus.fragments;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.melnykov.fab.FloatingActionButton;

import net.gotev.uploadservice.MultipartUploadRequest;
import net.gotev.uploadservice.ServerResponse;
import net.gotev.uploadservice.UploadInfo;
import net.gotev.uploadservice.UploadService;
import net.gotev.uploadservice.UploadStatusDelegate;

import org.biver.smartcampus.BuildConfig;
import org.biver.smartcampus.R;
import org.biver.smartcampus.activities.MainActivity;
import org.biver.smartcampus.core.SmartSpaceProvider;
import org.biver.smartcampus.services.filespace.FileSpaceAdapter;
import org.biver.smartcampus.services.filespace.FileSpaceFileToUpload;
import org.biver.smartcampus.utils.ALogger;
import org.biver.smartcampus.activities.ActivityManager;
import org.biver.smartcampus.utils.Config;
import org.biver.smartcampus.utils.Preferences;
import org.biver.smartcampus.utils.Toaster;
import org.biver.smartcampus.ui.TransferProgressDialog;
import org.biver.smartcampus.utils.Utils;
import org.apache.commons.validator.routines.UrlValidator;
import org.apache.log4j.Logger;
import org.json.JSONException;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by emi on 6/20/16.
 */
public class FileSpaceFragment extends Fragment {

    private static Logger log;

    private FileSpaceAdapter adapter;
    private ListView listView;

    private SwipeRefreshLayout swipeContainer;

    private static TextView textViewAttachementNameInAddItemDialog;
    private static String attachementPath = "";

    private static String filePathToPictureTakenWithIntent;


    private Activity activity;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.activity = getActivity();
        log = ALogger.getLogger(activity, FileSpaceFragment.class);

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        log.debug("onCreateView");

        View view = inflater.inflate(R.layout.fragment_file_space_layout, container, false);

        listView = (ListView) view.findViewById(R.id.list_view_file_space);

        /*
        listView.setOnScrollListener(new AbsListView.OnScrollListener() {
            int mLastFirstVisibleItem = 0;
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {

            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                if (view.getId() == listView.getId()) {
                    final int currentFirstVisibleItem = listView.getFirstVisiblePosition();

                    if (currentFirstVisibleItem > mLastFirstVisibleItem) {
                        // getSherlockActivity().getSupportActionBar().hide();
                        //((MainActivity) activity).getSupportActionBar().hide();
                        activity.findViewById(R.id.layout_second_action_bar).setVisibility(View.GONE);
                        log.debug("actionbar hide");
                    } else if (currentFirstVisibleItem < mLastFirstVisibleItem) {
                        // getSherlockActivity().getSupportActionBar().show();
                        //((MainActivity) activity).getSupportActionBar().show();
                        activity.findViewById(R.id.layout_second_action_bar).setVisibility(View.VISIBLE);
                        log.debug("actionbar show");
                    }

                    mLastFirstVisibleItem = currentFirstVisibleItem;
                }
            }
        });
        */


        //Setup swipe to refresh
        swipeContainer = (SwipeRefreshLayout) view.findViewById(R.id.swipeContainer);
        // Setup refresh listener which triggers new data loading
        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                log.debug("virtual notes onRefresh");
                // Your code to refresh the list here.
                // Make sure you call swipeContainer.setRefreshing(false)
                // once the network request has completed successfully.
                new FillFileSpaceListViewTask().execute();
            }
        });

        adapter = new FileSpaceAdapter(FileSpaceFragment.this.getActivity());
        listView.setAdapter(adapter);
        //Update listview
        new FillFileSpaceListViewTask().execute();


        //FloatingActionButton add item
        FloatingActionButton fab = (FloatingActionButton) view.findViewById(R.id.fab_add_item);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //reset attachment path to avoid using file from previous upload
                attachementPath = "";
                //Add item dialog
                launchAddItemDialog();
            }
        });

        ((MainActivity) activity).setActionBarTitle(this.getActivity().getString(R.string.title_file_space_service), true);

        return view;

    }

    private void launchAddItemDialog() {
        //TODO implement

        // Use the Builder class for convenient dialog construction
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        LayoutInflater inflater = getActivity().getLayoutInflater();

        final View view = inflater.inflate((R.layout.dialog_file_space_add_element), null);
        Button addAttachementButton = (Button) view.findViewById(R.id.button_add_attachement);
        addAttachementButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ActivityManager.launchFileChooserIntent(activity, Config.FILE_SELECT_CODE_FILE_SPACE);
            }
        });
        //set textViewAttachementNameInAddItemDialog so that filepath can be set and displayed to it
        textViewAttachementNameInAddItemDialog = (TextView) view.findViewById(R.id.text_view_attachement_name);
        final EditText editTextComment = (EditText) view.findViewById(R.id.edit_text_add_message);

        final Spinner ttlSpinner = (Spinner) view.findViewById(R.id.spinner_file_life_time_selects);

        //position 7 should be 24h
        //ttlSpinner.setSelection(7, false);

        //set spinner selection to default value set in Config
        final String[] spinnerValues = getResources().getStringArray(R.array.spinner_life_time_values);
        for (int i = 0; i< spinnerValues.length; i++) {
            if (Integer.valueOf(spinnerValues[i]) == Config.DEFAULT_EXPIRE_TIME_MINUTES) {
                ttlSpinner.setSelection(i, false);
            }
        }

        Button takePictureButton = (Button) view.findViewById(R.id.button_take_picture);
        takePictureButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy_MM_dd_HH_mm_ss");
                String fileName = simpleDateFormat.format(System.currentTimeMillis());
                File folder = new File(Preferences.getStorageDir(activity) + "photos");
                folder.mkdirs();

                setAttachementFilePath(folder.getPath() + "/" + fileName + ".jpg");

                File file = new File(folder, fileName + ".jpg");
                try {
                    file.createNewFile();
                } catch (IOException e) {
                    log.error("createNewFile failed " + file.toString(), e);
                }
                Uri outputFileUri = Uri.fromFile(file);

                Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);

                activity.startActivityForResult(cameraIntent, Config.INTENT_ID_TAKE_PICTURE);
            }
        });


        final CheckBox weblinkCheckBox = (CheckBox) view.findViewById(R.id.is_weblink_checkbox);
        weblinkCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                EditText weblinkEditText = (EditText) view.findViewById(R.id.edit_text_add_file_space_element_is_weblink);
                if (isChecked) {
                    weblinkEditText.setVisibility(View.VISIBLE);
                } else {
                    weblinkEditText.setVisibility(View.GONE);
                }
            }
        });

        view.findViewById(R.id.text_view_weblink_comment).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                weblinkCheckBox.setChecked(!weblinkCheckBox.isChecked());
            }
        });


        builder.setTitle(this.getString(R.string.title_file_space_upload_dialog));
        builder.setView(view);
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //Do nothing here but must set button so we can override it later
                    }
                });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // UserProfile cancelled the dialog
                    }
                });
        builder.setCancelable(false);

        //create the dialog
        final AlertDialog dialog = builder.create();

        //show the dialog
        dialog.show();

        //overwrite ok button so that it doesn't automatically closes
        //must be done after showing dialog
        dialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String comment = editTextComment.getText().toString();

                boolean hasWeblink = (((CheckBox) view.findViewById(R.id.is_weblink_checkbox)).isChecked());
                String weblink = ((EditText) view.findViewById(R.id.edit_text_add_file_space_element_is_weblink)).getText().toString();

                if (comment.isEmpty()) {
                    Toaster.toast(FileSpaceFragment.this.getActivity(), FileSpaceFragment.this.getString(R.string.hint_empty_comment));
                    return;
                }

                if (hasWeblink && !(new UrlValidator().isValid(weblink))) {
                    Toaster.toast(FileSpaceFragment.this.getActivity(), FileSpaceFragment.this.getString(R.string.hint_invalid_weblink));
                    return;
                }

                dialog.dismiss();
                //FIXME do not store filepath directly in ui element?
                String filePath = attachementPath;
                int spinnerPositionTTL = ttlSpinner.getSelectedItemPosition();
                int ttlInMin = Integer.valueOf(spinnerValues[spinnerPositionTTL]);
                //expireTime of 0 means the virtual note will never expire!
                long expireTime = 0;
                if (ttlInMin != 0) {
                    int ttlInMs = ttlInMin * 60  * 1000;
                    expireTime = System.currentTimeMillis() + ttlInMs;
                }


                FileSpaceFileToUpload fileToUpload = new FileSpaceFileToUpload(
                        Preferences.getDisplayedChatUserName(activity),
                        filePath,
                        comment,
                        expireTime,
                        SmartSpaceProvider.getActiveSmartSpace().getBeaconInstanceId(),
                        Utils.getUniquePhoneId(activity));

                if (hasWeblink) {
                    fileToUpload.setWeblink(weblink);
                }
                new UploadFileTask(fileToUpload).execute();
            }
        });
    }

    /**
     * AsyncTask to update the content of the list
     */

    public class FillFileSpaceListViewTask extends AsyncTask<Void, Void, Void> {

        public FillFileSpaceListViewTask() {

        }

        @Override
        protected Void doInBackground(Void... params) {
            log.debug("FillFileSpaceListViewTask doInBackground");

            //adapter = new FileSpaceAdapter(FileSpaceFragment.this.getActivity());
            try {
                adapter.updateData();
            } catch (IOException e) {
                log.error("IOException", e);
            } catch (JSONException e) {
                log.error("JSONException", e);
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            log.debug("FillFileSpaceListViewTask onPostExecute");

            //listView.setAdapter(adapter);
            adapter.notifyDataSetChanged();
            swipeContainer.setRefreshing(false);
        }
    }


    /**
     * Set attachement file path to the file selected
     * @param path The path of the file that we wish to upload
     */
    public static void setAttachementFilePath(final String path) {
        attachementPath = path;
    }

    /**
     * Sets the attachement name to the tmp file path that was set before launching intent
     */
    public static void updateAttachementPathInUi(Activity activity) {
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                textViewAttachementNameInAddItemDialog.setText(attachementPath);
            }
        });
    }

    /**
     * AsyncTask to upload a file
     * Currently also updates list afterwards
     */
    private class UploadFileTask extends AsyncTask<Void, Integer, Void> {

        private FileSpaceFileToUpload fileSpaceFileToUpload;

        private TransferProgressDialog uploadProgressDialog;

        UploadFileTask(FileSpaceFileToUpload fileSpaceFileToUpload) {
            this.fileSpaceFileToUpload = fileSpaceFileToUpload;
            log.debug("uploading file: " + fileSpaceFileToUpload.toString());
        }

        @Override
        protected void onPreExecute() {
            if (fileSpaceFileToUpload.hasFileAttachement()) {
                uploadProgressDialog = new TransferProgressDialog(activity, activity.getString(R.string.title_upload_progress_dialog));

                uploadProgressDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialog) {
                        new FillFileSpaceListViewTask().execute();
                    }
                });

                uploadProgressDialog.show();
            }
        }

        @Override
        protected Void doInBackground(Void... params) {
            log.debug("UploadFileTask doInBackground");
            try {
                uploadFile(activity, fileSpaceFileToUpload, SmartSpaceProvider.getActiveSmartSpace().getBeaconInstanceId());
            } catch (IOException e) {
                log.error("IOException: ", e);
            } catch (JSONException e) {
                log.error("JSONException: ", e);
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            log.debug("UploadFileTask onPostExecute");
            //after uploading a file, get new file list from server
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            int progress = values[0];
            uploadProgressDialog.updateStatus(progress);

        }

        /**
         * Uploading a file to FileSpace server
         * Different services are used for uploading with or without attachement
         * @param context
         * @param fileSpaceFileToUpload
         * @param instanceId
         * @throws IOException
         * @throws JSONException
         */
        public void uploadFile(Context context, FileSpaceFileToUpload fileSpaceFileToUpload, String instanceId) throws IOException, JSONException {

            //Convert FileSpaceElement to String representation
            final String argumentsStr = fileSpaceFileToUpload.toJSONObject().toString();

            //Check if there is an attachement
            if (!fileSpaceFileToUpload.getFullFilePath().isEmpty()) {

                final String url = Config.HOST + "uploadFile" + "?id=" + instanceId;;

                UploadService.NAMESPACE = BuildConfig.APPLICATION_ID;

                final String uploadId = new MultipartUploadRequest(context, url)
                        .addFileToUpload(fileSpaceFileToUpload.getFullFilePath(), "file")
                        .addParameter("arguments", argumentsStr)
                        //.addParameter("comment", "asdfasdfasdf")
                        .setMaxRetries(2)
                        .setDelegate(new UploadStatusDelegate() {
                            @Override
                            public void onProgress(UploadInfo uploadInfo) {
                                int progress = uploadInfo.getProgressPercent();
                                publishProgress(progress);
                            }

                            @Override
                            public void onError(UploadInfo uploadInfo, Exception exception) {
                                log.error("upload failed.", exception);
                                publishProgress(-1);
                            }

                            @Override
                            public void onCompleted(UploadInfo uploadInfo, ServerResponse serverResponse) {
                                log.debug("upload successful.");
                                publishProgress(100);
                            }

                            @Override
                            public void onCancelled(UploadInfo uploadInfo) {
                                log.debug("cancelled upload.");
                                publishProgress(-1);
                            }
                        })
                        .startUpload();
            } else {
                //Comment only, no attachement

                String commentUrl = Config.HOST + "postComment" + "?id=" + instanceId;;

                StringRequest myStringRequest = new StringRequest(Request.Method.POST, commentUrl, new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        //This code is executed if the server responds, whether or not the response contains data.
                        //The String 'response' contains the server's response.
                        new Thread(new Runnable() {
                            @Override
                            public void run() {
                                try {
                                    adapter.updateData();
                                    activity.runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            adapter.notifyDataSetChanged();
                                        }
                                    });
                                } catch (IOException e) {
                                    log.error("IOException, ", e);
                                } catch (JSONException e) {
                                    log.error("JSONException, ", e);
                                }
                            }
                        }).start();

                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    } //Create an error listener to handle errors appropriately.
                }) {
                    protected Map<String, String> getParams() {
                        Map<String, String> data = new HashMap<String, String>();
                        data.put("arguments", argumentsStr);
                        return data;
                    }
                };

                //Bug in volley that sends data twice without these settings
                myStringRequest.setRetryPolicy(new DefaultRetryPolicy(
                        0,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));


                RequestQueue requestQueue = Volley.newRequestQueue(context);
                requestQueue.add(myStringRequest);

            }

        }


    }
}
