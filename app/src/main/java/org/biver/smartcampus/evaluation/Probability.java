package org.biver.smartcampus.evaluation;

/**
 * Abstraction of a measured probability of missing beacon signals
 */
class Probability implements Comparable<Probability>{



    private double missProbability;
    private int scanPeriod;

    /**
     *
     */
    private double singleMissProbability;

    /**
     * Number of scanCycles in one time_out
     */
    private int nPower;

    /**
     * Multiplier for the scan:pause ratio
     * scan_period = x -> between_scan_period = ratioMultiplier * scan_period
     */
    private double ratioMultiplier;

    Probability() {

    }

    Probability(double missProbability, double singleMissProbability, int scanPeriod, int nPower, double ratioMultiplier) {
        this.missProbability = missProbability;
        this.scanPeriod = scanPeriod;
        this.singleMissProbability = singleMissProbability;
        this.nPower = nPower;
        this.ratioMultiplier = ratioMultiplier;
    }

    double getMissProbability() {
        return missProbability;
    }

    int getScanPeriod() {
        return scanPeriod;
    }

    double getSingleMissProbability() {
        return singleMissProbability;
    }

    int getnPower() {
        return nPower;
    }

    double getRatioMultiplier() {
        return ratioMultiplier;
    }

    @Override
    public int compareTo(Probability another) {
        return (-1) * Double.compare(missProbability, another.missProbability);
    }

    @Override
    public String toString() {
        return ("minimum: "
                + String.format("%.5f", this.getMissProbability())
                + " singleMissProbability: "
                + String.format("%.5f", this.getSingleMissProbability())
                + " scanPeriod: "
                + this.getScanPeriod()
                + " betweenScan: "
                + this.getScanPeriod() * this.getRatioMultiplier()
                + " nPower: "
                + this.getnPower()
                +" ratio: "
                + this.getRatioMultiplier());
    }
}
