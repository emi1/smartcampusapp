package org.biver.smartcampus.evaluation;

import android.content.Context;
import android.os.Environment;
import android.os.RemoteException;

import org.biver.smartcampus.core.BeaconScannerApplication;
import org.biver.smartcampus.utils.ALogger;
import org.biver.smartcampus.utils.Config;
import org.biver.smartcampus.utils.Preferences;
import org.biver.smartcampus.utils.Toaster;
import org.altbeacon.beacon.Beacon;
import org.altbeacon.beacon.BeaconManager;
import org.altbeacon.beacon.RangeNotifier;
import org.altbeacon.beacon.Region;
import org.apache.log4j.Logger;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Class that holds methods for the technical evaluation and
 * methods for calibrating the beacon parameters
 */
public class Evaluator {




    /**
     * Advertising Interval from the beacon in milliseconds
     */
    private static final int ADV_INT = 500;

    /**
     * Region exit timeout in milliseconds
     */
    private static final int TIME_OUT_IN_MILLISECONDS = 60000;

    /**
     * Number of minutes the measurement should run for each scanPeriod
     */
    private static final int EVALUATION_TIME_IN_MINUTES = 2;

    private double TARGET_MISS_PROBABILITY = 0.005;

    private int MAX_RATIO_MULTIPLIER = 29;

    private double RATIO_DECREMENT = 0.25;

    private int MIN_ADV_INT_MULTIPLIER = 2;
    private int MAX_ADV_INT_MULTIPLIER = 5;
    private int MAX_N_POWER_MULTIPLIER = 100;


    private Logger log;

    private Context context;
    private BeaconManager beaconManager;


    /**
     * RangeNotifier used for technical evaluation
     */
    private class MyEvaluationRangeNotifier implements RangeNotifier {

        private int successScanCount;
        private int missScanCount;
        private int totalCount;

        private int scanPeriod;

        public MyEvaluationRangeNotifier(int scanPeriod) {

            this.scanPeriod = scanPeriod;
        }

        @Override
        public void didRangeBeaconsInRegion(Collection<Beacon> collection, Region region) {
            addTotalCount();
            if (collection.isEmpty()) {
                scanMissed();
            } else {
                scanSuccess();
            }
            log.debug("evaluation ranging. scanPeriod: " + scanPeriod + " totalCount: " + totalCount + " success: " + successScanCount + " scanMissed: " + missScanCount);
        }


        public void scanMissed() {
            missScanCount++;
        }

        public void scanSuccess() {
            successScanCount++;
        }

        public void addTotalCount() {
            totalCount++;
        }

        public void reset() {
            successScanCount = 0;
            missScanCount = 0;
            totalCount = 0;
        }
    };

    public Evaluator(Context context, BeaconManager beaconManager) {
        this.context = context;
        this.beaconManager = beaconManager;
        this.log = ALogger.getLogger(context, Evaluator.class);

    }

    public void start() {

    }

    public static String getResultAsString(MyEvaluationRangeNotifier notifier) {
        int success = notifier.successScanCount;
        int miss = notifier.missScanCount;
        int count = notifier.totalCount;
        double ratio = ((double) success) / ((double) count) * 100;
        final String ratioStr = String.format("%.2f", ratio);

        final String statsString = "totalScans: " + count + " scanSuccess: " + success + " scanMissed: " + miss + " ratio: " + ratioStr;

        return statsString;
    }




    private String getTimeStamp() {
        return new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new Date());
    }

    public void evaluateAll() {

        String result = "";


        //Exit smart space
        //BeaconScannerApplication.getINSTANCE().forceExit();

        int beaconInterval = ADV_INT;


        //int[] intervals = new int[]{1000, 1250, 1500, 1750, 2000};
        /*
        int[] intervals = new int[MAX_ADV_INT_MULTIPLIER];
        for (int i = 1; i <= MAX_ADV_INT_MULTIPLIER; i++) {
            intervals[i - 1] = i * beaconInterval;
        }
        */

        HashMap<Integer, Integer> intervals = new HashMap<>();
        for (int i = MIN_ADV_INT_MULTIPLIER; i <= MAX_ADV_INT_MULTIPLIER; i++) {
            intervals.put(i, i * ADV_INT);
        }
        result = "Starting evalution " + getTimeStamp() + "\n";


        /*
        try {
            beaconManager.stopRangingBeaconsInRegion(Config.BEACON_REGION);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
        */

        stopAllRanging();


        for (Integer i : intervals.keySet()) {
            final int scanPeriod = intervals.get(i);
            /*
        for (int i = 0; i< intervals.length; i++) {
            final int scanPeriod = intervals[i];
*/


            //Update beaconmanager's between scan period with the new value
            beaconManager.setForegroundScanPeriod(scanPeriod);

            beaconManager.setForegroundBetweenScanPeriod(0);

            try {
                beaconManager.updateScanPeriods();
            } catch (RemoteException e) {
                e.printStackTrace();
            }

            MyEvaluationRangeNotifier currentNotifier= new MyEvaluationRangeNotifier(scanPeriod);

            beaconManager.addRangeNotifier(currentNotifier);
            try {
                beaconManager.startRangingBeaconsInRegion(Config.EVALUATION_REGION);
            } catch (RemoteException e) {
                e.printStackTrace();
            }


            try {
                Thread.sleep(EVALUATION_TIME_IN_MINUTES * 60 * 1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            beaconManager.removeAllRangeNotifiers();
            try {
                beaconManager.stopRangingBeaconsInRegion(Config.EVALUATION_REGION);
            } catch (RemoteException e) {
                e.printStackTrace();
            }

            /*
            try {
                beaconManager.stopRangingBeaconsInRegion(Config.BEACON_REGION);
            } catch (RemoteException e) {
                e.printStackTrace();
            }
            */
            //result += "interval: " + interval + " after 1min:" + getResultAsString() + "\n";

            double ratio = ((double) currentNotifier.successScanCount) / ((double) currentNotifier.totalCount) * 100;
            final String ratioStr = String.format("%.2f", ratio);
            String latexString = beaconInterval +
                    " & "  + scanPeriod +
                    " & " + 0  +
                    " & " + currentNotifier.totalCount +
                    " & " + currentNotifier.successScanCount +
                    " & " + currentNotifier.missScanCount +
                    " & " + ratioStr +
                    "\\\\\\hline"+ "\n";
            result += latexString;

        }

        result += "ending evaluation " + getTimeStamp() + "\n";

        writeResultToFile(result, "evaluation_results.txt");
        Toaster.toast(context, "Evaluation finished");

        restartRanging();


    }

    /**
     * Write the result to text file
     * @param result The string to be written to text file
     * @param fileName The file name where it should be stored
     */
    private static void writeResultToFile(final String result, String fileName) {

        File file = new File(Environment.getExternalStorageDirectory() + "/" + Config.STORAGE_DIR, fileName);

        if (file.exists()) {
            file.delete();
        }

        try {
            FileOutputStream stream = new FileOutputStream(file);
            stream.write(result.getBytes());
            stream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Remove the normally used RangeNotifier so there is no interference with the notifier used here for the evaluation
     */
    private void stopAllRanging() {
        try {
            beaconManager.stopRangingBeaconsInRegion(Config.BEACON_REGION);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
        beaconManager.removeAllRangeNotifiers();
    }

    /**
     * Reset beaconmanager with the normalley used RangeNotifier
     */
    private void restartRanging() {
        log.debug("restartRanging");
        try {
            beaconManager.startRangingBeaconsInRegion(Config.BEACON_REGION);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
        beaconManager.addRangeNotifier(BeaconScannerApplication.getINSTANCE().getMyRangeNotifier());
    }

    /**
     * Method to calibrate different phones with optimal scan_period and between_scan_period
     */
    public void calibrate() {

        stopAllRanging();


        double[] probs = new double[MAX_ADV_INT_MULTIPLIER];

        Map<Integer, Double> successProbsMap = new HashMap<>();

        //measuring data, build probabilities for a single scanPeriod
        for (int i = MIN_ADV_INT_MULTIPLIER; i <= MAX_ADV_INT_MULTIPLIER; i++) {
            int scanPeriod = i * ADV_INT;
            int index = i - 1;

            double measuredProb = getSuccessProbabilityForScanPeriod(scanPeriod, ADV_INT);
            successProbsMap.put(scanPeriod, measuredProb);

            probs[index] = measuredProb;

            log.info("Success probability for scanPeriod " + scanPeriod + " " + String.format("%.4f", probs[index]));
        }

        double minimalProb = 1;

        int i = 1;

        //table only needed for visualization
        double[][] table = new double[MAX_ADV_INT_MULTIPLIER][MAX_N_POWER_MULTIPLIER];

        //building table with probabilities of multiple iterations of scanPeriod within one timeout
        while (i <= MAX_ADV_INT_MULTIPLIER) {
            int n = 1;
            while (n <= MAX_N_POWER_MULTIPLIER) { //&& n * 10 * scanPeriod <= TIME_OUT_IN_MILLISECONDS
                double missProb = Math.pow(1 - probs[i - 1], n);
                table[i - 1][n - 1] = missProb;
                n++;
            }
            i++;
        }

        String output = "";
        for (int j = 0; j < MAX_ADV_INT_MULTIPLIER; j++) {
            for (int m = 0; m < MAX_N_POWER_MULTIPLIER && m < 10; m++) {
                String probStr = String.format("%.5f", table[j][m]);
                output += probStr + "  ";
            }
            output += "\n";
        }
        log.debug(output);
        writeResultToFile(getTimeStamp() + "\n" + output, "calibration_table.txt");

        Probability probability = new Probability();
        double ratioMultiplier = MAX_RATIO_MULTIPLIER; //9 pauses for 1 scan
        while (ratioMultiplier > 0) {
            probability = getMinimumForGivenRatio(successProbsMap, ratioMultiplier);

            if (probability.getMissProbability() > TARGET_MISS_PROBABILITY) {
                ratioMultiplier -= RATIO_DECREMENT;
            } else {
                break;
            }
        }


        log.info(probability.toString());
        writeResultToFile(getTimeStamp() + "\n" + probability.toString() , "calibration_results.txt");


        //update settings
        int minScanPeriod = probability.getScanPeriod();
        //TODO proper rounding
        int minBetween = ((int) (probability.getRatioMultiplier() * minScanPeriod));
        Toaster.toast(context, "new values: scanPeriod: " + minScanPeriod + " between: " + minBetween);

        //Set beaconnmanager to use new values
        setValuesAccordingToCalibration(minScanPeriod, minBetween);


        //reset the normal RangeNotifier
        restartRanging();
    }

    /**
     *
     * @param successProbsMap map containing measured success probability for given scanPeriods
     * @param ratioMultiplier Multiplier for the pause between scans (pause = multiplier * scanPeriod)
     * @return
     */
    private Probability getMinimumForGivenRatio(final Map<Integer, Double> successProbsMap, final double ratioMultiplier) {
        double minimalProb = 1;
        int minimalAdvIntMultiplier = 1;
        int minimalNPower = 1;
        int minimalScanPeriod = ADV_INT;
        int i = 1;

        for (Integer currentScanPeriod : successProbsMap.keySet()) {
            int n = MAX_N_POWER_MULTIPLIER;
            while (n > 0) {

                //TODO proper rounding
                int scanCycle = currentScanPeriod + ((int) (ratioMultiplier * currentScanPeriod));
                if (n * scanCycle > TIME_OUT_IN_MILLISECONDS) {
                    n--;
                } else {
                    double missProb = Math.pow(1.0 - successProbsMap.get(currentScanPeriod), n);
                    if (missProb < minimalProb) {
                        minimalProb = missProb;
                        minimalScanPeriod = currentScanPeriod;
                        minimalNPower = n;
                    }
                    break;
                }
            }
            i++;
        }
        return new Probability(minimalProb, (1 - successProbsMap.get(minimalScanPeriod)), minimalScanPeriod, minimalNPower, ratioMultiplier);

    }

    /**
     * Set newly found values to shared preferences
     * @param scanPeriod the scanperiod in milliseconds that should be used
     * @param betweenScanPeriod The pause between scans that should be used
     */
    private void setValuesAccordingToCalibration(final int scanPeriod, final int betweenScanPeriod) {
        log.info("Calibration setting: scanPeriod: " + scanPeriod + " betweenScanPeriod: " + betweenScanPeriod);

        Preferences.setScanPeriod(context, scanPeriod);
        Preferences.setBetweenScanPeriod(context, betweenScanPeriod);

        adjustBeaconManagerSettingsFromSharedPrefs();
    }

    /**
     * New settings have already been saved to sharedPrefs.
     * This method applies them to beaconManager
     */
    private void adjustBeaconManagerSettingsFromSharedPrefs() {
        beaconManager.setForegroundScanPeriod(Preferences.getScanPeriod(context));
        beaconManager.setForegroundBetweenScanPeriod(Preferences.getBetweenScanPeriond(context));
        try {
            beaconManager.updateScanPeriods();
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    /**
     * Calling this takes EVALUATION_TIME_IN_MINUTES time!!
     * @param scanPeriod
     * @param beaconInterval
     * @return
     */
    private double getSuccessProbabilityForScanPeriod(final int scanPeriod, final int beaconInterval) {

        beaconManager.setForegroundScanPeriod(scanPeriod);
        beaconManager.setForegroundBetweenScanPeriod(0);

        try {
            beaconManager.updateScanPeriods();
        } catch (RemoteException e) {
            e.printStackTrace();
        }

        MyEvaluationRangeNotifier currentNotifier= new MyEvaluationRangeNotifier(scanPeriod);

        beaconManager.addRangeNotifier(currentNotifier);
        try {
            beaconManager.startRangingBeaconsInRegion(Config.EVALUATION_REGION);
        } catch (RemoteException e) {
            e.printStackTrace();
        }

        try {
            Thread.sleep(EVALUATION_TIME_IN_MINUTES * 60 * 1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        beaconManager.removeAllRangeNotifiers();
        try {
            beaconManager.stopRangingBeaconsInRegion(Config.EVALUATION_REGION);
        } catch (RemoteException e) {
            e.printStackTrace();
        }

        return ((double) currentNotifier.successScanCount) / (double)currentNotifier.totalCount;
    }

}
