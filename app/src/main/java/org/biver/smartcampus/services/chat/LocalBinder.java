package org.biver.smartcampus.services.chat;


import android.os.Binder;

import java.lang.ref.WeakReference;

/**
 * Binder for the ChatService
 * @param <S>
 */
public class LocalBinder<S> extends Binder {
    private final WeakReference<S> mService;

    public LocalBinder(final S service) {
        mService = new WeakReference<S>(service);
    }

    public S getService() {
        return mService.get();
    }

}
