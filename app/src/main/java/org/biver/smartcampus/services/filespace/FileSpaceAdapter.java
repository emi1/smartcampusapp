package org.biver.smartcampus.services.filespace;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.os.Environment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import org.biver.smartcampus.R;
import org.biver.smartcampus.core.ServerRequests;
import org.biver.smartcampus.core.SmartSpaceProvider;
import org.biver.smartcampus.utils.ALogger;
import org.biver.smartcampus.utils.Config;
import org.biver.smartcampus.utils.Toaster;
import org.biver.smartcampus.utils.Utils;
import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.concurrent.TimeUnit;

/**
 * Created by emi on 6/29/16.
 */
public class FileSpaceAdapter extends BaseAdapter {


    private LayoutInflater inflater;
    private ArrayList<FileSpaceElement> data = new ArrayList<>();

    private SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm dd.MM.yyyy");



    private Activity activity;
    private Logger log;

    public FileSpaceAdapter(Activity activity) {
        this.activity = activity;
        this.log = ALogger.getLogger(activity, FileSpaceAdapter.class);

        this.inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        //FIXME implement properly
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final View view;
        if (convertView == null) {
            view = inflater.inflate(R.layout.list_item_file_space_element, null);
        } else {
            view = convertView;
        }

        final FileSpaceElement currentObject = data.get(position);
        final String currentInstanceId = SmartSpaceProvider.getActiveSmartSpace().getBeaconInstanceId();

        //Set the name of the attachement
        //Or remove the corresponding view if there is no attachement
        View layoutAttachement = view.findViewById(R.id.layout_attachement);
        if (currentObject.hasFileAttachement()) {
            layoutAttachement.setVisibility(View.VISIBLE);
            TextView textViewFile = (TextView) view.findViewById(R.id.text_view_file_name);
            textViewFile.setText(currentObject.getFileName());

            ImageView imageView = (ImageView) view.findViewById(R.id.image_view_attachement_icon);

            //load and display thumbnail iff the attachement is a picture
            if (currentObject.isAttachementPicture()) {
                imageView.setVisibility(View.VISIBLE);
                Picasso.with(activity)
                        .load(Config.HOST + currentInstanceId + "/" + currentObject.getFileName() + ".thumb")
                        .into(imageView);
            } else {
                imageView.setVisibility(View.GONE);
            }

        } else {
            layoutAttachement.setVisibility(View.GONE);
        }

        //Set the text comment entered by the user
        TextView textViewComment = (TextView) view.findViewById(R.id.text_view_comment);
        textViewComment.setText(currentObject.getComment());

        //Setup the textview containing username of user who uploaded this element
        TextView textViewUser = (TextView) view.findViewById(R.id.text_view_user);
        textViewUser.setText(currentObject.getUser());

        //Setup the time this element was uploaded
        TextView textViewTime = (TextView) view.findViewById(R.id.text_view_time_stamp);
        String readableTime = simpleDateFormat.format(currentObject.getTimeStamp());
        textViewTime.setText(readableTime);

        //Display remaining time until it expires
        TextView textViewExpires = (TextView) view.findViewById(R.id.text_view_expires_in);
        String expireTime = "";
        if (currentObject.getExpireTime() != 0) {
            long timeDiff = currentObject.getExpireTime() - System.currentTimeMillis();
            expireTime = String.format("%dh %dm",
                    TimeUnit.MILLISECONDS.toHours(timeDiff),
                    TimeUnit.MILLISECONDS.toMinutes(timeDiff) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toHours(timeDiff)));
        } else {
            expireTime = "never";
        }
        textViewExpires.setText(expireTime);

        TextView weblinkView = (TextView) view.findViewById(R.id.text_view_weblink);
        if (currentObject.isWeblink()) {
            weblinkView.setText(currentObject.getWeblink());
            weblinkView.setVisibility(View.VISIBLE);
        } else {
            weblinkView.setVisibility(View.GONE);
        }

        //Display popupmenu with delete option after click on three buttons
        final ImageView threeDots = (ImageView) view.findViewById(R.id.image_view_three_dots_menu);
        threeDots.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final PopupWindow popupWindow = new PopupWindow(activity);

                View popUpView = View.inflate(activity, R.layout.popup_view, null);


                //set onClickLstener for delete option
                TextView textViewDelete = (TextView) popUpView.findViewById(R.id.text_view_popup_delete);
                textViewDelete.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        //Check if we are the same user that uploaded this virtual note
                        if (Utils.havePermissionToDelete(activity, currentObject)) {

                            //send delete request to server in separate thread
                            new Thread(new Runnable() {
                                @Override
                                public void run() {
                                    String randomId = currentObject.getRandomId();
                                    try {
                                        //delete file
                                        new ServerRequests(activity).deleteFile(currentInstanceId, randomId);
                                    } catch (IOException e) {
                                        log.error("Exception when trying to delete file", e);
                                    }

                                    try {
                                        updateData();
                                    } catch (IOException e) {
                                        log.error("IOException: ", e);
                                    } catch (JSONException e) {
                                        log.error("JSONException: ", e);
                                    }

                                    //Ui update on listview
                                    //FileSpaceFragment.getINSTANCE().new FillFileSpaceListViewTask().execute();
                                    activity.runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            notifyDataSetChanged();
                                        }
                                    });

                                }
                            }).start();
                        } else {
                            //If we are not then we cannot delete it
                            Toaster.toast(activity, "You do not have permissions to delete this.");
                        }
                        popupWindow.dismiss();
                    }
                });

                //Set OnClickListener to download file or hide the view
                TextView viewDownloadAttachement = (TextView) popUpView.findViewById(R.id.text_view_download_attachement);
                if (currentObject.hasFileAttachement()) {
                    viewDownloadAttachement.setVisibility(View.VISIBLE);
                    viewDownloadAttachement.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            log.debug("downloading attachement: " + currentObject.toString());
                            (new ServerRequests(activity)).downloadFileInSpaceWithDownloadManager(currentInstanceId, currentObject.getFileName());
                            Toaster.toast(activity, "Downloading file to " + Environment.getExternalStorageDirectory().toString() + "/" + Config.STORAGE_DIR + "/downloads/");

                            //Dismiss the menu view
                            popupWindow.dismiss();
                        }
                    });
                } else {
                    viewDownloadAttachement.setVisibility(View.GONE);
                }

                //further setup of the popup window
                popUpView.setBackgroundColor(Color.WHITE);
                popUpView.setDrawingCacheBackgroundColor(Color.WHITE);

                //setup and show the popupWindow
                popupWindow.setContentView(popUpView);

                popupWindow.setHeight(LinearLayout.LayoutParams.WRAP_CONTENT);
                popupWindow.setWidth(LinearLayout.LayoutParams.WRAP_CONTENT);
                popupWindow.setFocusable(true);
                //mpopup.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.t));
                popupWindow.setOutsideTouchable(true);
                // mpopup.setAnimationStyle(R.anim.slide_out_up);
                //popupWindow.showAtLocation(popUpView, Gravity.TOP, 0, 0);

                popupWindow.showAsDropDown(threeDots);
            }
        });

        return view;
    }


   /**
     * Refresh data from server
     * @throws IOException
     * @throws JSONException
     */
    public void updateData() throws IOException, JSONException {
        String files = (new ServerRequests(activity)).getFilesInSpace(SmartSpaceProvider.getActiveSmartSpace().getBeaconInstanceId());
        JSONArray jsonArray = new JSONArray(files);

        data = new ArrayList<>();
        for (int i = 0; i < jsonArray.length(); i++) {
            data.add(FileSpaceElement.fromJson(jsonArray.getJSONObject(i)));
        }


        Collections.sort(data);

    }


}
