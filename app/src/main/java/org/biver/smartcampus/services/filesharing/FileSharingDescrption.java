package org.biver.smartcampus.services.filesharing;

import com.google.gson.Gson;

/**
 * Entity class to be used with file sharing with SI-FileTransfer over XMPP
 * Can be serialized to/from the requests description stored in the file transfer
 */
public class FileSharingDescrption {

    private String actualDescription;
    private String sender;

    private FileSharingDescrption() {

    }

    /**
     * Create a FileSharingDescription from textual description and sender
     * @param actualDescription
     * @param sender
     * @return
     */
    public static FileSharingDescrption create(String actualDescription, String sender) {
        FileSharingDescrption desc = new FileSharingDescrption();
        desc.actualDescription = actualDescription;
        desc.sender = sender;
        return desc;
    }

    public String getTextDescription() {
        return actualDescription;
    }

    public String getSender() {
        return sender;
    }

    /**
     * Serialize this FileSharingDescription instance
     * @return serialized String
     */
    public String serialize() {
        return new Gson().toJson(this);
    }


}
