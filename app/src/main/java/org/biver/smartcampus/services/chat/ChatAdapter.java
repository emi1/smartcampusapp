package org.biver.smartcampus.services.chat;


import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.preference.PreferenceManager;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import org.biver.smartcampus.R;
import org.biver.smartcampus.userprofiles.DisplayUserProfileAsyncTask;
import org.biver.smartcampus.utils.ALogger;
import org.biver.smartcampus.utils.Preferences;
import org.apache.log4j.Logger;

import java.text.SimpleDateFormat;
import java.util.ArrayList;

/**
 * Adapter to display messages in listview
 * Messages received via xmpp should be added to the adapter and displayed when listview is visible
 */
public class ChatAdapter extends BaseAdapter {

    private LayoutInflater inflater;
    private ArrayList<ChatMessage> chatMessageList;

    private Logger log;


    private SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd.MM.yyyy HH:mm");

    private Activity activity;

    public ChatAdapter(final Activity activity, final String roomName) {
        this.activity = activity;

        log = ALogger.getLogger(activity, ChatAdapter.class);

        inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        chatMessageList = new ArrayList<>();

        String historyString = PreferenceManager.getDefaultSharedPreferences(activity).getString(Preferences.getChatHistoryKeyOfSpace(roomName) , "");
        if (!historyString.isEmpty()) {
            try {
                ChatHistory chatHistory = new Gson().fromJson(historyString, ChatHistory.class);
                chatMessageList.addAll(chatHistory.getAll());
            } catch (JsonSyntaxException e) {
                log.error("JsonSyntaxException: " , e);
            }
        }



    }

    @Override
    public int getCount() {
        return chatMessageList.size();
    }

    @Override
    public Object getItem(int position) {
        return chatMessageList.get(position);
    }

    @Override
    public long getItemId(int position) {
        //FIXME implement properly
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ChatMessage message = chatMessageList.get(position);

        View view = convertView;
        if (convertView == null) {
            view = inflater.inflate(R.layout.chat_bubble, null);
        }

        TextView textViewMessage = (TextView) view.findViewById(R.id.text_view_message_text);
        textViewMessage.setText(message.getBody());

        LinearLayout layout = (LinearLayout) view.findViewById(R.id.bubble_layout);
        LinearLayout parent_layout = (LinearLayout) view.findViewById(R.id.bubble_layout_parent);

        TextView textViewSender = (TextView) view.findViewById(R.id.text_view_message_sender);
        TextView textViewTimeStamp = (TextView) view.findViewById(R.id.text_view_message_time_stamp);


        // if message is mine then align to right
        if (message.isMine(activity)) {
            layout.setBackgroundResource(R.drawable.bubble2);
            parent_layout.setGravity(Gravity.RIGHT);
            textViewSender.setTextColor(activity.getResources().getColor(R.color.colorMessageMine));
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            params.weight = 1.0f;
            params.gravity = Gravity.RIGHT;
            textViewSender.setLayoutParams(params);
            textViewSender.setText(activity.getString(R.string.text_you));

            textViewTimeStamp.setText(simpleDateFormat.format(message.getTimeStamp()));
        } else {
            // If not mine then align to left
            layout.setBackgroundResource(R.drawable.bubble1);
            parent_layout.setGravity(Gravity.LEFT);
            textViewSender.setTextColor(activity.getResources().getColor(R.color.colorMessageReceived));
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            params.weight = 1.0f;
            params.gravity = Gravity.LEFT;
            textViewSender.setLayoutParams(params);
            textViewSender.setText(message.getSender());

            textViewTimeStamp.setText(simpleDateFormat.format(message.getTimeStamp()));


            //Show dialog with profile information of user
            textViewSender.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    new DisplayUserProfileAsyncTask(activity, message.getSenderJid(), message.getSender()).execute();

                }
            });
        }
        textViewMessage.setTextColor(Color.BLACK);




        return view;
    }

    public void addChatMessage(ChatMessage object) {
        chatMessageList.add(object);
    }

    //TODO implement date separator
    /*
    private static final int TYPE_DATE_HOLDER = 1;
    private static final int TYPE_CHAT_MESSAGE = 2;

    private class ChatListViewItemHolder {
        private Object payload;

        ChatListViewItemHolder(Object payload) {
            if (!(payload instanceof ChatMessage) && !(payload instanceof DateHolder)) {
                //TODO exception handling
            } else {
                this.payload = payload;
            }

        }

        int getType() {
            if (payload instanceof ChatMessage) return TYPE_CHAT_MESSAGE;
            if (payload instanceof DateHolder) return TYPE_DATE_HOLDER;
            return 0;
        }
    }

    private class DateHolder {
        long date;

        DateHolder(long date) {
            this.date = date;
        }
    }
    */
}

