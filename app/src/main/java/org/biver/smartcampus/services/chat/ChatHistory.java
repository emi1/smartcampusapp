package org.biver.smartcampus.services.chat;

import org.biver.smartcampus.utils.Config;

import java.util.Collection;
import java.util.concurrent.ConcurrentSkipListSet;

/**
 * Created by emi on 7/8/16.
 */
public class ChatHistory {

    private ConcurrentSkipListSet<ChatMessage> history;

    public ChatHistory() {
        history = new ConcurrentSkipListSet<>();
    }

    public boolean add(ChatMessage chatMessage) {
        return history.add(chatMessage);
    }

    public Collection<ChatMessage> getAll() {
        return history;
    }

    /**
     * Purge elements if list is too long
     */
    public void purge() {
        while (history.size() > Config.CHAT_HISTORY_MAX_SIZE) {
            history.remove(history.first());
            //history = new ArrayList<>(history.subList(10, history.size()));
        }
    }

}
