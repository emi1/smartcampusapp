package org.biver.smartcampus.services.chat;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

import org.biver.smartcampus.utils.ALogger;
import org.biver.smartcampus.utils.Config;
import org.biver.smartcampus.utils.Utils;
import org.apache.log4j.Logger;

/**
 * Service that contains my xmpp implementation
 */
public class ChatService extends Service {


    private MyXMPP myXMPP;
    private Logger log;


    @Override
    public IBinder onBind(final Intent intent) {
        return new LocalBinder<ChatService>(this);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        log = ALogger.getLogger(this, ChatService.class);
        log.debug("onCreate");


        String userName = Utils.getUniquePhoneId(this);
        String password = Config.DEFAULT_PASSWORD;
        myXMPP = MyXMPP.create(ChatService.this, userName, password);
        myXMPP.connect();


    }

    @Override
    public int onStartCommand(final Intent intent, final int flags, final int startId) {
        log.debug("onStartCommand");
        return Service.START_NOT_STICKY;
    }

    @Override
    public boolean onUnbind(final Intent intent) {
        log.debug("onUnbind");
        return super.onUnbind(intent);
    }

    @Override
    public void onDestroy() {
        log.debug("onDestroy");
        myXMPP.destroy();
        super.onDestroy();
    }

}
