package org.biver.smartcampus.services.chat;

import android.app.Activity;
import android.content.Context;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import org.biver.smartcampus.R;
import org.biver.smartcampus.utils.Utils;
import org.jxmpp.jid.EntityFullJid;

import java.util.ArrayList;

/**
 * Created by emi on 9/14/16.
 */
public class UserNamersSpinnerAdapter extends BaseAdapter {


    private ArrayList<Pair<EntityFullJid, String>> jidNickPairs;
    private Activity activity;
    private LayoutInflater inflater;

    public UserNamersSpinnerAdapter (Activity activity, ArrayList<Pair<EntityFullJid, String>> jidNickPairs) {
        this.activity = activity;
        inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        this.jidNickPairs = jidNickPairs;
    }

    @Override
    public int getCount() {
        return jidNickPairs.size();
    }

    @Override
    public Object getItem(int position) {
        return jidNickPairs.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, final View convertView, ViewGroup parent) {
        View view = convertView;
        if (convertView == null) {
            view = inflater.inflate(R.layout.user_names_spinner_item, null);
        }

        TextView textView = (TextView) view.findViewById(R.id.text_view_user_name_in_spinner);
        textView.setText(jidNickPairs.get(position).second);

        textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Pair<EntityFullJid, String> pair = (Pair<EntityFullJid, String>) getItem(position);
                EntityFullJid jid = pair.first;
                String nick = pair.second;
                Utils.launchPersonalChatFragment(activity, jid.toString(), nick);
            }
        });
        return view;
    }
}
