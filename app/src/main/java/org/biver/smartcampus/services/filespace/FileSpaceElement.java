package org.biver.smartcampus.services.filespace;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Represents an element in the filespace
 */
public class FileSpaceElement implements Comparable<FileSpaceElement>{

    /**
     * The name of the attached file (can be empty)
     */
    private String fileName;

    /**
     * UserProfile entered comment
     */
    private String comment;

    /**
     * UserProfile that uploaded the virtual note (muc nickname)
     */
    private String user;

    /**
     * random id for the element
     */
    private String randomId;

    /**
     * account id of the uplloaded of this element (xmpp account name)
     */
    private  String accountId;

    private String weblink;


    /**
     * Time when the element was uploaded to server
     */
    private long timeStamp;

    private long expireTime;

    private FileSpaceElement() {

    }

    /**
     * Create an instance of FileSpaceElement from jsonobject received from server
     * @param jsonObject
     * @return
     * @throws JSONException
     */
    public static FileSpaceElement fromJson(JSONObject jsonObject) throws JSONException{
        FileSpaceElement result = new FileSpaceElement();
        result.fileName = jsonObject.getString("filename");
        result.comment = jsonObject.getString("comment");
        result.user = jsonObject.getString("user");
        result.randomId = jsonObject.getString("random_id");
        result.accountId = jsonObject.getString("account_id");
        result.timeStamp = jsonObject.getLong("timestamp");
        result.expireTime = jsonObject.getLong("expires");

        if (jsonObject.has("weblink")) {
            result.weblink = jsonObject.getString("weblink");
        }

        return result;
    }

    public String getFileName() {
        return this.fileName;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    public String getComment() {
        return this.comment;
    }

    public String getUser() {
        return this.user;
    }

    public String getRandomId() {
        return randomId;
    }

    public String getAccountId() {
        return accountId;
    }

    public long getTimeStamp() {
        return timeStamp;
    }

    public long getExpireTime() {
        return expireTime;
    }

    public boolean hasFileAttachement() {
        return !this.getFileName().isEmpty();
    }

    public boolean isAttachementPicture() {
        return hasFileAttachement() && getFileName().toUpperCase().endsWith(".JPG");
    }

    public boolean isWeblink() {
        return this.weblink != null && !this.weblink.isEmpty();
    }

    public String getWeblink() {
        return weblink;
    }

    @Override
    public int compareTo(FileSpaceElement another) {
        return -1 * Long.valueOf(timeStamp).compareTo(another.timeStamp);
    }
}
