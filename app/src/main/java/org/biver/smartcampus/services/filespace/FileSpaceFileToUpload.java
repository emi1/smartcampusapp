package org.biver.smartcampus.services.filespace;

import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Class to collect information about file that is going to be uploaded to filespace
 * Should not be used after the upload process anymores
 */
public class FileSpaceFileToUpload {

    private final String fileName;
    private final String comment;
    private final String fullFilePath;
    private final String user;
    private final String instanceId;
    private final String randomId;
    private final String accountId;

    private String weblink;

    private long expireTime;

    /**
     * Construcor to create a representation of a virtual note to be uploaded to the file space
     * @param fullFilePath The full absolute path of a file that should be attached
     *                     Empy string means no attachement!!
     * @param comment UserProfile entered text comment of the virtal note
     * @param expireTime Unix timestamp of when this virtual note expires
     */
    public FileSpaceFileToUpload(final String userName, final String fullFilePath, final String comment, long expireTime, final String instanceId, final String accountId) {
        this.fullFilePath = fullFilePath;
        this.comment = comment;
        this.fileName = fullFilePath.substring(fullFilePath.lastIndexOf("/") + 1);

        this.expireTime = expireTime;
        this.user = userName;
        this.instanceId = instanceId;

        //use a unique id to identify the element
        this.randomId = RandomStringUtils.randomAlphanumeric(8);

        this.accountId = accountId;

    }

    public void setWeblink(final String weblink) {
        this.weblink = weblink;
    }


    /**
     * Returns the absolute filepath (incl filename) of attachement
     * Currently used to check if element has an attachement
     * FIXME should have a designated hasAttachement() method that returns boolean
     * @return
     */
    public String getFullFilePath() {
        return fullFilePath;
    }

    public boolean hasFileAttachement() {
        return !this.getFullFilePath().isEmpty();
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    /**
     * Returns a JSONObject containing all the attributes of this element
     * @return
     * @throws JSONException
     */
    public JSONObject toJSONObject() throws JSONException{

        JSONObject jsonObject = new JSONObject();

        jsonObject.put("id", this.instanceId);
        jsonObject.put("comment", this.comment);
        jsonObject.put("filename", this.fileName);

        jsonObject.put("user", this.user);
        jsonObject.put("random_id", this.randomId);

        jsonObject.put("expires", this.expireTime);
        jsonObject.put("account_id", this.accountId);

        if (this.weblink != null && !this.weblink.isEmpty()) {
            jsonObject.put("weblink", this.weblink);
        }

        return jsonObject;
    }

}
