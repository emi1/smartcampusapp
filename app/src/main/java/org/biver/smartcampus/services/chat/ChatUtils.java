package org.biver.smartcampus.services.chat;

import android.content.Context;
import android.os.Build;

import org.biver.smartcampus.utils.Utils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Convencience methods
 */
public class ChatUtils {
    private static DateFormat dateFormat = new SimpleDateFormat("d MMM yyyy");
    private static DateFormat timeFormat = new SimpleDateFormat("K:mma");

    @Deprecated
    public static String getCurrentTime() {
        Date today = Calendar.getInstance().getTime();
        return timeFormat.format(today);
    }

    @Deprecated
    public static String getCurrentDate() {
        Date today = Calendar.getInstance().getTime();
        return dateFormat.format(today);
    }

    public static String getDefaultUserNameWithShortUniqueIdentifier(Context context) {
        String uniqueId = Utils.getUniquePhoneId(context);
        return Build.MODEL + "-" + (short) uniqueId.hashCode();

    }

}
