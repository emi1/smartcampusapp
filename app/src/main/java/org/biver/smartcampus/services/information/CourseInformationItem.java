package org.biver.smartcampus.services.information;


import com.google.gson.Gson;

import org.apache.commons.lang3.builder.ToStringBuilder;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.StringTokenizer;

/**
 * Created by emi on 6/24/16.
 */
public class CourseInformationItem {

    private String courseTitle;
    private String courseRoom;
    private String courseTimeString;
    private TimeInterval timeInterval;
    private int dayOfWeek;
    private String courseCampusUrl;
    private String courseL2PUrl;
    private String courseWWWUrl;
    private ArrayList<String> listOfProfs;

    public CourseInformationItem(String courseTitle, String courseRoom, String courseTimeString, String courseCampusUrl, int dayOfWeek) {
        this.courseTitle = courseTitle;
        this.courseRoom = courseRoom;
        this.courseTimeString = courseTimeString;
        this.timeInterval = new TimeInterval(courseTimeString);
        this.dayOfWeek = dayOfWeek;
        this.courseCampusUrl = courseCampusUrl;
    }

    public String getTitle() {
        return this.courseTitle;
    }

    public String getMucTopicName() {
        String result = "";
        StringTokenizer tokenizer = new StringTokenizer(courseTitle, " ");
        while (tokenizer.hasMoreTokens()) {
            String str = tokenizer.nextToken();
            int maxLen = (str.length() >= 3) ? 3 : str.length();
            result += str.substring(0, maxLen);
        }
        return result.toLowerCase();
    }

    public String getCourseTimeString() {
        return courseTimeString;
    }

    class TimeInterval {
        private String start; //14:00
        private String end;

        TimeInterval(String inputString) {
            String[] splitted = inputString.split("-");
            this.start = splitted[0];
            this.end = splitted[1].substring(0, splitted[1].length() - 1); //-1 because remove the "h" at the end
        }

        int getStartHours() {
            return Integer.valueOf(start.split(":")[0]);
        }

        int getStartMinutes() {
            return Integer.valueOf(start.split(":")[1]);
        }

        int getEndHours() {
            return Integer.valueOf(end.split(":")[0]);
        }

        int getEndMinutes() {
            return Integer.valueOf(end.split(":")[1]);
        }


        /**
         *
         * @param date The date to be checked if it is inside of the interval
         * @param buffer The buffer in minutes before and after the interval to be also considered inside.
         * @return boolean if the date is inside of the interval with buffer
         */
        boolean isDateTimeInInterval(Date date, int buffer) {

            Calendar calendar = GregorianCalendar.getInstance(); // creates a new calendar instance
            calendar.setTime(date);   // assigns calendar to given date
            int hours = calendar.get(Calendar.HOUR_OF_DAY); // gets hour in 24h format
            int minutes = calendar.get(Calendar.MINUTE);

            //int minutes = dt.getMinuteOfHour();
           // int hours = dt.getHourOfDay();

            int startHours = getStartHours();
            int endHours = getEndHours();
            int startMinutes = getStartMinutes();
            int endMinutes = getEndMinutes();

            //substract the buffer from startminutes
            if (startMinutes - buffer < 0) {
                startHours--;
                startMinutes = startMinutes - buffer + 60;
            } else {
                startMinutes -= buffer;
            }

            //add the buffer to endminutes
            if (endMinutes + buffer >= 60) {
                endHours++;
                endMinutes = endMinutes + buffer - 60;
            } else {
                endMinutes += buffer;
            }

            //Check if the date is in the interval (with added buffer)
            if (startHours <= hours && hours <= endHours) {
                if (hours == startHours) {
                    return startMinutes <= minutes;
                } else if (hours == endHours) {
                    return minutes <= endMinutes;
                } else {
                    return true;
                }
            } else {
                return false;
            }

        }
    }

    public String getCourseCampusUrl() {
        return this.courseCampusUrl;
    }

    public void setCourseL2PUrl(String url) {
        this.courseL2PUrl = url;
    }

    public String getCourseL2PUrl() {
        return this.courseL2PUrl;
    }

    public void setCourseWWWUrl(String url) {
        this.courseWWWUrl = url;
    }

    public String getCourseWWWUrl() {
        return this.courseWWWUrl;
    }

    public int getDayOfWeek() {
        return dayOfWeek;
    }

    public TimeInterval getTimeInterval() {
        return timeInterval;
    }

    public ArrayList<String> getListOfProfs() {
        return listOfProfs;
    }

    public void setListOfProfs(ArrayList<String> list) {
        this.listOfProfs = list;
    }
    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    public String serialize() {
        return new Gson().toJson(this);
    }

    public static CourseInformationItem deserialize(final String serializedCourseInformationItem) {
        return new Gson().fromJson(serializedCourseInformationItem, CourseInformationItem.class);
    }


}
