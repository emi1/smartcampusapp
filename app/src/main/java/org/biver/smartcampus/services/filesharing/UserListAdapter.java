package org.biver.smartcampus.services.filesharing;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.TextView;

import org.biver.smartcampus.MyWifiP2PManager;
import org.biver.smartcampus.userprofiles.DisplayUserProfileAsyncTask;
import org.biver.smartcampus.utils.FullJidResponseListener;
import org.biver.smartcampus.R;
import org.biver.smartcampus.activities.MainActivity;
import org.biver.smartcampus.services.chat.MyXMPP;
import org.biver.smartcampus.utils.ALogger;
import org.biver.smartcampus.activities.ActivityManager;
import org.biver.smartcampus.utils.Config;
import org.biver.smartcampus.utils.Preferences;
import org.biver.smartcampus.utils.Utils;
import org.apache.log4j.Logger;
import org.jivesoftware.smackx.muc.MultiUserChat;
import org.jivesoftware.smackx.muc.Occupant;
import org.jxmpp.jid.EntityFullJid;
import org.jxmpp.jid.Jid;

import java.util.ArrayList;

/**
 * Created by emi on 6/13/16.
 */
public class UserListAdapter extends BaseAdapter {

    private Logger log;

    private ArrayList<EntityFullJid> users = new ArrayList<>();

    private LayoutInflater inflater;
    private Activity activity;


    public UserListAdapter(Activity activity) {

        log = ALogger.getLogger(activity, UserListAdapter.class);

        log.debug("creating new UserListAdapter");

        this.activity = activity;

        //Try to reconnect if the muc is null
        if (MyXMPP.getINSTANCE().getSmartSpaceMuc() == null || !MyXMPP.getINSTANCE().isConnected()) {

            //if not connected, try to reconnect
            new AsyncTask<Void, Void, Void>() {
                @Override
                protected Void doInBackground(Void... params) {
                    MyXMPP.getINSTANCE().reconnectToXMPPAndConfigureMuc();
                    return null;
                }
            }.execute();
        } else {
            MultiUserChat muc = MyXMPP.getINSTANCE().getSmartSpaceMuc();
            this.users = new ArrayList<>(muc.getOccupants());

            //remove own name since getOccupants includes your own username
            EntityFullJid toRemove = null;
            for (EntityFullJid jid : users) {
                if (muc.getOccupant(jid).getNick().toString().contains(Preferences.getDisplayedChatUserName(activity))) {
                    toRemove = jid;
                }
            }
            users.remove(toRemove);
        }



        this.inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.activity = activity;
    }

    @Override
    public int getCount() {
        return users.size();
    }

    @Override
    public Object getItem(int position) {
        return users.get(position);
    }

    @Override
    public long getItemId(int position) {
        //FIXME implement properly
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View view = convertView;
        if (convertView == null) {
            view = inflater.inflate(R.layout.list_item_user_file_sharing, null);
        }

        /*
        final String fullId = users.get(position);

        Occupant occupant = muc.getOccupant(fullId);
        String nick = occupant.getNick();
        final String jid = occupant.getJid();
*/
        final EntityFullJid fullId = users.get(position);

        final Occupant occupant = MyXMPP.getINSTANCE().getSmartSpaceMuc().getOccupant(fullId);
        final Jid jid = occupant.getJid();
        final String nick = occupant.getNick().toString();

        TextView textViewUserName = (TextView) view.findViewById(R.id.text_view_user_name);

        textViewUserName.setText(nick);

        //Functionality moved to dedicated button
        /*
        textViewUserName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                MainActivity.getINSTANCE().setCurrentFileReceiverNickNameString(nick);

                if (jid == null) {
                    log.error("ERROR: jid is null for some reason!");
                    //Toaster.toast(activity, "Error...");
                    MyXMPP.getINSTANCE().requestFullJid(fullId, new FullJidResponseListener() {
                        @Override
                        public void onFullJIdReceived(final String fullJid) {
                            MainActivity.getINSTANCE().setCurrentFileReceiverJidString(fullJid);

                            ActivityManager.launchFileChooserIntent();
                        }
                    });
                } else {
                    MainActivity.getINSTANCE().setCurrentFileReceiverJidString(jid.toString(), nick);
                    //TODO move logic elsewhere
                    ActivityManager.launchFileChooserIntent(Config.FILE_SELECT_CODE_FILE_SHARING);
                }

               // MainActivity.getINSTANCE().setCurrentFileReceiverJidString(fullId.toString());
               // launchFileChooserIntent();

            }
        });
        */


        ImageButton profileButton = (ImageButton) view.findViewById(R.id.image_button_info_profile);
        profileButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                MyXMPP.getINSTANCE().requestFullJid(fullId, new FullJidResponseListener() {
                    @Override
                    public void onFullJIdReceived(final String fullJid) {
                        new DisplayUserProfileAsyncTask(activity, fullJid, nick).execute();

                    }
                });

            }
        });

        ImageButton sendFileButton = (ImageButton) view.findViewById(R.id.image_button_send_file);
        sendFileButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //wifi p2p testing, remove this
                if (false) {
                    new MyWifiP2PManager(activity).start();
                } else {
                    ((MainActivity) activity).setCurrentFileReceiverNickNameString(nick);

                    if (jid == null) {
                        log.warn("ERROR: jid is null for some reason! Requesting full jid");
                        //Toaster.toast(activity, "Error...");
                        MyXMPP.getINSTANCE().requestFullJid(fullId, new FullJidResponseListener() {
                            @Override
                            public void onFullJIdReceived(final String fullJid) {
                                log.debug("FullJid received " + fullJid);
                                ((MainActivity) activity).setCurrentFileReceiverJidString(fullJid);

                                ActivityManager.launchFileChooserIntent(activity, Config.FILE_SELECT_CODE_FILE_SHARING);
                            }
                        });
                    } else {
                        ((MainActivity) activity).setCurrentFileReceiverJidString(jid.toString(), nick);
                        //TODO move logic elsewhere
                        ActivityManager.launchFileChooserIntent(activity, Config.FILE_SELECT_CODE_FILE_SHARING);
                    }
                }

            }
        });

        ImageButton personalChatButton = (ImageButton) view.findViewById(R.id.image_button_private_message);
        personalChatButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utils.launchPersonalChatFragment(activity, fullId.toString(), nick);

            }
        });



        return view;
    }


}
