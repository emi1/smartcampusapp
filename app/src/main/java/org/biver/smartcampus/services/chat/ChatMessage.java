package org.biver.smartcampus.services.chat;

import android.content.Context;

import com.google.gson.Gson;

import org.biver.smartcampus.utils.Preferences;
import org.jivesoftware.smack.packet.Message;

import java.util.Random;

/**
 * Java abstraction for a chat message
 * Defines properties of a message for easy interchange between peers
 */
public class ChatMessage implements Comparable<ChatMessage> {


    private String senderJid;

    private String sender;
    private String receiver;
    private String body;


    private long timeStamp;

    private String msgid;

    /**
     * ChatMessage to be send via xmpp
     * @param sender The nickname(!) of the sender of this message
     * @param receiver The nickname(!) of the receiver of this message
     * @param message The payload, ie the text message to be send
     */
    public ChatMessage(String sender, String receiver, String message) {
        this.body = message;
        this.sender = sender;
        this.receiver = receiver;

        this.timeStamp = System.currentTimeMillis();
        setRandomMsgID();
    }


    public static ChatMessage fromXmppMessage(Message message) {
        return new Gson().fromJson(message.getBody(), ChatMessage.class);
    }


    /**
     * Each ChatMessage gets a unique(?) identifier
     */
    private void setRandomMsgID() {
        msgid = String.format("%02d", new Random().nextInt(10000));
    }

    /**
     * Am I the sender of this message?
     * @param context
     * @return
     */
    public boolean isMine(Context context) {
        String myUserName = Preferences.getDisplayedChatUserName(context);
        return sender.equals(myUserName);
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }


    public String getReceiver() {
        return receiver;
    }

    public String getMsgid() {
        return msgid;
    }

    public String getSender() {
        return sender;
    }

    public long getTimeStamp() {
        return timeStamp;
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof ChatMessage) {
            return this.msgid.equals(((ChatMessage) o).msgid);
        } else {
            return super.equals(o);
        }
    }


    @Override
    public int compareTo(ChatMessage another) {
        return Long.valueOf(this.timeStamp).compareTo(another.timeStamp);
    }

    /**
     * Serialize this ChatMessage instance
     * @return serialized String
     */
    public String serialize() {
        return new Gson().toJson(this);
    }


    public void setSenderJid(String senderJid) {
        this.senderJid = senderJid;
    }

    public String getSenderJid() {
        return senderJid;
    }
}
