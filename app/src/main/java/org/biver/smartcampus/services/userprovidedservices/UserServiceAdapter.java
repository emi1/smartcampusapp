package org.biver.smartcampus.services.userprovidedservices;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;

import org.biver.smartcampus.R;
import org.biver.smartcampus.activities.ActivityManager;
import org.biver.smartcampus.core.ServerRequests;
import org.biver.smartcampus.core.SmartSpaceProvider;
import org.biver.smartcampus.entities.UserService;
import org.json.JSONArray;
import org.json.JSONException;

import java.io.IOException;
import java.util.ArrayList;

import static org.biver.smartcampus.utils.SmartCampusLogger.log;

/**
 * Created by emi on 9/26/16.
 */

public class UserServiceAdapter extends BaseAdapter{

    private Activity activity;

    private ArrayList<UserService> data = new ArrayList<>();

    public UserServiceAdapter(Activity activity) {
        this.activity = activity;
    }
    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final LayoutInflater inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        if (convertView == null) {
            convertView = inflater.inflate(R.layout.list_item_user_services, null);
        }



        final UserService currentService = data.get(position);
        TextView textViewTitle = (TextView) convertView.findViewById(R.id.text_view_user_service_title);

        textViewTitle.setText(currentService.getName());

        TextView textViewUrl = (TextView) convertView.findViewById(R.id.text_view_user_sources_list_url);
        textViewUrl.setText(currentService.getUrl());

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ActivityManager.launchWebUrl(activity, currentService.getUrl());
            }
        });

        //Display popupmenu with delete option after click on three buttons
        final ImageView threeDots = (ImageView) convertView.findViewById(R.id.image_view_three_dots_menu);
        threeDots.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final PopupWindow popupWindow = new PopupWindow(activity);

                View popUpView = View.inflate(activity, R.layout.popup_view_user_source, null);


                //set onClickLstener for delete option
                TextView textViewDelete = (TextView) popUpView.findViewById(R.id.text_view_popup_delete);
                textViewDelete.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        //Check if we are the same user that uploaded this virtual note
                        //if (Utils.havePermissionToDelete(activity, currentObject)) {

                            //send delete request to server in separate thread
                            new Thread(new Runnable() {
                                @Override
                                public void run() {
                                    try {
                                        //delete file
                                        new ServerRequests(activity).deleteUserSource(SmartSpaceProvider.getActiveSmartSpace().getBeaconInstanceId(), currentService.getName());

                                        updateData();

                                        //Ui update on listview
                                        //FileSpaceFragment.getINSTANCE().new FillFileSpaceListViewTask().execute();
                                        activity.runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                notifyDataSetChanged();
                                            }
                                        });
                                    } catch (IOException e) {
                                        log.error("IOException when trying to delete file", e);
                                    } catch (JSONException e) {
                                        log.error("JSONException when trying to delete file", e);                                    }


                                }
                            }).start();
                        //} else {
                        //    //If we are not then we cannot delete it
                        //    Toaster.toast(activity, "You do not have permissions to delete this.");
                        //}
                        popupWindow.dismiss();
                    }
                });

                //further setup of the popup window
                popUpView.setBackgroundColor(Color.WHITE);
                popUpView.setDrawingCacheBackgroundColor(Color.WHITE);

                popupWindow.setContentView(popUpView);

                popupWindow.setHeight(LinearLayout.LayoutParams.WRAP_CONTENT);
                popupWindow.setWidth(LinearLayout.LayoutParams.WRAP_CONTENT);
                popupWindow.setFocusable(true);
                //mpopup.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.t));
                popupWindow.setOutsideTouchable(true);
                // mpopup.setAnimationStyle(R.anim.slide_out_up);
                //popupWindow.showAtLocation(popUpView, Gravity.TOP, 0, 0);
                //popupWindow.showA

                popupWindow.showAsDropDown(threeDots);
            }
        });


        return convertView;
    }

    public void updateData() throws IOException, JSONException {
        String result = new ServerRequests(activity).getUserServices(SmartSpaceProvider.getActiveSmartSpace().getBeaconInstanceId());
        JSONArray array = new JSONArray(result);

        data = new ArrayList<>();
        for (int i = 0; i < array.length(); i++) {
            data.add(UserService.fromJson(array.getJSONObject(i)));
        }


    }
}
