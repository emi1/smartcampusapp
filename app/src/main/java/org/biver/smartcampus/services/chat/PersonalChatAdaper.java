package org.biver.smartcampus.services.chat;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.preference.PreferenceManager;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;

import org.biver.smartcampus.R;
import org.biver.smartcampus.utils.Preferences;

import java.text.SimpleDateFormat;
import java.util.ArrayList;

/**
 * Created by emi on 7/28/16.
 */
public class PersonalChatAdaper extends BaseAdapter {

    private ArrayList<ChatMessage> messages = new ArrayList<>();

    private Activity activity;
    private LayoutInflater inflater;
    private SimpleDateFormat simpleDateFormat;

    /**
     * Adapter for the 1on1 personal chat
     * @param activity Activity this adapter belongs to.
     */
    public PersonalChatAdaper(Activity activity, String receiverNick) {
        this.activity = activity;
        inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        simpleDateFormat = new SimpleDateFormat("HH:mm");

        String historyString = PreferenceManager.getDefaultSharedPreferences(activity).getString(Preferences.getChatHistoryKeyOfUserChat(receiverNick) , "");
        if (!historyString.isEmpty()) {
            ChatHistory chatHistory = new Gson().fromJson(historyString, ChatHistory.class);
            messages.addAll(chatHistory.getAll());
        }

    }

    @Override
    public int getCount() {
        return messages.size();
    }

    @Override
    public Object getItem(int position) {
        return messages.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ChatMessage message = messages.get(position);

        View view = convertView;
        if (convertView == null) {
            view = inflater.inflate(R.layout.chat_bubble, null);
        }

        TextView textViewMessage = (TextView) view.findViewById(R.id.text_view_message_text);
        textViewMessage.setText(message.getBody());

        LinearLayout layout = (LinearLayout) view.findViewById(R.id.bubble_layout);
        LinearLayout parent_layout = (LinearLayout) view.findViewById(R.id.bubble_layout_parent);

        TextView textViewSender = (TextView) view.findViewById(R.id.text_view_message_sender);
        TextView textViewTimeStamp = (TextView) view.findViewById(R.id.text_view_message_time_stamp);


        // if message is mine then align to right
        if (message.isMine(activity)) {
            layout.setBackgroundResource(R.drawable.bubble2);
            parent_layout.setGravity(Gravity.RIGHT);
            textViewSender.setTextColor(activity.getResources().getColor(R.color.colorMessageMine));
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            params.weight = 1.0f;
            params.gravity = Gravity.RIGHT;
            textViewSender.setLayoutParams(params);
            textViewSender.setText(activity.getString(R.string.text_you));

            textViewTimeStamp.setText(simpleDateFormat.format(message.getTimeStamp()));
        } else {
            // If not mine then align to left
            layout.setBackgroundResource(R.drawable.bubble1);
            parent_layout.setGravity(Gravity.LEFT);
            textViewSender.setTextColor(activity.getResources().getColor(R.color.colorMessageReceived));
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            params.weight = 1.0f;
            params.gravity = Gravity.LEFT;
            textViewSender.setLayoutParams(params);
            textViewSender.setText(message.getSender());

            textViewTimeStamp.setText(simpleDateFormat.format(message.getTimeStamp()));
        }
        textViewMessage.setTextColor(Color.BLACK);
        return view;    }

    public void addMessage(ChatMessage message) {
        messages.add(message);
    }
}
