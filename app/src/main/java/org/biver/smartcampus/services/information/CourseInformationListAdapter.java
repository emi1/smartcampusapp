package org.biver.smartcampus.services.information;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;

import org.biver.smartcampus.R;
import org.biver.smartcampus.fragments.TopicChatFragment;
import org.biver.smartcampus.utils.ALogger;
import org.biver.smartcampus.utils.Config;
import org.biver.smartcampus.utils.Toaster;
import org.apache.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;

/**
 * Created by emi on 6/24/16.
 */
public class CourseInformationListAdapter extends BaseAdapter {

    private Logger log;

    private Activity activity;
    private LayoutInflater inflater;

    private ArrayList<CourseInformationItem> allData;

    private ArrayList<CourseInformationItem> displayedData;

    private int selectedDayOfWeek;

    public CourseInformationListAdapter(Activity activity) {
        log = ALogger.getLogger(activity, CourseInformationListAdapter.class);

        this.activity = activity;
        inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        //Set arrays to empty arrays instead of null to avoid exceptions later on
        this.allData = new ArrayList<>();
        this.displayedData = new ArrayList<>();
    }

    @Override
    public int getCount() {
        return displayedData.size();
    }

    @Override
    public Object getItem(int position) {
        return displayedData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, final View convertView, ViewGroup parent) {

        final CourseInformationItem currentItem = displayedData.get(position);

        View view = convertView;
        if (view == null) {
            view = inflater.inflate(R.layout.list_item_information_item, null);
        }

        //Fixme view is recycled improperly that is why we must reset the background here
        view.findViewById(R.id.card_view).setBackgroundColor(activity.getResources().getColor(android.R.color.background_light));

        Button jointopicMucButton = ((Button) view.findViewById(R.id.button_join_course_muc));
        jointopicMucButton.setVisibility(View.GONE);

        //Highlight course that is currently taking place
        if (selectedDayOfWeek == getActualDayOfWeek()) {
            CourseInformationItem.TimeInterval interval = currentItem.getTimeInterval();
            if (interval.isDateTimeInInterval(new Date(), 15)) {
                view.findViewById(R.id.card_view).setBackgroundColor(activity.getResources().getColor(R.color.yellow_highlight_color));
                if (Config.HAS_TOPIC_BASED_MUC) {
                    jointopicMucButton.setVisibility(View.VISIBLE);

                    jointopicMucButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            //WIP
                            //Open topic muc
                            Fragment fragmentToChangeTo = new TopicChatFragment();

                            Bundle bundle = new Bundle();
                            bundle.putString("information_item", currentItem.serialize());
                            fragmentToChangeTo.setArguments(bundle);
                            FragmentManager fm = activity.getFragmentManager();
                            fm.beginTransaction()
                                    .replace(R.id.fragment_container, fragmentToChangeTo)
                                    .addToBackStack(null)
                                    .commit();
                        }
                    });
                }
            }
        }

        TextView textViewTitle = (TextView) view.findViewById(R.id.text_view_course_name);
        TextView textViewTime = (TextView) view.findViewById(R.id.text_view_time_interval);

        textViewTitle.setText(currentItem.getTitle());
        textViewTime.setText(currentItem.getCourseTimeString());


        final Button campusButton = (Button) view.findViewById(R.id.button_campus_link);
        campusButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                launchOpenLinkIntent(currentItem.getCourseCampusUrl());
            }
        });

        final View fView = view;
        (view.findViewById(R.id.image_view_expand_icon)).setOnClickListener(new View.OnClickListener() {
            private boolean isExpanded = false;
            private FetchDataFromCampusUrlTask task;
            @Override
            public void onClick(View v) {
                if (!isExpanded) {
                    isExpanded = true;
                    task = new FetchDataFromCampusUrlTask(fView, currentItem);
                    task.execute();
                } else {
                    isExpanded = false;
                    if (task != null) {
                        task.cancel(true);
                    }
                    //make buttons invisible
                    fView.findViewById(R.id.layout_buttons).setVisibility(View.GONE);
                    fView.findViewById(R.id.layout_professors).setVisibility(View.GONE);
                    fView.findViewById(R.id.progress_bar_information_details).setVisibility(View.GONE);

                }
                v.setRotation(isExpanded ? 180 : 0);
            }
        });



        return view;
    }


    private class FetchDataFromCampusUrlTask extends AsyncTask<Void, Void, Void> {

        private boolean isLoading;
        private View view;
        private String url;
        private CourseInformationItem courseInformationItem;


        FetchDataFromCampusUrlTask(View view, CourseInformationItem courseInformationItem) {
            this.view = view;
            this.courseInformationItem = courseInformationItem;
            this.url = courseInformationItem.getCourseCampusUrl();
        }

        @Override
        protected void onPreExecute() {
            this.isLoading = true;
            view.findViewById(R.id.progress_bar_information_details).setVisibility(View.VISIBLE);
        }

        @Override
        protected Void doInBackground(Void... params) {
            log.debug("doInBackground ");
            Document doc = null;
            try {
                doc = Jsoup.connect(url).get();
            } catch (IOException e) {
                log.error("Scraping failed or was interrupted.", e);
            }
            if (doc == null) {
                return null;
            }

            //Getting list of professors
            if (courseInformationItem.getListOfProfs() == null) {
                ArrayList<String> profList = new ArrayList<>();
                Elements profElements = doc.getElementsByAttributeValueMatching("href", "../all/lecturer.asp");
                for (Element element : profElements) {
                    profList.add(element.ownText());
                }
                courseInformationItem.setListOfProfs(profList);
            }


            //Getting wwwUrl
            String wwwUrl = courseInformationItem.getCourseWWWUrl();
            if (wwwUrl == null) {

                    if (!(doc.getElementsContainingOwnText("WWW") == null
                            || doc.getElementsContainingOwnText("WWW").isEmpty()
                            || doc.getElementsContainingOwnText("WWW").get(0).getElementsByAttribute("href") == null
                            || doc.getElementsContainingOwnText("WWW").get(0).getElementsByAttribute("href").isEmpty())) {


                        Element wwwLinkElement = doc.getElementsContainingOwnText("WWW").get(0).getElementsByAttribute("href").get(0);
                        wwwUrl = wwwLinkElement.attr("href");
                        //Adjust url if it is a relative url
                        if (wwwUrl.startsWith("../../rwth/")) {
                            wwwUrl = Config.CAMPUS_BASE_URL + wwwUrl.substring(10, wwwUrl.length());
                        }
                        //Save url to object so we don't have to retrieve it again
                        courseInformationItem.setCourseWWWUrl(wwwUrl);
                    }
            }

            //getting L2Purl
            String l2pUrl = courseInformationItem.getCourseL2PUrl();
            if (l2pUrl == null) {
                if (!doc.getElementsByAttributeValueContaining("href", "L2PDispatcher").isEmpty()) {

                    Element l2pLinkElement = doc.getElementsByAttributeValueContaining("href", "L2PDispatcher").get(0);
                    //href link looks like: ../../rwth/all/L2PDispatcher.asp?gguid=0x662C70D721A05E4786E4E6CBAC939787
                    String relativeUrl = l2pLinkElement.attr("href");

                    l2pUrl = Config.CAMPUS_BASE_URL + relativeUrl.substring(10, relativeUrl.length());
                    //Save url to object so we don't have to retrieve it again
                    courseInformationItem.setCourseL2PUrl(l2pUrl);
                }
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {

            //set list of profs in ui
            String profsString = "";
            for (String prof : courseInformationItem.getListOfProfs()) {
                profsString += prof + "\n";
            }

            TextView textView = (TextView) view.findViewById(R.id.text_view_profs);
            textView.setText(profsString);


            //Setting wwwUrl button
            final Button wwwLinkButton = (Button) view.findViewById(R.id.button_www_link);
            wwwLinkButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    String wwwUrl = courseInformationItem.getCourseWWWUrl();
                    if (wwwUrl == null) {
                        Toaster.toast(activity, activity.getString(R.string.message_no_web_link_available));
                    } else {
                        log.debug("Launching intent to open link: " + wwwUrl);
                        launchOpenLinkIntent(wwwUrl);
                    }

                }
            });

            //setting l2p button
            final Button l2pButton = (Button) view.findViewById(R.id.button_l2p_link);
            l2pButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String l2pUrl = courseInformationItem.getCourseL2PUrl();
                    if (l2pUrl == null) {
                        Toaster.toast(activity, activity.getString(R.string.message_no_l2p_link_available));
                    } else {
                        log.debug("Launching intent to open link: " + l2pUrl);
                        launchOpenLinkIntent(l2pUrl);
                    }
                }
            });

            //remove progress bar
            view.findViewById(R.id.progress_bar_information_details).setVisibility(View.GONE);


            //make buttons visible
            view.findViewById(R.id.layout_buttons).setVisibility(View.VISIBLE);

            //make prof layouts visible
            view.findViewById(R.id.layout_professors).setVisibility(View.VISIBLE);

            this.isLoading = false;

        }
    }

    /**
     * Open a link with a different app
     * @param url the url to be passed to web app
     */
    private void launchOpenLinkIntent(final String url) {
        //open weblink
        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(Uri.parse(url));
        activity.startActivity(i);
    }

    public void setAllData(ArrayList<CourseInformationItem> data) {
        this.allData = data;

        //Set displayed data to current day of week
        if (this.selectedDayOfWeek == 0) {
            this.selectedDayOfWeek = getActualDayOfWeek();
        }
        this.displayedData = getDataFromDayOfWeek(allData, this.selectedDayOfWeek);
    }

    public ArrayList<CourseInformationItem> getDataFromDayOfWeek(ArrayList<CourseInformationItem> list, int dayOfWeek) {
        ArrayList<CourseInformationItem> result = new ArrayList<>(list);

        Iterator<CourseInformationItem> iterator = result.iterator();
        while (iterator.hasNext()) {
            CourseInformationItem item = iterator.next();
            if (item.getDayOfWeek() != dayOfWeek) {
                iterator.remove();
            }
        }

        return result;
    }


    private int getActualDayOfWeek() {
        Calendar c = Calendar.getInstance();
        int actualDayOfWeek = c.get(Calendar.DAY_OF_WEEK);
        return actualDayOfWeek;
    }

    public void setDayOfWeek(int weekDay) {
        this.selectedDayOfWeek = weekDay;
        this.displayedData = getDataFromDayOfWeek(allData, weekDay);
        this.notifyDataSetChanged();
    }
}
