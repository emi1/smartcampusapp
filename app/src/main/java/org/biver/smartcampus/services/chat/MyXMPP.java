package org.biver.smartcampus.services.chat;

import android.app.Service;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Looper;
import android.preference.PreferenceManager;
import android.support.v7.app.AlertDialog;

import com.google.gson.Gson;

import org.biver.smartcampus.activities.ActivityManager;
import org.biver.smartcampus.core.BeaconScannerApplication;
import org.biver.smartcampus.fragments.ChatFragment;
import org.biver.smartcampus.utils.FullJidResponseListener;
import org.biver.smartcampus.R;
import org.biver.smartcampus.activities.MainActivity;
import org.biver.smartcampus.core.MyNotificationManager;
import org.biver.smartcampus.core.SmartSpaceProvider;
import org.biver.smartcampus.fragments.PersonalChatFragment;
import org.biver.smartcampus.services.filesharing.FileSharingDescrption;
import org.biver.smartcampus.utils.ALogger;
import org.biver.smartcampus.utils.Config;
import org.biver.smartcampus.utils.Preferences;
import org.biver.smartcampus.utils.Toaster;
import org.apache.log4j.Logger;

import org.biver.smartcampus.utils.Utils;
import org.jivesoftware.smack.PresenceListener;
import org.jivesoftware.smack.packet.Presence;
import org.jivesoftware.smackx.muc.MucEnterConfiguration;
import org.jivesoftware.smackx.muc.MultiUserChat;
import org.jivesoftware.smack.ConnectionConfiguration;
import org.jivesoftware.smack.ConnectionListener;
import org.jivesoftware.smack.SmackException;
import org.jivesoftware.smack.SmackException.NotConnectedException;
import org.jivesoftware.smack.XMPPConnection;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smack.chat.Chat;
import org.jivesoftware.smack.chat.ChatManager;
import org.jivesoftware.smack.chat.ChatManagerListener;
import org.jivesoftware.smack.chat.ChatMessageListener;
import org.jivesoftware.smack.packet.Message;
import org.jivesoftware.smack.packet.Stanza;
import org.jivesoftware.smack.tcp.XMPPTCPConnection;
import org.jivesoftware.smack.tcp.XMPPTCPConnectionConfiguration;
import org.jivesoftware.smackx.filetransfer.FileTransfer;
import org.jivesoftware.smackx.filetransfer.FileTransferListener;
import org.jivesoftware.smackx.filetransfer.FileTransferManager;
import org.jivesoftware.smackx.filetransfer.FileTransferRequest;
import org.jivesoftware.smackx.filetransfer.IncomingFileTransfer;
import org.jivesoftware.smackx.filetransfer.OutgoingFileTransfer;
import org.jivesoftware.smackx.iqregister.AccountManager;
import org.jivesoftware.smackx.muc.MultiUserChatManager;
import org.jivesoftware.smackx.receipts.DeliveryReceiptManager;
import org.jivesoftware.smackx.receipts.DeliveryReceiptManager.AutoReceiptMode;
import org.jivesoftware.smackx.receipts.ReceiptReceivedListener;
import org.jivesoftware.smackx.vcardtemp.VCardManager;
import org.jivesoftware.smackx.vcardtemp.packet.VCard;
import org.jxmpp.jid.EntityBareJid;
import org.jxmpp.jid.EntityFullJid;
import org.jxmpp.jid.EntityJid;
import org.jxmpp.jid.Jid;
import org.jxmpp.jid.impl.JidCreate;
import org.jxmpp.jid.parts.Localpart;
import org.jxmpp.jid.parts.Resourcepart;
import org.jxmpp.stringprep.XmppStringprepException;


import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

/**
 * MyXMPP handles everything related to the xmpp connection
 * Handles incoming/outgoing messages and user accounts
 * Implementation is inspired from http://www.tutorialsface.com/2015/08/building-your-own-android-chat-messenger-app-similar-to-whatsapp-using-xmpp-smack-4-1-api-from-scratch-part-2/
 */
public class MyXMPP {


    private ArrayList<FullJidResponseListener> fullJidResponseListeners = new ArrayList<>();

    private ArrayList<MultiUserChat> topicMucs = new ArrayList<>();

    private Logger log;

    private boolean connected = false;
    private boolean loggedin = false;
    private boolean isconnecting = false;
    private boolean isToasted = false;
    private boolean chat_created = false;
    private XMPPTCPConnection connection;
    private String loginUser;
    private String passwordUser;
    private static MyXMPP INSTANCE;
    public static boolean instanceCreated = false;

    private Service chatService;

    private MultiUserChat muc;

    private MyChatManagerListener mChatManagerListener;
    private MyMessageListener mMessageListener;
    private MyGroupChatMessageListener mGroupChatListener;
    private MyFileTransferListener myFileTransferListener;
    /**
     * Private constructor called via create()
     * @param chatService
     * @param loginUser
     * @param passworduser
     */
    private MyXMPP(ChatService chatService, String loginUser, String passworduser) {
        log = ALogger.getLogger(chatService, MyXMPP.class);
        this.loginUser = loginUser;
        this.passwordUser = passworduser;
        this.chatService = chatService;
        mMessageListener = new MyMessageListener();
        mGroupChatListener = new MyGroupChatMessageListener(chatService);

        mChatManagerListener = new MyChatManagerListener();

        initializeConnection();

    }

    /**
     * Create instance of MyXMPP
     * @param chatService
     * @param username Username of the user to be logged in (xmpp account, not nickname)
     * @param password password of the user to be logged in (xmpp account)
     * @return
     */
    public static MyXMPP create(ChatService chatService, String username, String password) {
        if (INSTANCE == null) {
            INSTANCE = new MyXMPP(chatService, username, password);
            instanceCreated = true;
        } else {
            INSTANCE.log.error("Called create but INSTANCE already exists!!");
        }
        return INSTANCE;
    }

    public static MyXMPP getINSTANCE() {
        return INSTANCE;
    }

    /**
     * Initialize the connection to XMPP server
     * Can be called on first launch or on reconnect
     */
    private void initializeConnection() {
        log.debug("initializeConnection");

        XMPPTCPConnectionConfiguration.Builder config = XMPPTCPConnectionConfiguration.builder();
        config.setSecurityMode(ConnectionConfiguration.SecurityMode.disabled);
        //config.setServiceName(Config.IP_ADDRESS);
        try {
            config.setXmppDomain(JidCreate.domainBareFrom(Config.IP_ADDRESS));
        } catch (XmppStringprepException e) {
            log.error("XmppStringprepException", e);
        }
        config.setHost(Config.IP_ADDRESS);
        config.setPort(5222);
        config.setDebuggerEnabled(true);

        XMPPTCPConnection.setUseStreamManagementResumptionDefault(true);
        XMPPTCPConnection.setUseStreamManagementDefault(true);



        connection = new XMPPTCPConnection(config.build());

        MyXMPPConnectionListener connectionListener = new MyXMPPConnectionListener();
        connection.addConnectionListener(connectionListener);

        FileTransferManager manager = FileTransferManager.getInstanceFor(connection);
        myFileTransferListener = new MyFileTransferListener();
        manager.addFileTransferListener(myFileTransferListener);



    }

    public void destroy() {
        connection.disconnect();
        INSTANCE = null;
    }

    /**
     * Connect to xmpp server (in AsyncTask)
     */
    public void connect() {
        log.debug("connect");
        new AsyncTask<Void, Void, Boolean>() {
            @Override
            protected synchronized Boolean doInBackground(Void... arg0) {
                if (connection.isConnected()) {
                    return false;
                }
                isconnecting = true;
                log.debug("XMPP connecting...");

                try {
                    connection.connect();
                    DeliveryReceiptManager dm = DeliveryReceiptManager.getInstanceFor(connection);
                    dm.setAutoReceiptMode(AutoReceiptMode.always);
                    dm.addReceiptReceivedListener(new ReceiptReceivedListener() {
                        @Override
                        public void onReceiptReceived(Jid fromJid, Jid toJid, String receiptId, Stanza receipt) {

                        }
                    });
                    connected = true;
                } catch (IOException e) {
                    log.error("IOException: ", e);
                } catch (SmackException e) {
                    log.error("SMACKException: ", e);
                } catch (XMPPException e) {
                    log.error("XMPPException: ", e);
                } catch (InterruptedException e) {
                    log.error("InterruptedException: ", e);
                }
                return isconnecting = false;
            }


        }.execute();
    }

    /**
     * Login with xmpp account
     */
    public void doLogin() {
        log.info("doLogin");
        boolean loginSuccess = false;
        try {
            connection.login(loginUser, passwordUser);

            loginSuccess = true;
        } catch (SmackException.NoResponseException e) {
            log.error("login failed, NoResponseException: ", e);
        } catch (XMPPException e) {
            //likely due to account not created yet
            log.error("login failed, XMPPException ", e);

        } catch (SmackException e) {
            log.error("login failed, SmackException: ", e);
        } catch (InterruptedException e) {
            log.error("login failed, InterruptedException;", e);
        } catch (IOException e) {
            log.error("login failed, IOException: ", e);
        }

        if (!loginSuccess) {
            log.warn("Login failed... Trying account creation...");
            AccountManager accountManager = AccountManager.getInstance(connection);
            accountManager.sensitiveOperationOverInsecureConnection(true);
            try {
                if (accountManager.supportsAccountCreation()) {
                    Localpart localpart = Localpart.from(loginUser);
                    accountManager.createAccount(localpart, passwordUser);
                    try {
                        //FIXME need to disconnect and reconnect after account creation for it to work?
                        connection.disconnect();
                        connection.connect();


                        //create empty v-card
                        VCard vCard = new VCard();
                        try {
                            VCardManager.getInstanceFor(connection).saveVCard(vCard);
                            log.debug("EMPTY VCARD CREATED");
                        } catch (SmackException.NoResponseException e) {
                            log.error("NoResponseException", e);
                        } catch (XMPPException.XMPPErrorException e) {
                            log.error("XMPPErrorException", e);
                        } catch (SmackException.NotConnectedException e) {
                            log.error("NotConnectedException", e);
                        } catch (InterruptedException e) {
                            log.error("InterruptedException", e);
                        }







                        return;
                        //connection.login(loginUser, passwordUser);
                        //loginSuccess = true;
                    } catch (XMPPException | SmackException | IOException e) {
                        log.error("Login failed: " + e);
                    } catch (Exception e) {
                        log.error("Login failed: " + e);
                    }
                } else {
                    log.error("Server does not support account creation. Abort.");
                }
            } catch (SmackException.NoResponseException e) {
                log.error("SmackException: ", e);
            } catch (XMPPException.XMPPErrorException e) {
                log.error("XMPPException: ", e);
            } catch (NotConnectedException e) {
                log.error("NotConnectedException: ", e);
            } catch (InterruptedException e) {
                log.error("InterruptedException: ", e);
            } catch (XmppStringprepException e) {
                log.error("XmppStringprepException: ", e);
            }
        }

        //If xmpp login was successfull, join MUC of smart space
        if (loginSuccess) {
            log.debug("Login successfull. Creating chat.");
            muc = multiUserChatConfigure(SmartSpaceProvider.getActiveSmartSpace().getIdentifier());
            chat_created = true;
        } else {
            log.error("XMPP Login failed. Could not join muc.");
        }

    }

    private class MyChatManagerListener implements ChatManagerListener {
        @Override
        public void chatCreated(final Chat chat, final boolean createdLocally) {
            log.debug("chatCreated. locally: " + createdLocally);
            if (!createdLocally) {
                chat.addMessageListener(mMessageListener);
            }
            //chat.addMessageListener(new MyMessageListener());
        }
    }


    public void requestFullJid(EntityFullJid receiverJid, FullJidResponseListener responseListener) {
        this.fullJidResponseListeners.add(responseListener);
        sendPersonalMessage(receiverJid, Config.XMPP_REQUESTS.REQUEST_FULL_JID);
    }

    /**
     * Send message destined to a single receiver
     * @param receiverJid
     * @param message Message to be send in String format.
     *                If the message is a ChatMessage it has to be serialized already before being passed here.
     */
    public boolean sendPersonalMessage(EntityJid receiverJid, String message) {

        Chat chat = ChatManager.getInstanceFor(connection).createChat(receiverJid);
        chat.addMessageListener(mMessageListener);

        boolean success = false;

        try {
            chat.sendMessage(message);
            success = true;
        } catch (NotConnectedException e) {
            log.error(e.getMessage(), e);
        } catch (InterruptedException e) {
            log.error(e.getMessage(), e);
        }

        return success;
    }

    /**
     * Send message to the currently connected MUC
     * @param chatMessage
     */
    public boolean sendMucMessage(final MultiUserChat muc, final ChatMessage chatMessage) {
        log.debug("sendMucMessage: " + chatMessage.getBody() + " in muc: " + muc.getRoom().toString());

        boolean success = true;
        //serialize ChatMessage to be send

        //Check connection and reconnect if we are not connected
        if (!connected || !connection.isConnected() || !connection.isAuthenticated()) {
            reconnectToXMPPAndConfigureMuc();
            return false;
        }


        try {
        //    if (connection.isAuthenticated() && connection.isConnected()) {
                //add sender jid to message
                chatMessage.setSenderJid(connection.getUser().toString());

                final Message message = new Message();
                message.setBody( chatMessage.serialize());
                message.setStanzaId(chatMessage.getMsgid());
                message.setType(Message.Type.chat);
                muc.sendMessage(message);
          //  }
            //TODO what do if user is not authenticated
          //  else {
           //     Toaster.toast(chatService, chatService.getString(R.string.message_sending_failed_message));
                //reset edittext with chatmessage whose sending failed
            //    doLogin();
                success = true;
        //    }
        } catch (NotConnectedException e) {
            Toaster.toast(chatService, "Error! Message not sent!");
            log.error("NotConnectedException msg Not sent!-Not Connected!", e);
            success = false;
            reconnectToXMPPAndConfigureMuc();
        } catch (Exception e) {
            log.error("Exception msg Not sent!", e);
            success = false;
        }
        return success;
    }

    /**
     * Set up multi user chat room
     * For each beacon or smart space respectively a MUC should exist
     * @param roomname Name of the muc room as present on xmpp server
     */
    public MultiUserChat multiUserChatConfigure(String roomname){
        log.debug("multiUserChatConfigure with room: " + roomname);

        MultiUserChatManager manager = MultiUserChatManager.getInstanceFor(connection);


        try {
            EntityBareJid mucJid = JidCreate.entityBareFrom(roomname + "@" + "muc." + Config.IP_ADDRESS);

            //do not create new instance of muc when reconnecting, or message listeners can be lost
           // if (muc == null) {
                muc = manager.getMultiUserChat(mucJid);
          //  }
            //this uses arbitrary nick for the muc, not necessarily the username of the xmpp account


            //old and working configuration
            //muc.createOrJoin(Resourcepart.from(Preferences.getDisplayedChatUserName(chatService)));


            Resourcepart nickNameResourcePart = Resourcepart.from(Preferences.getDisplayedChatUserName(chatService));

            MucEnterConfiguration.Builder builder = muc.getEnterConfigurationBuilder(nickNameResourcePart);

            int seconds = Config.MUC_HISTORY_SINCE_IN_SECONDS;
            builder.requestHistorySince(seconds);
            MucEnterConfiguration mucEnterConfiguration = builder.build();


            muc.createOrJoin(mucEnterConfiguration);


        } catch (XMPPException.XMPPErrorException e) {
            log.error("XMPPException: ", e);
        } catch (SmackException e) {
            log.error("SmackException: ", e);
        } catch (XmppStringprepException e) {
            log.error("XmppStringprepException: ", e);
        } catch (InterruptedException e) {
            log.error("InterruptedException: ", e);
        }

        muc.addMessageListener(mGroupChatListener);

        muc.addParticipantListener(new PresenceListener() {
            @Override
            public void processPresence(Presence presence) {
                log.debug("processPresence: " + presence.toString());
               // if (ChatFragment.getINSTANCE() != null && ChatFragment.getINSTANCE().visible()) {
                //    ChatFragment.getINSTANCE().updateCurrentUsersDisplay();
               // }
                if (PersonalChatFragment.getINSTANCE() != null && PersonalChatFragment.getINSTANCE().visible()) {
                    //Check if the receiver for the personal chat is still online
                    //this presence listener listens for any presence changes not necessarily the other user's presence
                    PersonalChatFragment.getINSTANCE().checkIfReceiverStillOnline();
                }
            }
        });

        return muc;
    }

    /**
     * Get list of full jids of users currently in muc.
     * Remove entry for myself in this list
     * @return
     */
    public ArrayList<EntityFullJid> getMucParticipantsListWithoutMyself(Context context) {
        ArrayList<EntityFullJid> users = new ArrayList<>(muc.getOccupants());

        //remove own name since getOccupants includes your own username
        EntityFullJid toRemove = null;
        for (EntityFullJid jid : users) {
            if (muc.getOccupant(jid).getNick().toString().contains(Preferences.getDisplayedChatUserName(context))) {
                toRemove = jid;
            }
        }
        users.remove(toRemove);
        return users;
    }

    /**
     * Leave the muc of this smart space
     */
    public void leaveMuc() {
        log.debug("leaveMuc");
        if (muc != null) {
            try {
                muc.leave();
            } catch (NotConnectedException e) {
                log.error("Exception leaving Muc ", e);
            } catch (InterruptedException e) {
                log.error("Exception leaving Muc ", e);
            }
        }
        chat_created = false;
    }

    /**
     * Leave a specific topicmuc
     * @param topicMuc
     */
    public void leaveMuc(MultiUserChat topicMuc) {
        topicMucs.remove(topicMuc);
        try {
            topicMuc.leave();
        } catch (NotConnectedException e) {
            log.error("NotConnectedException", e);
        } catch (InterruptedException e) {
            log.error("InterruptedException", e);
        }
    }

    public MultiUserChat getSmartSpaceMuc() {
        return muc;
    }

    /**
     * Set up multi user chat room
     * For each beacon or smart space respectively a MUC should exist
     * @param roomname
     */
    public MultiUserChat getTopicBasedMuc(Context context, String roomname){
        log.debug("multiUserChatConfigure with room: " + roomname);

        //See if topicMuc already exists in list of joined mucs
        for (MultiUserChat topicMuc : topicMucs) {
            if (topicMuc.getRoom().toString().toLowerCase().startsWith(roomname.toLowerCase())) {
                return topicMuc;
            }
        }

        //Need to create it first

        final MultiUserChatManager manager = MultiUserChatManager.getInstanceFor(connection);

        MultiUserChat topicMuc = null;

        try {
            EntityBareJid mucJid = JidCreate.entityBareFrom(roomname + "@" + "muc." + Config.IP_ADDRESS);

            topicMuc = manager.getMultiUserChat(mucJid);

            Resourcepart nickNameResourcePart = Resourcepart.from(Preferences.getDisplayedChatUserName(context));

            MucEnterConfiguration.Builder builder = muc.getEnterConfigurationBuilder(nickNameResourcePart);

            int seconds = Config.MUC_HISTORY_SINCE_IN_SECONDS;
            builder.requestHistorySince(seconds);
            MucEnterConfiguration mucEnterConfiguration = builder.build();


            topicMuc.createOrJoin(mucEnterConfiguration);

        } catch (XMPPException.XMPPErrorException e) {
            log.error("XMPPException: ", e);
        } catch (SmackException e) {
            log.error("SmackException: ", e);
        } catch (XmppStringprepException e) {
            log.error("XmppStringprepException: ", e);
        } catch (InterruptedException e) {
            log.error("InterruptedException: ", e);
        }


        /*
        muc.addMessageListener(mGroupChatListener);

        muc.addParticipantListener(new PresenceListener() {
            @Override
            public void processPresence(Presence presence) {
                log.debug("processPresence: " + presence.toString());
                if (ChatFragment.getINSTANCE() != null && ChatFragment.getINSTANCE().visible()) {
                    ChatFragment.getINSTANCE().updateCurrentUsersDisplay();
                }
                if (PersonalChatFragment.getINSTANCE() != null && PersonalChatFragment.getINSTANCE().visible()) {
                    //Check if the receiver for the personal chat is still online
                    //this presence listener listens for any presence changes not necessarily the other user's presence
                    PersonalChatFragment.getINSTANCE().checkIfReceiverStillOnline();
                }
            }
        });
        */
        topicMucs.add(topicMuc);
        return topicMuc;
    }




    /**
     * send a file to recipient via xmpp
     * @param path
     * @param receiverJid
     */
    public void sendFileViaXMPP(final String path, final String description, String receiverJid) {
        log.debug("sendFileViaXMPP path: " + path + " receiver: " + receiverJid);
        File file = new File(path);

        if (!file.exists()) {
            log.error("Something went wrong.");
            Toaster.toast(chatService, "Something went wrong.");
            return;
        }


        final FileTransferManager manager = FileTransferManager.getInstanceFor(connection);

        try {
            OutgoingFileTransfer outgoingFileTransfer = manager.createOutgoingFileTransfer(JidCreate.entityFullFrom(receiverJid));

            
            //TODO get description
            FileSharingDescrption fDesc = FileSharingDescrption.create(description, Preferences.getDisplayedChatUserName(chatService));
            String fDescSerialized = new Gson().toJson(fDesc);

            outgoingFileTransfer.sendFile(file, fDescSerialized);

            //new Thread(new Runnable() {
            //    @Override
            //    public void run() {
                    boolean transferSuccess = true;
                    while(!outgoingFileTransfer.isDone()) {
                        log.debug("Filetransfer in progress. Status: " + outgoingFileTransfer.getStatus());
                        try {
                            Thread.sleep(1000L);
                        } catch (Exception e) {
                            log.error("Exception: " + e);
                            transferSuccess = false;
                        }
                        if (outgoingFileTransfer.getStatus().equals(FileTransfer.Status.error)) {
                            log.error("Error in file transfer: " + outgoingFileTransfer.getError());
                            transferSuccess = false;
                            break;
                        }
                        if (outgoingFileTransfer.getException() != null) {
                            log.error("Exception in transfer:", outgoingFileTransfer.getException());
                            transferSuccess = false;
                            break;
                        }
                    }
                    log.debug("File transfer finished!");

                    if (outgoingFileTransfer.getStatus().equals(FileTransfer.Status.refused)) {
                        log.info("Recipient has denied request");
                        Toaster.toast(chatService, "Request was denied");
                    } else {
                        if (transferSuccess && outgoingFileTransfer.getStatus().equals(FileTransfer.Status.complete)) {
                            log.info("FileTransfer success");
                            Toaster.toast(chatService, chatService.getString(R.string.message_file_transfer_success));
                        } else {
                            log.error("FileTransfer failed");
                            Toaster.toast(chatService, chatService.getString(R.string.message_file_transfer_failed));
                        }
                    }
        //        }
        //    }).start();


        } catch (SmackException e) {
            log.error("Failed to send file.", e);
        } catch (XmppStringprepException e) {
            log.error("XmppStringprepException ", e);
        }

    }

    /**
     * Implementation of ConnectionListener to listen for connection changes
     */
    public class MyXMPPConnectionListener implements ConnectionListener {

        private Logger log;

        public MyXMPPConnectionListener() {
            log = ALogger.getLogger(chatService, MyXMPPConnectionListener.class);
        }

        @Override
        public void connected(final XMPPConnection connection) {

            log.debug("Connected!");
            connected = true;
            if (!connection.isAuthenticated()) {
                doLogin();
            }
        }

        @Override
        public void connectionClosed() {
            log.debug("connectionClosed!");
            //if (isToasted) {
            //    Toaster.toast(chatService, "Connection closed. Check your internet connection.");
            //}
            connected = false;
            chat_created = false;
            loggedin = false;

        }

        @Override
        public void connectionClosedOnError(Exception arg0) {
            //if (isToasted) {
            if (ActivityManager.isAnyActivityVisible()) {
                Toaster.toast(chatService, "Connection closed on error. Check your internet connection.");

            }
            //}
            log.error("ConnectionClosedOn Error!", arg0);
            connected = false;

            chat_created = false;
            loggedin = false;

            /*
            //Try to reconnect...
            Timer timer = new Timer();
            timer.schedule(new TimerTask() {
                @Override
                public void run() {
                    if (Utils.checkConditions(chatService)) {
                        log.debug("Reconnecting...");
                        if (MyXMPP.getINSTANCE().getSmartSpaceMuc().isJoined()) {
                            leaveMuc();
                        }
                        BeaconScannerApplication.getINSTANCE().bindChatConnection();

                        if (ChatFragment.visible()) {
                            MainActivity.getINSTANCE().onBackPressed();
                            Toaster.toast(chatService, "Reconnected.");
                        }


                        //FIXME need to attach chat to messagelistener of fragment
                    } else {
                        log.debug("Could not reconnect. Exiting.");
                        BeaconScannerApplication.getINSTANCE().forceExit();

                    }
                    //reconnectToXMPPAndConfigureMuc();
                }
            }, 10000);
            */


           BeaconScannerApplication.getINSTANCE().forceExit();
        }

        @Override
        public void reconnectingIn(int arg0) {
            log.warn("Reconnecting in " + arg0);
            loggedin = false;
        }

        @Override
        public void reconnectionFailed(Exception arg0) {
            log.error("ReconnectionFailed!");
            connected = false;

            chat_created = false;
            loggedin = false;
        }

        @Override
        public void reconnectionSuccessful() {
            log.debug("ReconnectionSuccessful");
            connected = true;

            chat_created = false;
            loggedin = false;
        }

        @Override
        public void authenticated(XMPPConnection arg0, boolean arg1) {
            log.debug("Authenticated!");
            loggedin = true;

            ChatManager.getInstanceFor(connection).addChatListener(mChatManagerListener);

            chat_created = false;

            if (isToasted) {
                Toaster.toastShort(chatService, "Connected!");
            }
        }
    }

    /**
     * Check if we are connected to xmpp server (and if muc is created?)
     * @return
     */
    public boolean isConnected() {
        return connected;
    }

    public void reconnectToXMPPAndConfigureMuc() {
        log.debug("reconnectToXMPPAndConfigureMuc");
        this.initializeConnection();
        try {
            connection.connect();
        } catch (IOException e) {
            log.error("IOException: ", e);
        } catch (SmackException e) {
            log.error("SMACKException: ", e);
        } catch (XMPPException e) {
            log.error("XMPPException: ", e);
        } catch (InterruptedException e) {
            log.error("InterruptedException: ", e);
        }

        //also reconfigure muc
        this.multiUserChatConfigure(SmartSpaceProvider.getActiveSmartSpace().getIdentifier());
    }

    /**
     * Listener to handle incoming private messages
     */
    private class MyMessageListener implements ChatMessageListener {

        private Logger log;

        public MyMessageListener() {
            log = ALogger.getLogger(chatService, MyMessageListener.class);
        }

        @Override
        public void processMessage(final Chat chat, final Message message) {
            log.info("Xmpp message received: " + message);

            //Toaster.toast(chatService, "received personal chat message " + message.getBody());

            if (message.getBody().equals(Config.XMPP_REQUESTS.REQUEST_FULL_JID)) {
                try {
                    chat.sendMessage(Config.XMPP_REQUESTS.RESPONSE_FULL_JID + connection.getUser());
                } catch (NotConnectedException e) {
                    log.error("NotConnectedException", e);
                } catch (InterruptedException e) {
                    log.error("InterruptedException", e);
                }
            } else if (message.getBody().startsWith(Config.XMPP_REQUESTS.RESPONSE_FULL_JID)) {
                //TODO better separators and message structure!!
                String jid = message.getBody().substring(Config.XMPP_REQUESTS.RESPONSE_FULL_JID.length(), message.getBody().length());

                /*
                for (Iterator<FullJidResponseListener> iter = fullJidResponseListeners.iterator(); iter.hasNext(); ) {
                    FullJidResponseListener responder = iter.next();
                    responder.onFullJIdReceived(jid.toString());
                    iter.remove();
                }
                */

                for (FullJidResponseListener responder : fullJidResponseListeners) {
                    responder.onFullJIdReceived(jid.toString());
                }
                fullJidResponseListeners.clear();

            } else {
                //TODO check if it is a personal message
                //Received personal chat message?
                final ChatMessage chatMessage = new Gson().fromJson(message.getBody(), ChatMessage.class);

                //persist message to storage
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(chatService);

                        String activeSpaceBeaconId = SmartSpaceProvider.getActiveSmartSpace().getBeaconInstanceId();
                        String chatHistoryKey = Preferences.getChatHistoryKeyOfUserChat(chatMessage.getSender());
                        String historyString = prefs.getString(chatHistoryKey, "");

                        ChatHistory chatHistory = new ChatHistory();
                        if (!historyString.isEmpty()) {
                            chatHistory = new Gson().fromJson(historyString, ChatHistory.class);
                        }
                        boolean hasBeenAdded = chatHistory.add(chatMessage);

                        //Only notify if message is really new and is not in ChatHistory yet
                        //This was a bug that we received messages from server that were already seen by user but still causes notification
                        if (hasBeenAdded) {
                            //post notification iff the message is not from myself and the ChatFragment is currently not visible
                            if (!chatMessage.isMine(chatService)) {
                                if ((PersonalChatFragment.getINSTANCE() == null)
                                        || !PersonalChatFragment.getINSTANCE().visible()
                                        || PersonalChatFragment.getINSTANCE().getReceiverJid().equals(chat.getParticipant())) {
                                    (new MyNotificationManager(chatService)).notifyPersonalChatMessage(chatMessage, chat.getParticipant());
                                }
                            }
                        }


                        //Purge so that history doesn't grow indefinitely
                        chatHistory.purge();
                        //Save history to SharedPreferences
                        final String newHistory = new Gson().toJson(chatHistory);
                        prefs.edit()
                                .putString(chatHistoryKey, newHistory)
                                .apply();
                    }
                }).start();

                if (PersonalChatFragment.getINSTANCE() != null
                        && PersonalChatFragment.getINSTANCE().getAdaper() != null
                        && PersonalChatFragment.getINSTANCE().getReceiverJid().equals(chat.getParticipant())) {
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            PersonalChatFragment.getINSTANCE().getAdaper().addMessage(chatMessage);
                            PersonalChatFragment.getINSTANCE().getAdaper().notifyDataSetChanged();
                        }
                    });
                }

            }
        }

    }



    private class MyFileTransferListener implements FileTransferListener {
        private Logger log;

        public MyFileTransferListener() {
            log = ALogger.getLogger(chatService, MyFileTransferListener.class);
        }

        @Override
        public void fileTransferRequest(final FileTransferRequest request) {
            log.info("fileTransferRequest");
            (new MyNotificationManager(chatService)).notifyIncomingFileTransferRequest();

            final Jid requestor = request.getRequestor();

            String strDesc = request.getDescription();
            final FileSharingDescrption fDesc = new Gson().fromJson(strDesc, FileSharingDescrption.class);

            new Handler(Looper.getMainLooper()).post(new Runnable() {
                @Override
                public void run() {
                    // Use the Builder class for convenient dialog construction
                    AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.getINSTANCE());
                    builder.setTitle("File transfer request from: " + fDesc.getSender());
                    builder.setMessage("Filename: " + request.getFileName() + "\r\n" + "Description: " + fDesc.getTextDescription());
                    builder.setPositiveButton("Accept", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            new Thread(new Runnable() {
                                @Override
                                public void run() {
                                    acceptFileTransferRequest(request);
                                }
                            }).start();
                            (new MyNotificationManager(chatService)).clearFileTransferRequstNotification();
                        }
                    });
                    builder.setNegativeButton("Deny", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            // Reject it
                            try {
                                request.reject();
                            } catch (NotConnectedException e) {
                                log.error("NotConnectedException: ", e);
                            } catch (InterruptedException e) {
                                log.error("InterruptedException: ", e);
                            }
                            (new MyNotificationManager(chatService)).clearFileTransferRequstNotification();
                        }
                    });
                    builder.setCancelable(false);
                    builder.setOnDismissListener(new DialogInterface.OnDismissListener() {
                        @Override
                        public void onDismiss(DialogInterface dialog) {
                            //Remove notification when dialog is dismissed
                            (new MyNotificationManager(chatService)).clearFileTransferRequstNotification();
                        }
                    });
                    builder.create().show();
                }
            });



        }

        /**
         * Method to accept the request file transfer request
         * @param request FileTransferRequest that is passed here from FileTransferListener
         */
        private void acceptFileTransferRequest(FileTransferRequest request) {
            //Dismiss the notification (if any)
            (new MyNotificationManager(chatService)).clearFileTransferRequstNotification();

            // Accept it
            final IncomingFileTransfer transfer = request.accept();
            final File destinationFile = new File(Preferences.getStorageDir(chatService) + request.getFileName());
            try {
                transfer.recieveFile(destinationFile);

            } catch (SmackException e) {
                log.error("SmackException: ", e);
            } catch (IOException e) {
                log.error("IOException: ", e);
            }

            new Thread(new Runnable() {
                @Override
                public void run() {
                    boolean transferSuccess = true;
                    while(!transfer.isDone()) {
                        log.debug("Filetransfer in progress. Status: " + transfer.getStatus());
                        try{
                            Thread.sleep(1000L);
                        }catch (Exception e) {
                            log.error("Exception: ", e);
                            transferSuccess = false;
                        }
                        if (transfer.getStatus().equals(FileTransfer.Status.error)) {
                            log.error("Error in file transfer: " + transfer.getError());
                            transferSuccess = false;
                        }
                        if (transfer.getException() != null) {
                            log.error("Exception in transfer:",  transfer.getException());
                            transferSuccess = false;
                        }
                    }
                    log.debug("File transfer finished!");


                    if (transferSuccess) {
                        Toaster.toast(chatService, chatService.getString(R.string.message_file_transfer_success));
                        (new MyNotificationManager(chatService)).notifyFileTransferSuccess(chatService, destinationFile);
                    } else {
                        Toaster.toast(chatService, chatService.getString(R.string.message_file_transfer_failed));
                    }
                }
            }).start();

        }
    }

    public VCard getMyOwnVCard() {
        VCard vCard = null;
        VCardManager manager = VCardManager.getInstanceFor(connection);
        //get existing vcard from server
        try {

            vCard = manager.loadVCard();
        } catch (SmackException.NoResponseException e) {
            log.error("NoResponseException", e);
        } catch (XMPPException.XMPPErrorException e) {
            log.error("XMPPErrorException", e);
        } catch (SmackException.NotConnectedException e) {
            log.error("NotConnectedException", e);
        } catch (InterruptedException e) {
            log.error("InterruptedException", e);
        }
        return vCard;
    }

    /**
     * Method to update my own vCard on server
     * Will be executed in separate thread!
     * @param keyChanged the profile value that has been changed
     * @param value The new value for this key
     */
    public void updateMyOwnVCard(final String keyChanged, final String value) {
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... params) {
                VCardManager manager = VCardManager.getInstanceFor(connection);
                try {
                    //get existing vcard from server
                    VCard vCard = manager.loadVCard();
                    //udpate value
                    vCard.setField(keyChanged, value);
                    //save vcard with server
                    manager.saveVCard(vCard);
                } catch (SmackException.NoResponseException e) {
                    log.error("NoResponseException", e);
                } catch (XMPPException.XMPPErrorException e) {
                    log.error("XMPPErrorException", e);
                } catch (SmackException.NotConnectedException e) {
                    log.error("NotConnectedException", e);
                } catch (InterruptedException e) {
                    log.error("InterruptedException", e);
                }
                //nothing to return from asynctask
                return null;
            }
        }.execute();
    }

    /**
     * Get a vCard from a user
     * @param strJId Jid in String format of user to get the vCard from
     * @return the vCard from the user
     */
    public VCard getVCard(final String strJId) {
        EntityBareJid jid = null;
        try {
            jid = JidCreate.entityBareFrom(strJId);
        } catch (XmppStringprepException e) {
            log.error("XmppStringprepException", e);
        }
        VCard vCard = null;
        try {
            vCard = VCardManager.getInstanceFor(connection).loadVCard(jid);
            //vCard.load(connection, jid);
        } catch (SmackException.NoResponseException e) {
            log.error("NoResponseException", e);
        } catch (XMPPException.XMPPErrorException e) {
            log.error("XMPPErrorException", e);
        } catch (SmackException.NotConnectedException e) {
            log.error("NotConnectedException", e);
        } catch (InterruptedException e) {
            log.error("InterruptedException", e);
        }
        return vCard;

    }

}
