package org.biver.smartcampus.services.chat;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.google.gson.Gson;

import org.biver.smartcampus.core.MyNotificationManager;
import org.biver.smartcampus.core.SmartSpaceProvider;
import org.biver.smartcampus.fragments.ChatFragment;
import org.biver.smartcampus.utils.ALogger;
import org.biver.smartcampus.utils.Preferences;
import org.apache.log4j.Logger;
import org.jivesoftware.smack.MessageListener;
import org.jivesoftware.smack.packet.Message;

/**
 * Listener for incoming messages in the joined MUC
 * This one is used in the main chat service
 */
public class MyGroupChatMessageListener implements MessageListener {

    private Logger log;
    private Context context;

    public MyGroupChatMessageListener(Context context) {
        this.context = context;
        log = ALogger.getLogger(context, MyGroupChatMessageListener.class);
    }

    @Override
    public void processMessage(Message message) {
        log.info("Xmpp message received: " + message);
        if (message.getType() == Message.Type.groupchat && message.getBody() != null) {
            final ChatMessage chatMessage = ChatMessage.fromXmppMessage(message);

            log.info("ChatMessage: " + chatMessage.getBody());


            //persist message to storage
            new Thread(new Runnable() {
                @Override
                public void run() {
                    SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);

                    String activeSpaceBeaconId = SmartSpaceProvider.getActiveSmartSpace().getBeaconInstanceId();
                    String chatHistoryKey = Preferences.getChatHistoryKeyOfSpace(activeSpaceBeaconId);
                    String historyString = prefs.getString(chatHistoryKey, "");

                    ChatHistory chatHistory = new ChatHistory();
                    if (!historyString.isEmpty()) {
                        chatHistory = new Gson().fromJson(historyString, ChatHistory.class);
                    }
                    boolean hasBeenAdded = chatHistory.add(chatMessage);

                    //Only notify if message is really new and is not in ChatHistory yet
                    //This was a bug that we received messages from server that were already seen by user but still causes notification
                    if (hasBeenAdded) {
                        //post notification iff the message is not from myself and the ChatFragment is currently not visible
                        if (!chatMessage.isMine(context)) {
                            if (!ChatFragment.visible()) {
                                (new MyNotificationManager(context)).notifyGroupChatMessage(chatMessage);
                            }
                        }
                    }

                    //Purge so that history doesn't grow indefinitely
                    chatHistory.purge();
                    //Save history to SharedPreferences
                    final String newHistory = new Gson().toJson(chatHistory);
                    prefs.edit()
                            .putString(chatHistoryKey, newHistory)
                            .apply();
                }
            }).start();



        }

    }
}