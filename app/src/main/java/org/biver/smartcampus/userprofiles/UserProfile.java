package org.biver.smartcampus.userprofiles;

import android.content.Context;

import com.google.gson.Gson;

import org.biver.smartcampus.R;
import org.jivesoftware.smackx.vcardtemp.packet.VCard;

import java.lang.reflect.Field;

import static org.biver.smartcampus.utils.SmartCampusLogger.log;


/**
 * Class representing a user profile
 */
public class UserProfile {

    private String firstName = "";
    private String lastName = "";
    private String role = "";
    private String email = "";
    private String webPage = "";
    private String age = "";
    private String about = "";

    /**
     * Private constructore, use fromVCard to create instance
     */
    private UserProfile() {

    }

    /**
     * Create instance of user class via a VCard received from xmpp server
     * @param context
     * @param vCard
     * @return
     */
    public static UserProfile fromVCard(Context context, VCard vCard) {
        UserProfile userProfile = new UserProfile();
        userProfile.firstName = vCard.getField(context.getString(R.string.user_profile_key_first_name));
        userProfile.lastName = vCard.getField(context.getString(R.string.user_profile_key_last_name));
        userProfile.role = vCard.getField(context.getString(R.string.user_profile_key_role));

        userProfile.email = vCard.getField(context.getString(R.string.user_profile_key_email));
        userProfile.webPage = vCard.getField(context.getString(R.string.user_profile_key_web_page));
        userProfile.age = vCard.getField(context.getString(R.string.user_profile_key_age));
        userProfile.about = vCard.getField(context.getString(R.string.user_profile_key_about));

        //set all null Strings to empty string
        for (Field f : userProfile.getClass().getDeclaredFields()) {
            f.setAccessible(true);
            try {
                if (f.getType().getCanonicalName().equals("java.lang.String") && f.get(userProfile) == null) {
                    f.set(userProfile, "");
                }
            } catch (IllegalAccessException e) {
                log.error("IllegalAccessException", e);
            }
        }

        return userProfile;
    }

    public String serialize() {
        return new Gson().toJson(this);
    }

    public static UserProfile deserialize(final String str) {
        final UserProfile userProfile = new Gson().fromJson(str, UserProfile.class);
        return userProfile;
    }

    public String getLastName() {
        return lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getRole() {
        return role;
    }

    public String getAge() {
        return age;
    }

    public String getWebPage() {
        return webPage;
    }

    public String getEmail() {
        return email;
    }

    public String toMultiLineString() {
        String info = "First Name: " + this.getFirstName();
        info += "\n" + "Last Name: " + this.getLastName();
        info += "\n" + "Role: " + this.getRole();
        info += "\n" + "Email: " + this.getEmail();
        info += "\n" + "Web Page: " + this.getWebPage();
        return info;
    }

    public String getAbout() {
        return about;
    }
}
