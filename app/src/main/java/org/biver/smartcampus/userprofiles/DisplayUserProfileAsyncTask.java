package org.biver.smartcampus.userprofiles;

import android.app.Activity;
import android.os.AsyncTask;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import org.biver.smartcampus.R;
import org.biver.smartcampus.services.chat.MyXMPP;
import org.biver.smartcampus.utils.FullJidResponseListener;
import org.biver.smartcampus.utils.Toaster;
import org.biver.smartcampus.utils.Utils;
import org.jivesoftware.smackx.vcardtemp.packet.VCard;
import org.jxmpp.jid.EntityFullJid;
import org.jxmpp.jid.impl.JidCreate;
import org.jxmpp.stringprep.XmppStringprepException;

/**
 * AsyncTask to display a dialog containing a user profile
 */
public class DisplayUserProfileAsyncTask extends AsyncTask<Void, Void, UserProfile> {

    private Activity activity;
    private String userJid;
    private String userNick;
    private boolean withChatIcon;

    /**
     * Constructor to get an instance
     * @param activity Activity where this AsyncTask should be started from
     * @param userJid Full jid of the user to display the user profile from
     * @param userNick Nickname of the user whose profile should be displayed
     */
    public DisplayUserProfileAsyncTask(Activity activity, String userJid, String userNick) {
        this(activity, userJid, userNick, false);
    }

    /**
     * Constructor to get an instance
     * @param activity Activity where this AsyncTask should be started from
     * @param userJid Full jid of the user to display the user profile from
     * @param userNick Nickname of the user whose profile should be displayed
     */
    public DisplayUserProfileAsyncTask(Activity activity, String userJid, String userNick, boolean withChatIcon) {
        this.activity = activity;
        this.userJid = userJid;
        this.userNick = userNick;
        this.withChatIcon = withChatIcon;
    }

    /**
     * Gets a user profile from xmpp vcard
     * @param params
     * @return A user profile
     */
    @Override
    protected UserProfile doInBackground(Void... params) {
        VCard vCard = MyXMPP.getINSTANCE().getVCard(userJid);

        /**
        if (vCard == null) {
            try {
                EntityFullJid fullJid = JidCreate.entityFullFrom(userJid);
                MyXMPP.getINSTANCE().requestFullJid(fullJid, new FullJidResponseListener() {
                    @Override
                    public void onFullJIdReceived(String fullJidReceived) {
                        new DisplayUserProfileAsyncTask(activity, fullJidReceived, userNick);
                    }
                });
            } catch (XmppStringprepException e) {
                e.printStackTrace();
            }
            return null;
        }
         */

        //TODO WIP
        if (vCard != null) {
            UserProfile userProfile = UserProfile.fromVCard(activity, vCard);
            return userProfile;
        } else {
            return null;
        }

    }

    /**
     * Receives a userProfile profile as input and should put it into ui
     * @param userProfile
     */
    @Override
    protected void onPostExecute(final UserProfile userProfile) {
        if (userProfile == null) {
            //TODO show error message to userProfile
            Toaster.toast(activity, "Could not get userProfile profile.");
            return;
        }
        final AlertDialog dialog = new AlertDialog.Builder(activity).create();
        dialog.setTitle("Profile of " + userNick);

        View profileView = activity.getLayoutInflater().inflate(R.layout.dialog_view_user_profile, null);

        ((TextView) profileView.findViewById(R.id.text_view_user_profile_first_name)).setText(userProfile.getFirstName());
        ((TextView) profileView.findViewById(R.id.text_view_user_profile_last_name)).setText(userProfile.getLastName());
        ((TextView) profileView.findViewById(R.id.text_view_user_profile_role)).setText(userProfile.getRole());
        ((TextView) profileView.findViewById(R.id.text_view_user_profile_email)).setText(userProfile.getEmail());
        ((TextView) profileView.findViewById(R.id.text_view_user_profile_web_page)).setText(userProfile.getWebPage());
        ((TextView) profileView.findViewById(R.id.text_view_user_profile_age)).setText(userProfile.getAge());
        ((TextView) profileView.findViewById(R.id.text_view_user_profile_about)).setText(userProfile.getAbout());

        if (withChatIcon) {
            ImageButton chatImageButton = ((ImageButton) profileView.findViewById(R.id.image_button_private_message_profile_dialog));
            chatImageButton.setVisibility(View.VISIBLE);

            chatImageButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                    Utils.launchPersonalChatFragment(activity, userJid, userNick);
                    Toaster.toastShort(activity, "Joined personal chat with: " + userNick);
                }
            });
        }

        dialog.setView(profileView);
        dialog.show();
    }
}