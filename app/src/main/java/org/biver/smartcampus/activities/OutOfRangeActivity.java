package org.biver.smartcampus.activities;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import org.biver.smartcampus.R;
import org.biver.smartcampus.core.SmartSpaceProvider;
import org.biver.smartcampus.utils.ALogger;
import org.biver.smartcampus.utils.Config;
import org.biver.smartcampus.utils.Preferences;
import org.biver.smartcampus.utils.Utils;
import org.apache.log4j.Logger;

import java.util.ArrayList;

/**
 * Activity to be displayed when out of range of any beacons part of the Smart Campus
 * This is the activity that is started when launching the app
 */
public class OutOfRangeActivity extends AppCompatActivity {

    private Logger log;

    /**
     * true iff this activity is in foreground and visible to the user
     */
    private static boolean visible = false;

    /*
    private static OutOfRangeActivity INSTANCE;

    public static OutOfRangeActivity getINSTANCE() {
        return INSTANCE;
    }
    */

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        log = ALogger.getLogger(this, OutOfRangeActivity.class);
        setContentView(R.layout.activity_out_of_range);

        log.debug("onCreate");

        //request permissions if using Android M
        checkPermissions();
    }

    /**
     * Android 6.0 (Marshmallow) requires to request of permissions from user at runtime
     */
    private void checkPermissions() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            ArrayList<String> permissions = new ArrayList<>();
            if (this.checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                log.debug("requesting ACCESS_COARSE_LOCATION");
                permissions.add(Manifest.permission.ACCESS_COARSE_LOCATION);
            }
            if (this.checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                log.debug("requesting WRITE_EXTERNAL_STORAGE");
                permissions.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
            }
            if (permissions.size() > 0) {
                log.info("Requesting permissions: " + permissions.toString());
                String[] perms = new String[permissions.size()];
                perms = permissions.toArray(perms);
                requestPermissions(perms, Config.ASK_MULTIPLE_PERMISSION_REQUEST_CODE);
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        log.debug("onResume");
        visible = true;

        //Maybe show ranging disabled hint
        TextView textViewRangingDisabledHint = (TextView) this.findViewById(R.id.text_view_ranging_disabled_hint);
        if (textViewRangingDisabledHint != null) {
            if (Preferences.isRangingEnabled(this)) {
                textViewRangingDisabledHint.setVisibility(View.GONE);
            } else {
                textViewRangingDisabledHint.setVisibility(View.VISIBLE);
            }
        } else {
            log.error("textview was null!");
        }

        //Check if calibration is in progress and display hint if so
        TextView textViewCalibratingHint = (TextView) this.findViewById(R.id.text_view_calibrating_in_progress_hint);
        if (textViewCalibratingHint != null) {
            if (SettingsActivity.isCalibrating) {
                textViewCalibratingHint.setVisibility(View.VISIBLE);
            } else {
                textViewCalibratingHint.setVisibility(View.GONE);
            }
        } else {
            log.error("textview was null!");
        }

        //Show warning when bluetooth disabled
        View layout = findViewById(R.id.layout_bluetooth_hint);
        if (!Utils.isBluetoothOn()) {

            layout.setVisibility(View.VISIBLE);
            Button bluetoothButton = (Button) findViewById(R.id.button_turn_on_bluetooth);
            bluetoothButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ActivityManager.launchBluetoothEnableActivity(OutOfRangeActivity.this);
                }
            });

        } else {
            layout.setVisibility(View.GONE);
        }

        //Fixme if using no internet hint, also need to update it...
        /**
        //set not internet hint
        TextView internetHintTextView = (TextView) findViewById(R.id.text_view_internet_hint);
        if (Utils.isInternetConnected(this)) {
            internetHintTextView.setVisibility(View.GONE);
        } else {
            internetHintTextView.setVisibility(View.VISIBLE);
        }
         */

        //Check if we joined a smartspace while in background and start respective activity
        if (SmartSpaceProvider.getActiveSmartSpace() != null) {
            log.debug("launching MainActivity");
            ActivityManager.launchMainActivity(this);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        log.debug("onPause");
        visible = false;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    /**
     * Determine if activity is in foreground (ie not paused)
     *
     * @return visibility of activity
     */
    public static boolean isVisible() {
        return visible;
    }

    public String getName() {
        return this.getString(R.string.activity_title_out_of_range_activity);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = this.getMenuInflater();
        inflater.inflate(R.menu.menu_activity_main, menu);
        return true;
    }

    @Override
    public void onBackPressed() {
        log.debug("onBackPressed");
        this.finishAffinity();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_settings:
                Intent intent = new Intent(this, SettingsActivity.class);
                this.startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    /**
     * Can do sth if user grants or refuses permission
     * Currently not used, only for logging
     * @param requestCode
     * @param permissions
     * @param grantResults
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        log.debug("onRequestPermissionsResult: requestCode: " + requestCode + " permissions: " + permissions + " grantResults: " + grantResults);
        if (grantResults.length == 0) {
            log.error("grantResults.length == 0. return.");
            return;
        }
        /*
        switch (requestCode) {
            case Config.PERMISSION_REQUEST_COARSE_LOCATION: {
                int grantResult = grantResults[0];
                if (grantResult == PackageManager.PERMISSION_GRANTED) {
                    log.debug("ACCESS_COARSE_LOCATION permission GRANTED");
                } else {
                    log.debug("ACCESS_COARSE_LOCATION permission DENIED");
                    //TODO terminate app when user doesn't give permissions
                }
                break;
            }
            case Config.PERMISSION_REQUEST_WRITE_EXTERNAL_STORAGE: {
                int grantResult = grantResults[0];
                if (grantResult == PackageManager.PERMISSION_GRANTED) {
                    log.debug("WRITE_EXTERNAL_STORAGE permission GRANTED");
                } else {
                    log.debug("WRITE_EXTERNAL_STORAGE DENIED");
                    //TODO terminate app when user doesn't give permissions
                }
                break;
            }
            default: {
                break;
            }
        }
        */

        if (requestCode == Config.ASK_MULTIPLE_PERMISSION_REQUEST_CODE) {
            for (int i = 0; i < permissions.length; i++) {
                String perm = permissions[i];
                int grantResult = grantResults[i];
                switch (perm) {
                    case (Manifest.permission.WRITE_EXTERNAL_STORAGE): {
                        if (grantResult == PackageManager.PERMISSION_GRANTED) {
                            log.info("WRITE_EXTERNAL_STORAGE permission GRANTED");
                        }
                        break;
                    }
                    case (Manifest.permission.ACCESS_COARSE_LOCATION): {
                        if (grantResult == PackageManager.PERMISSION_GRANTED) {
                            log.info("ACCESS_COARSE_LOCATION permission GRANTED");
                        }
                        break;
                    }
                    default: {
                        break;
                    }
                }
            }
        }
    }
}
