package org.biver.smartcampus.activities;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;

import org.biver.smartcampus.R;
import org.biver.smartcampus.WifiP2PBroadCastReceiver;
import org.biver.smartcampus.core.SmartSpaceProvider;
import org.biver.smartcampus.fragments.ChatFragment;
import org.biver.smartcampus.fragments.FileSpaceFragment;
import org.biver.smartcampus.fragments.MainFragment;
import org.biver.smartcampus.services.chat.MyXMPP;
import org.biver.smartcampus.utils.ALogger;
import org.biver.smartcampus.utils.Config;
import org.biver.smartcampus.utils.Preferences;
import org.biver.smartcampus.utils.Toaster;
import org.biver.smartcampus.utils.UriToFilePathConverter;
import org.biver.smartcampus.utils.Utils;
import org.apache.log4j.Logger;
import org.jxmpp.jid.EntityFullJid;
import org.jxmpp.jid.impl.JidCreate;
import org.jxmpp.stringprep.XmppStringprepException;

/**
 * MainActivity of the Smart Campus Android application
 * This activity holds all the availalbe services and displays relevant fragments when a Smart Space is joined
 */
public class MainActivity extends AppCompatActivity {

    /**
     * Logger instance
     */
    private Logger log;

    private static MainActivity INSTANCE;

    private static boolean visible = false;

    /**
     * Identifying the receiver of a file transfer when onActivityResult is called
     */
    private String currentFileReceiverJidString;
    private String currentFileReceiverNickNameString;

    public static MainActivity getINSTANCE() {
        return INSTANCE;
    }


    @Override
    public void startActivityForResult(Intent intent, int requestCode, Bundle options) {
        super.startActivityForResult(intent, requestCode, options);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        //new MyWifiP2PManager(this).startWithEmptyReceiver();
        //new WifiP2PServer().start(this);

        this.getWindow().requestFeature(Window.FEATURE_ACTION_BAR_OVERLAY);

        super.onCreate(savedInstanceState);
        log = ALogger.getLogger(this, MainActivity.class);
        setContentView(R.layout.activity_main_layout);

        INSTANCE = this;

        if (SmartSpaceProvider.getActiveSmartSpace() == null) {
            log.error("Created MainActivity but no smart space set!? Exiting.");
            return;
        }


        setSmartSpaceTitle(SmartSpaceProvider.getActiveSmartSpace().getName());

        // get fragment manager
        FragmentManager fm = getFragmentManager();

        // add
        FragmentTransaction ft = fm.beginTransaction();
        ft.add(R.id.fragment_container, new MainFragment());
        ft.commit();

    }

    /**
     * Set the title of secondary actionbar
     * @param title The string to be set as title.
     */
    public void setSmartSpaceTitle(final String title) {
        //Set name of active smart space
        TextView textView = (TextView) findViewById(R.id.text_view_space_identifier);
        textView.setText(title);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_activity_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            //3 dots settings button
            case R.id.action_settings: {
                log.debug("onOptionsItemSelected " + "R.id.action_settings");
                Intent intent = new Intent(this, SettingsActivity.class);
                this.startActivity(intent);
                return true;
            }
            // Back arrow in actionbar
            case android.R.id.home: {
                log.debug("onOptionsItemSelected " + "android.R.id.home");

                //Minimize keyboard when going back from ChatFragment??
                if (ChatFragment.visible()) {
                    ((InputMethodManager) this.getSystemService(Context.INPUT_METHOD_SERVICE)).hideSoftInputFromWindow(getWindow().getDecorView().getWindowToken(), 0);
                }

                if (!getFragmentManager().popBackStackImmediate()) {
                    moveTaskToBack(true);
                }
                return true;
            }
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        log.debug("onBackPressed");
        if (!this.getFragmentManager().popBackStackImmediate()) {

            moveTaskToBack(true);

        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        log.debug("onResume");
        visible = true;

        if (SmartSpaceProvider.getActiveSmartSpace() == null) {
            log.debug("No active smart space. Launching OutOfRangeActivity.");
            ActivityManager.launchOutOfRangeActivity(this);
        }


    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        log.debug("onNewIntent");
        String fragmentToOpen = intent.getStringExtra(Config.INTENT_EXTRA_FRAGMENT_TO_OPEN);
        if (fragmentToOpen != null && !fragmentToOpen.isEmpty()) {

            if (fragmentToOpen.equals(Config.SERVICE_TYPES.CHAT)) {

                //switch to group cht fragment
                Fragment fragmentToChangeTo = new ChatFragment();
                FragmentManager fm = this.getFragmentManager();
                fm.beginTransaction()
                        .replace(R.id.fragment_container, fragmentToChangeTo)
                        .addToBackStack(null)
                        .commit();
            } else if (fragmentToOpen.equals(Config.SERVICE_TYPES.PERSONAL_CHAT)) {
                try {
                    EntityFullJid jid = JidCreate.entityFullFrom(intent.getStringExtra("personal_chat_receiver"));
                    String receiverNick = intent.getStringExtra("personal_chat_receiver_nick");
                    Utils.launchPersonalChatFragment(this, jid.toString(), receiverNick);
                } catch (XmppStringprepException e) {
                    log.error("Exception launching personal cht intetn", e);
                }
            }
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        log.debug("onPause");
        visible = false;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        log.debug("onDestroy");
    }


    /**
     * Is activity active and in foreground (visible to the user)
     * @return true iff app is visible
     */
    public static boolean isVisible() {
        return visible;
    }

    /**
     * Get the human readable name of this activity (can be displayed i ui)
     * @return
     */
    public String getName() {
        return this.getString(R.string.activity_title_main_activity);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, final Intent data) {
        log.info("onActivityResult " + "requestCode: " + requestCode + " resultCode: " + resultCode);

        switch (requestCode) {
            //file selected in send file service
            case Config.FILE_SELECT_CODE_FILE_SHARING : {
                if (resultCode == RESULT_OK) {

                    final Uri uri = data.getData();
                    final String filePath = UriToFilePathConverter.getFilePathFromUri(MainActivity.this, data.getData());

                    log.debug("Selected file: " + "uri: " + uri + " filepath: " + filePath);

                    LayoutInflater inflater = MainActivity.this.getLayoutInflater();

                    final View view = inflater.inflate(R.layout.file_transfer_dialog_view, null);

                    ((TextView) view.findViewById(R.id.text_view_file_name)).setText(filePath);


                    AlertDialog dialog = new AlertDialog.Builder(MainActivity.this)
                            .setTitle("Send file to " + currentFileReceiverNickNameString)
                            .setView(view)
                            .setPositiveButton("SEND", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {

                                    final String descriptionText = ((EditText) view.findViewById(R.id.edit_text_file_transfer_description)).getText().toString();

                                    //Start sending the file
                                    new Thread(new Runnable() {
                                        @Override
                                        public void run() {


                                            //getPathFromUri only works on one device and not on the other, hence further processing... (hack)
                                            //FIXME need better and more sophisticated method to determine file
                                            //if (filePath.contains(":")) {
                                            //    filePath = filePath.substring(filePath.indexOf(':') + 1, filePath.length());
                                            //    filePath = Environment.getExternalStorageDirectory() + "/" +  filePath;
                                            //}
                                            MyXMPP.getINSTANCE().sendFileViaXMPP(filePath, descriptionText, MainActivity.this.currentFileReceiverJidString);
                                        }
                                    }).start();
                                }
                            })
                            .setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    //dialog should be dismissed autoatically
                                }
                            })
                            .create();

                    dialog.show();

                }
                break;
            }
            // file selected in file space upload
            case Config.FILE_SELECT_CODE_FILE_SPACE : {
                if (resultCode == RESULT_OK) {
                    log.debug("onActivityResult selected file for file space");
                    String filePath = UriToFilePathConverter.getFilePathFromUri(this, data.getData());
                    log.debug("Uri: " + data.getData() + " filepath: " + filePath);

                    FileSpaceFragment.setAttachementFilePath(filePath);
                    FileSpaceFragment.updateAttachementPathInUi(MainActivity.this);

                    //FIXME need better and more sophisticated method to determine file

                }
                break;
            }
            //Picture taken with intent in file space
            case Config.INTENT_ID_TAKE_PICTURE: {

                if (resultCode == RESULT_OK) {
                    log.debug("Photo taken successfully. Setting attachement name.");
                    Toaster.toast(this, "Photo taken successfully");

                    FileSpaceFragment.updateAttachementPathInUi(MainActivity.this);

                    //String filePath = getPathFromUri(data.getData());
                    //FileSpaceFragment.getINSTANCE().setAttachementFilePath(filePath);

                    //FIXME need better and more sophisticated method to determine file
                }
                break;
            }
            default:
                break;
        }

    }



    /**
     * Call this method to set the current receiver of a file transaction
     * Needed because no additional data can be passed between calling system file chooser and onActivityResult
     * @param jid The receiver jid
     */
    public void setCurrentFileReceiverJidString(String jid, String nick) {
        log.debug("setCurrentFileReceiverJidString " + jid);
        this.currentFileReceiverJidString = jid;
        this.currentFileReceiverNickNameString = nick;
    }

    /**
     * Call this method to set the current receiver of a file transaction
     * Needed because no additional data can be passed between calling system file chooser and onActivityResult
     * @param jid The receiver jid
     */
    public void setCurrentFileReceiverJidString(String jid) {
        log.debug("setCurrentFileReceiverJidString " + jid);
        this.currentFileReceiverJidString = jid;
    }

    public void setCurrentFileReceiverNickNameString(String nick) {
        this.currentFileReceiverNickNameString = nick;
    }

    /**
     * Sets up the actionbar (in case we switch fragments)
     * @param title Title to be displayed
     * @param backArrowEnabled Show the default back button in top left
     *                         (This should not be displayed when in main context and there is no back action)
     */
    public void setActionBarTitle(final String title, final boolean backArrowEnabled) {
        ActionBar actionBar = this.getSupportActionBar();
        actionBar.setTitle(title);
        actionBar.setDisplayHomeAsUpEnabled(backArrowEnabled);
    }

    /**
     * Update the distance indicator element with new distance
     * @param distance The estimated distance from beacon to phone in meters
     */
    public void updateDistanceTextView(final double distance) {

        final boolean hasDistanceIndicator = Preferences.hasDistanceIndicator(MainActivity.this);
        String textViewString = "";
        //If distance has not been set -> current beacon has not been ranged -> notify this in ui
        if (distance == 0) {
            textViewString = MainActivity.this.getString(R.string.distance_string_out_of_range);
        } else {
            final String strDistance = String.format("%.2f", distance);
            textViewString = MainActivity.this.getString(R.string.distance_string, strDistance);
        }

        final String fTextViewString = textViewString;
        this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                TextView textView = (TextView) MainActivity.this.findViewById(R.id.text_view_distance);
                //FIXME maybe check this preference already earlier
                if (hasDistanceIndicator) {
                    textView.setVisibility(View.VISIBLE);
                    textView.setText(fTextViewString);
                } else {
                    textView.setVisibility(View.GONE);
                }
            }
        });
    }

}
