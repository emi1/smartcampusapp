package org.biver.smartcampus.activities;

import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.RemoteException;
import android.preference.EditTextPreference;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceManager;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;

import org.biver.smartcampus.core.SmartSpaceProvider;
import org.biver.smartcampus.utils.AutoUpdateApk;

import org.biver.smartcampus.BuildConfig;
import org.biver.smartcampus.R;
import org.biver.smartcampus.core.BeaconScannerApplication;
import org.biver.smartcampus.core.MyNotificationManager;
import org.biver.smartcampus.services.chat.ChatUtils;
import org.biver.smartcampus.services.chat.MyXMPP;
import org.biver.smartcampus.utils.ALogger;
import org.biver.smartcampus.utils.Config;
import org.biver.smartcampus.evaluation.Evaluator;
import org.biver.smartcampus.utils.Preferences;
import org.biver.smartcampus.utils.Toaster;
import org.altbeacon.beacon.BeaconManager;
import org.apache.log4j.Logger;
import org.jivesoftware.smack.SmackException;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smackx.muc.MultiUserChat;
import org.jivesoftware.smackx.muc.MultiUserChatException;
import org.jxmpp.jid.EntityFullJid;
import org.jxmpp.jid.parts.Resourcepart;
import org.jxmpp.stringprep.XmppStringprepException;

import java.io.File;
import java.util.ArrayList;

/**
 * PreferenceActivity for settings that the user can change
 */
public class SettingsActivity extends PreferenceActivity {

    private Logger log;

    private static boolean visible;

    public static boolean isCalibrating;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        log = ALogger.getLogger(this, SettingsActivity.class);

        setUpActionBar();

        //load the standard preferences
        this.addPreferencesFromResource(R.xml.preferences);

        //set them up
        setupPreferences();


        //show and setup advanced settings iff we are in developing mode
        if (!Config.IS_DEMO) {
            this.addPreferencesFromResource(R.xml.preferences_advanced);
            setupAdvancedPreferences();
        }

        //setup the buttons for technical evaluation
        if (Config.HAS_TECHICAL_EVALUATION) {
            this.addPreferencesFromResource(R.xml.preferences_evaluation);

            //only while developing
            setupStatistics();
            setupCalibrator();

            setupEvaluationForm();

        }
    }



    @Override
    protected void onResume() {
        super.onResume();
        visible = true;
    }

    @Override
    protected void onPause() {
        super.onPause();
        visible = false;
    }

    public static boolean isVisible() {
        return visible;
    }

    public String getName() {
        return this.getString(R.string.activity_title_settings_activity);
    }

    /**
     * Sets up functionality of individual preferences that are being displayed
     * Each preference/button has its own independent setup method
     */
    private void setupPreferences() {
        setUpSendLogToDevPreference();
        setUpCheckForUpdatePreference();
        setUpUserNamePreference();
        setupEnableScanningPreference();
        setupSmartSpaceNotificationPreference();

        setupUserProfilButton();



    }

    /**
     * Sets up the advanced configuration parameters.
     * Can only be called after addPreferencesFromResource(R.xml.preferences_advanced) has been called
     */
    private void setupAdvancedPreferences() {
        setupScanPeriod();
        setupBetweenScanPref();
        setupRegionExitTimeout();

        setupBackgroundModeSwitcher();
        setupBackgroundScanPeriod();
        setupBetweenBackgroundScanPeriod();

        setupKillAppButton();
        setupResetSettings();


    }

    /**
     * Button to open activity where user can edit user profiles
     */
    private void setupUserProfilButton() {
        final Preference userProfilButton = this.getPreferenceScreen().findPreference(this.getString(R.string.pref_key_set_user_profile));
        if (userProfilButton == null) return;

        userProfilButton.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {

                //Abort if we are not joined to a Smart Space currently, because can't update profile then
                if (MyXMPP.getINSTANCE() == null || MyXMPP.getINSTANCE().getSmartSpaceMuc() == null) {
                    Toaster.toast(SettingsActivity.this, "Can only change profile when connected to a Smart Space");
                    return true;
                } else {
                    Intent intent = new Intent(SettingsActivity.this, UserProfileActivity.class);
                    SettingsActivity.this.startActivity(intent);
                    return true;                }
            }
        });

    }


    /**
     * Open web url to google forms evaluation questionnaire
     */
    private void setupEvaluationForm() {

        final Preference evaluationLinkPref = this.getPreferenceScreen().findPreference("pref_evaluation_weblink");
        if (evaluationLinkPref == null) return;

        final String googleFormsUrl = SettingsActivity.this.getString(R.string.evaluation_google_forms_weblink);

        evaluationLinkPref.setSummary("Open online questionnaire \n" + googleFormsUrl);

        evaluationLinkPref.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {

                ActivityManager.launchWebUrl(SettingsActivity.this, googleFormsUrl);
                return true;
            }
        });

    }

    /**
     * Setup a button for tracking statistics
     * Only use while developing
     */
    private void setupStatistics() {
        final Preference statsPref = this.getPreferenceScreen().findPreference("pref_statistics");
        if (statsPref == null) return;

        statsPref.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                Toaster.toastShort(SettingsActivity.this, "Taking statistics, this may take a while...");

                log.debug("Start taking stats");
                //Evaluator.reset();
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        new Evaluator(SettingsActivity.this, BeaconScannerApplication.getINSTANCE().getBeaconManager()).evaluateAll();
                    }
                }).start();

                return true;
            }
        });

    }

    /**
     * Configure the button to do the calibration proedure
     */
    private void setupCalibrator() {
        final Preference prefCalibrate = this.getPreferenceScreen().findPreference("pref_calibrate");
        if (prefCalibrate == null) return;

        if (isCalibrating) {
            prefCalibrate.setSummary("Calibrating in progress. Please wait...");
        } else {
            prefCalibrate.setSummary("Find good scanning parameters. This may take a while.");
        }

        prefCalibrate.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                if (isCalibrating) {
                    return false;
                }
                Toaster.toastShort(SettingsActivity.this, "Calibrating, this may take a while...");

                log.debug("onClick calibrate");

                isCalibrating = true;
                prefCalibrate.setSummary("Calibrating in progress. Please wait...");
                //Evaluator.reset();
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        Evaluator evaluator = new Evaluator(SettingsActivity.this, BeaconScannerApplication.getINSTANCE().getBeaconManager());
                        evaluator.calibrate();

                        SettingsActivity.this.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                prefCalibrate.setSummary("Calibration done.");
                                isCalibrating = false;
                            }
                        });
                    }
                }).start();


                return true;
            }
        });
    }

    private void setupSmartSpaceNotificationPreference() {
        Preference preferenceSmartSpaceNotifications = this.getPreferenceScreen().findPreference(this.getString(R.string.pref_key_smart_space_notification));
        if (preferenceSmartSpaceNotifications == null) return;

        preferenceSmartSpaceNotifications.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                final Boolean newValueBoolean = (Boolean) newValue;

                //Remove notification if required or create a new one if we are in a smart space
                if (newValueBoolean) {
                    if (SmartSpaceProvider.getActiveSmartSpace() != null) {
                        (new MyNotificationManager(SettingsActivity.this)).notifySmartSpace(SmartSpaceProvider.getActiveSmartSpace().getName(), true);
                    }
                } else {
                    (new MyNotificationManager(SettingsActivity.this)).clearSmartSpaceNotification();
                }
                return true;
            }
        });
    }

    /**
     * Setup the preference to enable/disable ble ranging completely
     */
    private void setupEnableScanningPreference() {

        //disable scanning button
        Preference preferenceDisableScanning = this.getPreferenceScreen().findPreference(this.getString(R.string.pref_key_scanning_enabled));
        if (preferenceDisableScanning == null) return;

        preferenceDisableScanning.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                final Boolean newValueBoolean = ((Boolean) newValue);
                log.debug("Changed pref_key_scanning_enabled. New value: " + newValueBoolean);
                BeaconManager beaconManager = BeaconScannerApplication.getINSTANCE().getBeaconManager();
                try {
                    if (newValueBoolean) {
                        beaconManager.startRangingBeaconsInRegion(Config.BEACON_REGION);
                        Toaster.toast(SettingsActivity.this, "Enabled ranging.");
                    } else {
                        beaconManager.stopRangingBeaconsInRegion(Config.BEACON_REGION);
                        //BeaconScannerApplication.getINSTANCE().forceExit();
                        Toaster.toast(SettingsActivity.this, "Disabled ranging. The app is now useless.");
                    }
                } catch (RemoteException e) {
                    log.error("RemoteException when starting/stoping ranging.", e);
                }
                return true;
            }
        });

    }

    /**
     * Set clicklistener to kill app button
     */
    private void setupKillAppButton() {

        //kill app button
        Preference preferenceKill = this.getPreferenceScreen().findPreference(this.getString(R.string.pref_key_kill_app));
        preferenceKill.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                killApp();
                return true;
            }
        });
    }

    /**
     * Removes all notifications, finishes all activity and kills the process.
     */
    private void killApp() {
        log.warn("USER INITIATED APP KILL!");
        new MyNotificationManager(SettingsActivity.this).clearAllNotifications();

        //finish SettingsActivity
        SettingsActivity.this.finish();

        //finish MainActivity
        if (MainActivity.getINSTANCE() != null) {
            MainActivity.getINSTANCE().finish();
        }


        //finish OutOfRangeActivity
        //if (OutOfRangeActivity.isVisible()) {
        //    OutOfRangeActivity.getINSTANCE().finish();
        //}

        AutoUpdateApk.clearNotification(this);

        //Kill whole process
        android.os.Process.killProcess(android.os.Process.myPid());
    }

    /**
     * Preference of username that will be used in XMPP muc
     */
    private void setUpUserNamePreference() {
        //get the preference
        final EditTextPreference preferenceUserName = (EditTextPreference) this.getPreferenceScreen().findPreference(getResources().getString(R.string.pref_key_xmpp_user_name));
        if (preferenceUserName == null) return;

        //Display current selected username in summary
        String userName = Preferences.getDisplayedChatUserName(this);
        preferenceUserName.setSummary(userName);

        //Do not show the dialog to edit the username when not connected to a Smart Space
        preferenceUserName.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                //Abort if we are not joined to a Smart Space currently, because can't update nickname then
                if (MyXMPP.getINSTANCE() == null || MyXMPP.getINSTANCE().getSmartSpaceMuc() == null) {
                    preferenceUserName.getDialog().dismiss();
                    Toaster.toast(SettingsActivity.this, "Can only change nickname when connected to a Smart Space");
                    return true;
                } else {
                    return false;
                }
            }
        });


        //FIXME VERY MESSY
        preferenceUserName.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                log.debug("onPreferenceChange " + preference.getKey() + " new value: " + (String) newValue);

                //Abort if we are not joined to a Smart Space currently, because can't update nickname then
                if (MyXMPP.getINSTANCE() == null || MyXMPP.getINSTANCE().getSmartSpaceMuc() == null) {
                    Toaster.toast(SettingsActivity.this, "Can only change nickname when connected to a Smart Space");
                    return false;
                }


                //Get old username that is/was stored in preferences
                //Fixme can't get this from preference directly?
                SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(SettingsActivity.this);
                String oldNickName = prefs.getString(preference.getKey(), ChatUtils.getDefaultUserNameWithShortUniqueIdentifier(SettingsActivity.this));

                String newNickName = (String) newValue;

                //check if nickname too long
                if (newNickName.length() > 20) {
                    Toaster.toast(SettingsActivity.this, SettingsActivity.this.getString(R.string.user_name_too_long_hint));
                    return false;
                }

                //Get the muc and list of all joined users
                MultiUserChat muc = MyXMPP.getINSTANCE().getSmartSpaceMuc();
                ArrayList<EntityFullJid> allUsers = new ArrayList<>(muc.getOccupants());

                //remove own name since getOccupants includes your own username
                EntityFullJid toRemove = null;
                for (EntityFullJid jid : allUsers) {
                    if (muc.getOccupant(jid).getNick().toString().contains(oldNickName)) {
                        toRemove = jid;
                    }
                }
                allUsers.remove(toRemove);

                //Check if any of the remaining user's nickname is equal to your new desired username
                for (EntityFullJid jid : allUsers) {
                    if (muc.getOccupant(jid).getNick().toString().equals((newNickName))) {
                        //nickname already in use
                        Toaster.toast(SettingsActivity.this, "Nickname already in use!");
                        return false;
                    }
                }

                //Return if the oldNickname is the same as new one
                if (oldNickName.equals(newNickName)) {
                    return true;
                }

                //Change nickname in muc
                try {
                    Resourcepart nick = Resourcepart.from(newNickName);
                    muc.changeNickname(nick);


                } catch (MultiUserChatException.MucNotJoinedException e) {
                    log.error("MultiUserChatException.MucNotJoinedException " ,e);
                } catch (InterruptedException e) {
                    log.error("InterruptedException " ,e);
                } catch (XMPPException.XMPPErrorException e) {
                    //Nick is not available
                    //reset settings to old one
                    log.error("XMPPException.XMPPErrorException new nickname might not be available." ,e);
                    //prefs.edit().putString(preference.getKey(), oldNickName).apply();
                    Toaster.toast(SettingsActivity.this, "Nickname not available!");
                    return false;
                } catch (XmppStringprepException e) {
                    log.error("XmppStringprepException " ,e);
                } catch (SmackException.NotConnectedException e) {
                    log.error("SmackException.NotConnectedException " ,e);
                } catch (SmackException.NoResponseException e) {
                    log.error("SmackException.NoResponseException " ,e);
                }

                //update summary displayed in ui
                preferenceUserName.setSummary(newNickName);

                return true;
            }
        });
    }

    /**
     * Setup preference to check if there is an update available on server
     */
    private void setUpCheckForUpdatePreference() {
        //get versionName and versionCode
        PackageInfo pInfo = null;
        try {
            pInfo = getPackageManager().getPackageInfo(this.getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e) {
            log.error("NameNotFoundException", e);
        }

        //Display information about current version
        String versionName = pInfo.versionName;
        int versionCode = pInfo.versionCode;
        Preference preferenceVersion = this.getPreferenceScreen().findPreference(getResources().getString(R.string.pref_key_version));

        String buildString = versionName;
        buildString += "-";
        buildString += (Config.IS_DEMO) ? "demo" : "dev";
        buildString += "-";
        buildString += BuildConfig.BUILD_TYPE;

        //String phoneId = "Phone Id: " + Utils.getUniquePhoneId(this);

        final String fullVersionInfoString = "Version: " + buildString + "\nVersionCode: " + versionCode;

        preferenceVersion.setSummary(fullVersionInfoString);
        preferenceVersion.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                //new AutoUpdateApk(SettingsActivity.this, "apkUpdate", Config.HOST).checkUpdatesManually();
                ActivityManager.launchPlayStoreIntent(SettingsActivity.this);
                return true;
            }
        });
    }

    /**
     * Setup the preference to send the log file to developer
     * This should open the email app on user's phone with the file attached
     */
    private void setUpSendLogToDevPreference() {
        //Clicklistener for send log to dev function
        Preference prefSendLog = this.getPreferenceScreen().findPreference(getResources().getString(R.string.pref_key_send_log_to_dev));
        if (prefSendLog == null) return;

        //cannot set summary from xml because concatenation of 2 strings
        prefSendLog.setSummary(this.getString(R.string.summary_contact_developer, this.getString(R.string.dev_email)));

        prefSendLog.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                lauchEmailIntentWithLogFileAttached();
                return true;
            }
        });
    }

    /**
     * Open user's email app, attach logfile and put dev mail as recipient
     */
    private void lauchEmailIntentWithLogFileAttached() {

        File file = new File(Preferences.getLogFile(this));
        if (!file.exists() || !file.canRead()) {
            log.error("Cannot read log file");
            Toaster.toast(this, "Attachement error");
            return;
        }
        Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts("mailto", this.getString(R.string.dev_email), null));
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, this.getString(R.string.app_name));
        Uri uri = Uri.fromFile(file);
        emailIntent.putExtra(Intent.EXTRA_STREAM, uri);
        startActivity(Intent.createChooser(emailIntent, this.getString(R.string.text_select_email_app)));
    }

    /**
     * Configure the actionbar
     */
    private void setUpActionBar() {
        //Setup toolbar
        LinearLayout root = (LinearLayout)findViewById(android.R.id.list).getParent().getParent().getParent();
        Toolbar bar = (Toolbar) LayoutInflater.from(this).inflate(R.layout.preferences_toolbar, root, false);
        root.addView(bar, 0); // insert at top

        //make title text color white
        bar.setTitleTextColor(Color.WHITE);
        //make back arrow white
        final Drawable upArrow = getResources().getDrawable(R.drawable.abc_ic_ab_back_mtrl_am_alpha);
        upArrow.setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_ATOP);
        bar.setNavigationIcon(upArrow);
        //back arrow click should stop activity
        bar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }



    /**
     * The pref_key_reset preference should be a button to reset all settings to the default values.
     */
    private void setupResetSettings() {
        Preference prefReset = this.getPreferenceScreen().findPreference(this.getString(R.string.pref_key_reset));
        if (prefReset == null) return;

        prefReset.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {

                //confirm dialog
                new AlertDialog.Builder(SettingsActivity.this)
                        .setTitle("Confirm")
                        .setMessage("Delete all local preferences? This is irreversible. App will restart.")
                        .setNegativeButton(R.string.dialog_button_negative, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                            }
                        })
                        .setPositiveButton(R.string.dialog_button_positive, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                //Clear sharedpreferences
                                PreferenceManager.getDefaultSharedPreferences(SettingsActivity.this)
                                        .edit()
                                        .clear()
                                        .commit();

                                //schedule activity re-launch with alarm manager (activity will be killed)
                                Intent intent = new Intent(SettingsActivity.this, OutOfRangeActivity.class);
                                int mPendingIntentId = Config.INTENT_ID_RESTART;
                                PendingIntent mPendingIntent = PendingIntent.getActivity(SettingsActivity.this, mPendingIntentId, intent, PendingIntent.FLAG_CANCEL_CURRENT);
                                AlarmManager mgr = (AlarmManager)SettingsActivity.this.getSystemService(Context.ALARM_SERVICE);
                                mgr.set(AlarmManager.RTC, System.currentTimeMillis() + 500, mPendingIntent);

                                //kill the app
                                killApp();
                            }
                        })
                        .create()
                        .show();

                return true;
            }
        });
    }



    private void setupBetweenScanPref() {
        Preference prefBetweenScanPeriod = this.getPreferenceScreen().findPreference(getResources().getString(R.string.pref_key_between_scan_period));
        if (prefBetweenScanPeriod == null) return;

        prefBetweenScanPeriod.setSummary(Integer.toString(Preferences.getBetweenScanPeriond(this)) + "ms");

        //update all preferences regardless what preferences has been changed
        prefBetweenScanPeriod.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                log.debug("between_scan_period set to : " + (String) newValue);

                //update ui
                preference.setSummary((String) newValue + "ms");

                BeaconManager beaconManager = BeaconScannerApplication.getINSTANCE().getBeaconManager();

                //Update beaconmanager's between scan period with the new value
                beaconManager.setForegroundBetweenScanPeriod(Integer.valueOf((String) newValue));

                try {
                    beaconManager.updateScanPeriods();
                } catch (RemoteException e) {
                    log.error("RemoteException: ", e);
                }

                return true;
            }
        });
    }

    private void setupScanPeriod() {
        Preference prefScanPeriod = this.getPreferenceScreen().findPreference(getResources().getString(R.string.pref_key_scan_period));
        if (prefScanPeriod == null) return;

        prefScanPeriod.setSummary(Integer.toString(Preferences.getScanPeriod(this)) + "ms");


        //update all preferences regardless what preferences has been changed
        prefScanPeriod.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                log.debug("scan_period set to : " + (String) newValue);

                //update ui
                preference.setSummary((String) newValue + "ms");

                BeaconManager beaconManager = BeaconScannerApplication.getINSTANCE().getBeaconManager();

                //Update beaconmanager's scan period with the new value
                beaconManager.setForegroundScanPeriod(Integer.valueOf((String) newValue));

                try {
                    beaconManager.updateScanPeriods();
                } catch (RemoteException e) {
                    log.error("RemoteException: ", e);
                }

                return true;
            }
        });

    }

    private void setupRegionExitTimeout() {
        Preference prefTimeOut = this.getPreferenceScreen().findPreference(this.getString(R.string.pref_key_region_exit_timeout));
        if (prefTimeOut == null) return;

        prefTimeOut.setSummary(Integer.toString(Preferences.getRegionExitTimeOut(this)) + "ms");

        //update all preferences regardless what preferences has been changed
        prefTimeOut.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                log.debug("region_exit_timeout set to : " + (String) newValue);

                //for the exit timeout we only need to update ui value in summary
                //Value doesn't need to be propagated as it used directly from shared preferences
                preference.setSummary((String) newValue + "ms");
                return true;
            }
        });

    }

    /**
     * Setting to go into background mode
     * (Background mode is a setting of the underlying beacon scanning library)
     */
    private void setupBackgroundModeSwitcher() {
        final Preference prefBackgroundScanPeriod = this.getPreferenceScreen().findPreference(this.getString(R.string.pref_key_background_mode));
        if (prefBackgroundScanPeriod == null) return;

        prefBackgroundScanPeriod.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                log.info("onPreferenceChange: pref_key_background_mode. New value: " + newValue);
                BeaconManager beaconManager = BeaconScannerApplication.getINSTANCE().getBeaconManager();

                beaconManager.setBackgroundMode((Boolean) newValue);

                return true;
            }
        });

    }

    /**
     * Scan period for the background mode of scanning library
     */
    private void setupBackgroundScanPeriod() {
        final Preference prefBackgroundScanPeriod = this.getPreferenceScreen().findPreference(this.getString(R.string.pref_key_background_scan_period));
        if (prefBackgroundScanPeriod == null) return;

        prefBackgroundScanPeriod.setSummary(Integer.toString(Preferences.getBackgroundScanPeriod(this)) + "ms");
        prefBackgroundScanPeriod.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                log.debug("backgroun_scan_period set to : " + (String) newValue);

                //set summary
                preference.setSummary((String) newValue + "ms");

                //Update beaconmanager's scan period with the new value
                BeaconManager beaconManager = BeaconScannerApplication.getINSTANCE().getBeaconManager();
                beaconManager.setBackgroundScanPeriod(Integer.valueOf((String) newValue));

                try {
                    beaconManager.updateScanPeriods();
                } catch (RemoteException e) {
                    log.error("RemoteException: ", e);
                }
                return true;
            }
        });
    }

    /**
     * background between scan parameter in scanning library
     */
    private void setupBetweenBackgroundScanPeriod() {
        final Preference prefBetweenBackgroundScanPeriod = this.getPreferenceScreen().findPreference(this.getString(R.string.pref_key_background_between_scan_period));
        if (prefBetweenBackgroundScanPeriod == null) return;

        prefBetweenBackgroundScanPeriod.setSummary(Integer.toString(Preferences.getBackgroundBetweenScanPeriod(this)) + "ms");

        prefBetweenBackgroundScanPeriod.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                log.debug("backgroun_between_scan_period set to : " + (String) newValue);
                //set summary
                preference.setSummary((String) newValue + "ms");


                BeaconManager beaconManager = BeaconScannerApplication.getINSTANCE().getBeaconManager();

                //Update beaconmanager's scan period with the new value
                beaconManager.setBackgroundBetweenScanPeriod(Integer.valueOf((String) newValue));

                try {
                    beaconManager.updateScanPeriods();
                } catch (RemoteException e) {
                    log.error("RemoteException: ", e);
                }
                return true;
            }
        });
    }


}
