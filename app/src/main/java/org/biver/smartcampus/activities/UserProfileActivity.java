package org.biver.smartcampus.activities;

import android.content.Context;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.EditTextPreference;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.text.InputFilter;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ListAdapter;

import org.biver.smartcampus.R;
import org.biver.smartcampus.services.chat.MyXMPP;
import org.biver.smartcampus.utils.ALogger;
import org.biver.smartcampus.utils.InputFilterMinMax;
import org.apache.log4j.Logger;
import org.jivesoftware.smackx.vcardtemp.packet.VCard;

/**
 * Created by emi on 9/10/16.
 */
public class UserProfileActivity extends PreferenceActivity {

    private static boolean visible = false;
    private Logger log;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.log = ALogger.getLogger(this, UserProfileActivity.class);

        this.setUpActionBar();

        this.addPreferencesFromResource(R.xml.user_profile_preferences);

        /*
        final ListAdapter adapter = UserProfileActivity.this.getPreferenceScreen().getRootAdapter();
        for (int i = 0; i < adapter.getCount(); i++) {
            log.debug("Preference of UserProfileActivity " + adapter.getItem(i));
            if (adapter.getItem(i) instanceof Preference) {
                Preference thisPref = (Preference) adapter.getItem(i);



            }
        }
        */


        final ListAdapter adapter = UserProfileActivity.this.getPreferenceScreen().getRootAdapter();

        new AsyncTask<Void, Void, Void>() {

            private VCard vCard = null;

            @Override
            protected Void doInBackground(Void... params) {
                vCard = MyXMPP.getINSTANCE().getMyOwnVCard();
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                for (int i = 0; i < adapter.getCount(); i++) {
                    if (adapter.getItem(i) instanceof Preference) {
                        final Preference pref = ((Preference) adapter.getItem(i));
                        final String valueFromVCard = vCard.getField(pref.getKey());
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                pref.setSummary(valueFromVCard);

                                pref.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
                                    @Override
                                    public boolean onPreferenceChange(Preference preference, Object newValue) {
                                        MyXMPP.getINSTANCE().updateMyOwnVCard(preference.getKey(), (String) newValue);
                                        preference.setSummary((String) newValue);
                                        return true;
                                    }
                                });
                            }
                        });
                    }
                }
            }
        }.execute();

        //Restrictions for age edit text
        ((EditTextPreference) findPreference(this.getString(R.string.user_profile_key_age)))
                .getEditText()
                .setFilters(new InputFilter[]{new InputFilterMinMax(1, 99)});
    }

    @Nullable
    @Override
    public View onCreateView(String name, Context context, AttributeSet attrs) {
        return super.onCreateView(name, context, attrs);
    }

    @Override
    protected void onResume() {
        super.onResume();
        log.debug("onResume");
        visible = true;
    }

    @Override
    protected void onPause() {
        super.onPause();
        log.debug("onPause");
        visible = false;
    }

    public static boolean isVisible() {
        return visible;
    }

    public String getName() {
        return this.getString(R.string.activity_title_user_profile_activity);
    }

    /**
     * Configure the actionbar
     */
    private void setUpActionBar() {
        //Setup toolbar
        LinearLayout root = (LinearLayout) findViewById(android.R.id.list).getParent().getParent().getParent();
        Toolbar bar = (Toolbar) LayoutInflater.from(this).inflate(R.layout.preferences_toolbar, root, false);
        root.addView(bar, 0); // insert at top

        bar.setTitle(this.getName());

        //make title text color white
        bar.setTitleTextColor(Color.WHITE);
        //make back arrow white
        final Drawable upArrow = getResources().getDrawable(R.drawable.abc_ic_ab_back_mtrl_am_alpha);
        upArrow.setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_ATOP);
        bar.setNavigationIcon(upArrow);
        //back arrow click should stop activity
        bar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}
