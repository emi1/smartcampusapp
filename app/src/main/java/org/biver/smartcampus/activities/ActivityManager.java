package org.biver.smartcampus.activities;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;

import org.biver.smartcampus.utils.Config;

import static android.content.Intent.FLAG_ACTIVITY_NEW_TASK;

/**
 * Any activity switching should be done through this class for better oversight about who changes activity when.
 */
public class ActivityManager {

    /**
     * Private constructor to avoid instantiation
     */
    private ActivityManager() {

    }

    /**
     * Start MainActivity
     * @param context Context or Activity to launch this intent from
     */
    public static void launchMainActivity(final Context context) {
        Intent intent = new Intent(context, MainActivity.class);
        intent.setFlags(FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);

        context.startActivity(intent);
    }

    /**
     * Start OutOfRangeActivity
     * @param context Activity where this intent will be launched from
     */
    public static void launchOutOfRangeActivity(final Context context) {
        Intent intent = new Intent(context, OutOfRangeActivity.class);
        intent.setFlags(FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);
    }

    /**
     * Open dialog to let user enable bluetooth
     * @param activity Activity where this intent will be launched from
     */
    public static void launchBluetoothEnableActivity(final Activity activity) {
        Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
        activity.startActivityForResult(enableBtIntent, Config.REQUEST_ENABLE_BT);
    }

    /**
     * Method to display the sysems file chooser dialog
     * @param requestCode Requestcode that will be used in onActivityResult
     */
    public static void launchFileChooserIntent(final Activity activity, final int requestCode) {

        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("*/*");      //all files
        intent.addCategory(Intent.CATEGORY_OPENABLE);

        activity.startActivityForResult(Intent.createChooser(intent, "Select a File to send"), requestCode);
    }

    /**
     * Determine if any activity is visible to the user
     * @return true if an activity is currently in foreground and displayed to user
     */
    public static boolean isAnyActivityVisible() {
        return (MainActivity.isVisible() || OutOfRangeActivity.isVisible() || SettingsActivity.isVisible() || UserProfileActivity.isVisible());
    }

    /**
     * Open an url with browser
     * @param activity
     * @param url
     */
    public static void launchWebUrl(final Activity activity, final String url) {
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
        activity.startActivity(browserIntent);
    }

    public static void launchPlayStoreIntent(final Activity activity) {
        final String appPackageName = activity.getPackageName(); // getPackageName() from Context or Activity object
        try {
            activity.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
        } catch (android.content.ActivityNotFoundException anfe) {
            activity.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
        }
    }
}
