package org.biver.smartcampus.ui;

import android.app.Activity;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import org.biver.smartcampus.R;

/**
 * Abstraction of progress dialog for file transfers
 */
public class TransferProgressDialog {

    private AlertDialog dialog;
    private View view;
    private TextView progressView;
    private ProgressBar progressBar;

    public TransferProgressDialog(Activity activity, String title) {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        LayoutInflater inflater = activity.getLayoutInflater();

        view = inflater.inflate((R.layout.dialog_upload_progress), null);
        progressView = ((TextView) view.findViewById(R.id.text_view_progress));
        progressBar = ((ProgressBar) view.findViewById(R.id.upload_progress));

        //builder.setTitle(activity.getString(R.string.title_upload_progress_dialog));
        builder.setTitle(title);
        builder.setView(view);
        builder.setCancelable(false);

        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //Do nothing here will be overwritten later
            }
        });

        dialog = builder.create();

    }

    /**
     * Show the actual dialog
     */
    public void show() {
        dialog.show();
        dialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
    }

    public void updateStatus(int progress) {
        progressView.setText(Integer.toString(progress));
        progressBar.setProgress(progress);

        //progress == 100 -> upload finished
        //negative progress -> upload failed
        if (progress == 100 || progress < 0) {
            progressView.setText("Transfer successful!");
            dialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });
            dialog.setCancelable(true);
        }
    }

    public void dismisss() {
        dialog.dismiss();
    }

    public void setOnDismissListener(DialogInterface.OnDismissListener listener) {
        dialog.setOnDismissListener(listener);
    }
}
