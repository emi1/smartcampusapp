package org.biver.smartcampus.core;

import android.app.DownloadManager;
import android.content.Context;
import android.net.Uri;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.biver.smartcampus.entities.CampusProximityInformationService;
import org.biver.smartcampus.utils.ALogger;
import org.biver.smartcampus.utils.Config;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.apache.log4j.Logger;
import org.json.JSONException;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * All communication with the server should be handled within this class
 */
public class ServerRequests {

    private Logger log;

    private Context context;


    /**
     * Create an instance with context and create a logger
     * @param context
     */
    public ServerRequests(Context context) {
        this.context = context;
        log = ALogger.getLogger(context, ServerRequests.class);
    }
    /**
     *
     * @param instanceId The instance id that we want to check for existing services.
     *                   Id in hexadecimal string form, without 0x prefix
     * @return
     */
    public String getServices(String instanceId) throws IOException{

        String request = "getServices?id=" + instanceId;

        String url = Config.HOST + request;

        DefaultHttpClient httpClient = new DefaultHttpClient();
        HttpGet httpGet = new HttpGet(url);

        HttpResponse httpResponse = httpClient.execute(httpGet);

        HttpEntity httpEntity = httpResponse.getEntity();
        String output = EntityUtils.toString(httpEntity);

        return output;

    }

    public String getUserServices(String instanceId) throws IOException{

        String request = "getUserSources?id=" + instanceId;

        String url = Config.HOST + request;

        DefaultHttpClient httpClient = new DefaultHttpClient();
        HttpGet httpGet = new HttpGet(url);

        HttpResponse httpResponse = httpClient.execute(httpGet);

        HttpEntity httpEntity = httpResponse.getEntity();
        String output = EntityUtils.toString(httpEntity);

        return output;

    }

    /**
     * Gets the files/comments available in the file space of a given instance id
     * @param instanceId The beacon instance id of the desired file space
     * @return The result of the http get request in String format
     * @throws IOException
     */
    public String getFilesInSpace(String instanceId) throws IOException{
        log.debug("getFilesInSpace " + instanceId);

        String output = "";
        String request = "";

        request = "getFilesInSpace?id=" + instanceId;
        String url = Config.HOST + request;

        DefaultHttpClient httpClient = new DefaultHttpClient();
        HttpGet httpGet = new HttpGet(url);

        HttpResponse httpResponse = httpClient.execute(httpGet);

        HttpEntity httpEntity = httpResponse.getEntity();
        output = EntityUtils.toString(httpEntity);

        log.debug("output: " + output);

        return output;

    }

    /**
     * Delete a file in the filespace
     * @param instanceId The instance id of the filespace
     * @param randomId The id of the file to be deleted.
     * @throws IOException
     */
    public void deleteFile(String instanceId, String randomId) throws IOException{
        log.debug("deleteFile " + instanceId + " " + randomId);

        String request = "delete?id=" + instanceId + "&random_id=" + randomId;

        String url = Config.HOST + request;

        DefaultHttpClient httpClient = new DefaultHttpClient();
        HttpGet httpGet = new HttpGet(url);

        HttpResponse httpResponse = httpClient.execute(httpGet);

        HttpEntity httpEntity = httpResponse.getEntity();
        String output = EntityUtils.toString(httpEntity);

        //Fixme maybe return confirmation of deletion?
        //return output;
    }


    /**
     * Delete a file in the filespace
     * @param instanceId The instance id of the filespace
     * @param name The id of the file to be deleted.
     * @throws IOException
     */
    public void deleteUserSource(String instanceId, String name) throws IOException{
        log.debug("deleteUserSource " + instanceId + " " + name);

        String request = "deleteUserSource?id=" + instanceId + "&name=" + name;

        String url = Config.HOST + request;

        DefaultHttpClient httpClient = new DefaultHttpClient();
        HttpGet httpGet = new HttpGet(url);

        HttpResponse httpResponse = httpClient.execute(httpGet);

        HttpEntity httpEntity = httpResponse.getEntity();
        String output = EntityUtils.toString(httpEntity);

        //Fixme maybe return confirmation of deletion?
        //return output;
    }

    /**
     * Uses the Android DownloadManager to download a file
     * @param instanceId The beacon instance id of the current file space
     * @param fileName The filename (without path) of the file to be downloaded.
     */
    public void downloadFileInSpaceWithDownloadManager(String instanceId, String fileName) {
        //Build server path
        Uri uri = Uri.parse(Config.HOST + instanceId + "/" + fileName);
        DownloadManager.Request request = new DownloadManager.Request(uri);
        //Fixme create a folder for each instance id
        request.setDestinationInExternalPublicDir("/" + Config.STORAGE_DIR + "/downloads/", fileName);
        request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);

        DownloadManager downloadManager = (DownloadManager) context.getSystemService(Context.DOWNLOAD_SERVICE);
        downloadManager.enqueue(request);

    }

    public void addInformationSource(final String instanceId, final CampusProximityInformationService service, Response.Listener<String> responseListener) throws JSONException {

        final String serviceJsonString = service.toJSONObject().toString();

        String serviceUrl = Config.HOST + "addUserSource" + "?id=" + instanceId;;

        StringRequest myStringRequest = new StringRequest(
                Request.Method.POST,
                serviceUrl,
                responseListener,
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        log.error("onError");
                    } //Create an error listener to handle errors appropriately.
                })
        {
                protected Map<String, String> getParams() {
                    Map<String, String> data = new HashMap<String, String>();
                    data.put("information_service", serviceJsonString);
                    return data;
                }
        };

        //Bug in volley that sends data twice without these settings
        myStringRequest.setRetryPolicy(new DefaultRetryPolicy(
                0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));


        RequestQueue requestQueue = Volley.newRequestQueue(context);
        requestQueue.add(myStringRequest);
    }
}
