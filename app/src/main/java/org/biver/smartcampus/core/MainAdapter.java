package org.biver.smartcampus.core;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.google.gson.Gson;

import org.biver.smartcampus.R;
import org.biver.smartcampus.entities.CampusProximityInformationService;
import org.biver.smartcampus.entities.CampusServiceInterface;
import org.biver.smartcampus.entities.SmartSpace;
import org.biver.smartcampus.fragments.ChatFragment;
import org.biver.smartcampus.fragments.UserListFragment;
import org.biver.smartcampus.fragments.FileSpaceFragment;
import org.biver.smartcampus.fragments.InformationFragment;
import org.biver.smartcampus.fragments.InformationWebViewFragment;
import org.biver.smartcampus.fragments.UserServicesFragment;
import org.biver.smartcampus.services.chat.MyXMPP;
import org.biver.smartcampus.utils.ALogger;
import org.biver.smartcampus.utils.Config;
import org.biver.smartcampus.utils.Toaster;
import org.apache.log4j.Logger;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

/**
 * Adapter to display available services in main view inside of MainActivity
 * OnClick of elements in the ListView must trigger change to the service's respective fragment
 */
public class MainAdapter extends BaseAdapter {


    private Activity activity;

    private SmartSpace smartSpace;

    private Logger log;

    /**
     * Construct adapter from MainActivty and the joined smartspace
     * @param activity
     * @param smartSpace
     */
    public MainAdapter(Activity activity, SmartSpace smartSpace) {
        this.activity = activity;

        this.log = ALogger.getLogger(activity, MainAdapter.class);

        this.smartSpace = smartSpace;
    }

    @Override
    public int getCount() {
        return smartSpace.getServices().size();
    }

    @Override
    public Object getItem(int position) {
        return smartSpace.getServices().get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final LayoutInflater inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        if (convertView == null) {
            convertView = inflater.inflate(org.biver.smartcampus.R.layout.list_item_main_services_view, null);
        }

        TextView textViewTitle = (TextView) convertView.findViewById(R.id.text_view_file_name);
        final CardView cardView = (CardView) convertView.findViewById(R.id.card_view);

        final CampusServiceInterface currentService = smartSpace.getServices().get(position);

        textViewTitle.setText(currentService.getName());

        //Set OnClickListener to individual list elements to switch to respective fragment
        //get fragment manager
        cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                log.debug("onClick " + currentService.getName());
                Fragment fragmentToChangeTo = null;
                switch (currentService.getName()) {
                    case Config.SERVICE_TYPES.CHAT : {
                        if (MyXMPP.getINSTANCE().getSmartSpaceMuc() != null) {
                            fragmentToChangeTo = new ChatFragment();
                        }

                        break;
                    }
                    /*
                    case Config.SERVICE_TYPES.SEND_FILES : {
                        fragmentToChangeTo = new UserListFragment();

                        break;
                    }
                    */
                    case Config.SERVICE_TYPES.USERS : {
                        //TODO rename
                        fragmentToChangeTo = new UserListFragment();

                        break;
                    }
                    case Config.SERVICE_TYPES.INFORMATION : {
                        fragmentToChangeTo = new InformationFragment();

                        //Information services need to retrieve url in fragment, set serialized information service to bundle of fragent
                        Bundle bundle = new Bundle();
                        final String serializedCampusProximityInformationService = new Gson().toJson(currentService);
                        bundle.putString(Config.INTENT_EXTRA_CAMPUS_PROXIMITY_INFORMATION_SERVICE, serializedCampusProximityInformationService);
                        fragmentToChangeTo.setArguments(bundle);

                        break;
                    }
                    case Config.SERVICE_TYPES.MENSA_MENU : {

                        fragmentToChangeTo = new InformationWebViewFragment();

                        //Information services need to retrieve url in fragment, set serialized information service to bundle of fragent
                        Bundle bundle = new Bundle();
                        final String serializedCampusProximityInformationService = new Gson().toJson(currentService);
                        bundle.putString(Config.INTENT_EXTRA_CAMPUS_PROXIMITY_INFORMATION_SERVICE, serializedCampusProximityInformationService);
                        fragmentToChangeTo.setArguments(bundle);

                        break;
                    }
                    case Config.SERVICE_TYPES.FILESPACE : {
                        fragmentToChangeTo = new FileSpaceFragment();
                        //Toaster.toast(activity, "Warning experimental!!");

                        break;
                    }
                    case Config.SERVICE_TYPES.SHARED_EDITING : {
                        fragmentToChangeTo = new InformationWebViewFragment();


                        //Toaster.toast(activity, "Warning experimental!!");
                        //Information services need to retrieve url in fragment, set serialized information service to bundle of fragent
                        Bundle bundle = new Bundle();
                        final String serializedCampusProximityInformationService = new Gson().toJson(currentService);
                        bundle.putString(Config.INTENT_EXTRA_CAMPUS_PROXIMITY_INFORMATION_SERVICE, serializedCampusProximityInformationService);
                        fragmentToChangeTo.setArguments(bundle);

                        break;
                    }
                    case Config.SERVICE_TYPES.USER_SERVICES : {
                        fragmentToChangeTo = new UserServicesFragment();
                        break;
                    }
                    default: {
                        break;
                    }


                }

                //handle user generated information fragment here
                if (fragmentToChangeTo == null && currentService instanceof CampusProximityInformationService && ((CampusProximityInformationService) currentService).isUserGenerated()) {
                    fragmentToChangeTo = new InformationWebViewFragment();

                    //Information services need to retrieve url in fragment, set serialized information service to bundle of fragent
                    Bundle bundle = new Bundle();
                    final String serializedCampusProximityInformationService = new Gson().toJson(currentService);
                    bundle.putString(Config.INTENT_EXTRA_CAMPUS_PROXIMITY_INFORMATION_SERVICE, serializedCampusProximityInformationService);
                    fragmentToChangeTo.setArguments(bundle);
                }

                //Change to new fragment if it is non-null
                if (fragmentToChangeTo != null) {
                    FragmentManager fm = activity.getFragmentManager();
                    fm.beginTransaction()
                        .replace(R.id.fragment_container, fragmentToChangeTo)
                        .addToBackStack(null)
                        .commit();
                } else {
                    //Clicked on a button that has no implementation yet
                    Toaster.toast(activity, activity.getString(R.string.message_not_implemented_yet));
                }
            }
        });
        return convertView;
    }


    /**
     * update the data displayed by this listadapter
     * @throws IOException
     * @throws JSONException
     */
    public void updateData() throws IOException, JSONException {
        log.debug("updateData " + smartSpace.getBeaconInstanceId());

        String result = "";
        result = (new ServerRequests(activity)).getServices(smartSpace.getBeaconInstanceId());

        log.debug("getServices response: " + result);
        JSONObject jsonResult = new JSONObject(result);
        if (jsonResult.has("error")) {
            log.debug("Received invalid instance id");
        } else {

            //FIXME hack...
            SmartSpace space = SmartSpace.createFromJson(activity, jsonResult);

            //update name in active space
            SmartSpaceProvider.getActiveSmartSpace().setName(space.getName());
            //update services in active space
            SmartSpaceProvider.getActiveSmartSpace().setServices(space.getServices());

            //get updated smart space
            this.smartSpace = SmartSpaceProvider.getActiveSmartSpace();


            //Update data in smartspace without creating a new instance (some things may get lost?)
            //this.smartSpace.setServices(SmartSpaceProvider.getActiveSmartSpace().getServices());
            //this.smartSpace.setName(SmartSpaceProvider.getActiveSmartSpace().getName());

            //Persist updates to other parts of the application
            //SmartSpaceProvider.setActiveSpace(space);


        }

    }
}
