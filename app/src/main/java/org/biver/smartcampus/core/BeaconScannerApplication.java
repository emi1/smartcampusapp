package org.biver.smartcampus.core;

import android.app.Activity;
import android.app.Application;
import android.content.ComponentCallbacks2;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.os.RemoteException;
import android.preference.PreferenceManager;

import org.apache.log4j.net.SyslogAppender;
import org.biver.smartcampus.R;
import org.biver.smartcampus.activities.MainActivity;
import org.biver.smartcampus.activities.OutOfRangeActivity;
import org.biver.smartcampus.entities.SmartSpace;
import org.biver.smartcampus.services.chat.ChatService;
import org.biver.smartcampus.services.chat.MyXMPP;
import org.biver.smartcampus.utils.ALogger;
import org.biver.smartcampus.activities.ActivityManager;
import org.biver.smartcampus.utils.Config;
import org.biver.smartcampus.utils.Preferences;
import org.biver.smartcampus.utils.SmartCampusLogger;
import org.biver.smartcampus.utils.Toaster;
import org.biver.smartcampus.utils.Utils;
import org.altbeacon.beacon.Beacon;
import org.altbeacon.beacon.BeaconConsumer;
import org.altbeacon.beacon.BeaconManager;
import org.altbeacon.beacon.BeaconParser;
import org.altbeacon.beacon.RangeNotifier;
import org.altbeacon.beacon.Region;
import org.apache.log4j.Logger;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;

/**
 * One of the core components of the Smart Campus App
 * Application that always runs in background and scans for BLE beacon ids
 * joins/exits smart spaces when available
 */
public class BeaconScannerApplication extends Application implements BeaconConsumer {

    /**
     * log4j logger
     */
    private Logger log;

    /**
     * instance of beaconmanager
     */
    private BeaconManager beaconManager;

  //  private ChatService chatService;

    private MyChatServiceConnection myChatServiceConnection = new MyChatServiceConnection();

    private class MyChatServiceConnection implements ServiceConnection {

        private boolean isBound;

        @Override
        public void onServiceConnected(final ComponentName name, final IBinder service) {
            log.debug("onServiceConnected");
   //         chatService = ((LocalBinder<ChatService>) service).getService();
            isBound = true;
        }

        @Override
        public void onServiceDisconnected(final ComponentName name) {
            log.debug("onServiceDisconnected");
      //      chatService = null;
            isBound = false;
        }

        public boolean isBound() {
            return isBound;
        }
    };

    public MyChatServiceConnection getMyChatServiceConnection() {
        return myChatServiceConnection;
    }

    private static BeaconScannerApplication INSTANCE;

    private MyRangingNotifier myRangeNotifier;

    public static BeaconScannerApplication getINSTANCE() {
        return INSTANCE;
    }

    public BeaconManager getBeaconManager() {
        return this.beaconManager;
    }

    /**
     * Get instance of the currently used RangeNotifier
     * @return instance of the RangeNotifier
     */
    public MyRangingNotifier getMyRangeNotifier() {
        return myRangeNotifier;
    }


    @Override
    public void onCreate() {
        super.onCreate();
        log = ALogger.getLogger(this, BeaconScannerApplication.class);
        log.info("App started up. MAnufacturer: " + Build.MANUFACTURER + " Model: " + Build.MODEL);


        //possibly remove leftover notifications that survived a previous (unintented) application kill
        (new MyNotificationManager(this)).clearAllNotifications();

        //init logger that can be used globally
        SmartCampusLogger.init(this);

        //BeaconManager.setAndroidLScanningDisabled(true);

        INSTANCE = this;
        beaconManager = BeaconManager.getInstanceForApplication(this);

        //add the eddystone layout
        beaconManager.getBeaconParsers().add(new BeaconParser().setBeaconLayout(BeaconParser.EDDYSTONE_UID_LAYOUT));

        //beaconManager.setDebug(true);

        myRangeNotifier = new MyRangingNotifier();
        beaconManager.addRangeNotifier(myRangeNotifier);


        //Bind beaconmanager to this application
        beaconManager.bind(this);

        //ActivityLifecycleCallbacks
        this.registerActivityLifecycleCallbacks(new MyActivityLifeCycleCallbacks());

        //Very important! The default nickname "testuser" should never be used because it is not unique
        Utils.setupInitialChatUserName(this);

    }

    @Override
    public void onLowMemory() {
        log.debug("onLowMemory");
        super.onLowMemory();
    }

    @Override
    public void onTrimMemory(int level) {
        //convert level to the corresponding static int's name
        String strLevel = "";

        try {
            strLevel = Utils.getIntConstantName(ComponentCallbacks2.class, "TRIM_MEMORY", level);
        } catch (IllegalAccessException e) {
            log.error("IllegalAccessException", e);
        }

        log.debug("onTrimMemory with level: " + level + " " + strLevel);
        super.onTrimMemory(level);
    }

    /**
     * Called when we scanSuccess a beacon id
     * This id is not necessarily tied to a smart space
     * @param scannedId isntance id without 0x
     * @return Returns true iff we joined a space
     */
    private boolean tryToJoinSmartSpace(final String scannedId) {
        log.debug("tryToJoinSmartSpace " + scannedId);

        String result = "";
        try {
            result = (new ServerRequests(this)).getServices(scannedId);
        } catch (IOException e) {
            log.error("IOException in http Get: ", e);

            if (OutOfRangeActivity.isVisible()) {
                Toaster.toastShort(this, "Server error!");
            }
            return false;
        }

        log.debug("getServices response: " + result);
        try {
            JSONObject jsonResult = new JSONObject(result);
            if (jsonResult.has("error")) {
                log.debug("Received invalid instance id");
            } else {
                SmartSpace space = SmartSpace.createFromJson(this, jsonResult);
                joinSmartSpace(space);
                return true;
            }
        } catch (JSONException e) {
            log.error("JSONException: ", e);
        }

        return false;
    }

    /**
     * After scanning id we call this function if we can join this Smart Space
     * @param space
     */
    private void joinSmartSpace(SmartSpace space) {
        log.debug("joinSmartSpace");

        if (SmartSpaceProvider.getActiveSmartSpace() != null) {
            log.warn("joinSmartSpace called with active space != null.");
            return;
        }

        SmartSpaceProvider.setActiveSpace(space);

        //FIXME shouldn't this crash if called when already bound?
        bindService(new Intent(this, ChatService.class), myChatServiceConnection, Context.BIND_AUTO_CREATE);

        //MyXMPP.getINSTANCE().multiUserChatConfigure(SmartSpaceProvider.getActiveSmartSpace().getIdentifier());


        //switch to MainActivity if we just entered beacon region and if app is in foreground
        if (OutOfRangeActivity.isVisible()) {
            log.debug("Launching MainActivity");
            ActivityManager.launchMainActivity(this);

        }

        //persistent notification about smartspace
        final String spaceName = SmartSpaceProvider.getActiveSmartSpace().getName();
        (new MyNotificationManager(this)).notifySmartSpace(spaceName);

        if (ActivityManager.isAnyActivityVisible()) {
            Toaster.toastShort(BeaconScannerApplication.this, "Joined Smart Space " + spaceName);
        }
    }

    public void bindChatConnection() {
        bindService(new Intent(this, ChatService.class), myChatServiceConnection, Context.BIND_AUTO_CREATE);
    }

    /**
     * Public method to exit smartspace
     */
    public void forceExit() {
        exitSmartSpace();
    }

    /**
     * Called upon exiting a beacon region and its associated Smart Space
     */
    private void exitSmartSpace() {
        log.debug("exitSmartSpace");

        if (SmartSpaceProvider.getActiveSmartSpace() != null) {
            SmartSpaceProvider.getActiveSmartSpace().removeExitTimers();
        }

        SmartSpaceProvider.setActiveSpace(null);
        if (MyXMPP.getINSTANCE() != null) {
            MyXMPP.getINSTANCE().leaveMuc();
        } else {
            log.warn("MyXMPP was already null before exiting");
        }

        //FIXME figure out if we need to unbind service
        if (myChatServiceConnection != null && myChatServiceConnection.isBound) {
            try {
                unbindService(myChatServiceConnection);
            } catch (IllegalArgumentException e) {
                //dirty hack, this exception may be thrown when service is not actually bound
                log.warn("IllegalArgumentException when trying to unbind service.", e);
            }
        }


        /*
        //Unbind service
        //First (try to bind again) to find out if it is bound
        //unbinding if it is not bound causes IllegalArgumentException
        boolean isServiceBound = bindService(new Intent(this, ChatService.class), myChatServiceConnection, Context.BIND_AUTO_CREATE);
        if (isServiceBound) {
            this.unbindService(myChatServiceConnection);
        }
        */

        //switch to OutOfRangeActivity if we just exited all beacon regions and if app is in foreground
        //if (beaconsInRange.isEmpty() && MainActivity.getINSTANCE() != null && MainActivity.getINSTANCE().isVisible()) {
        //if (ActivityManager.getVisibleActivity() != null) {
        //    ActivityManager.launchOutOfRangeActivity(ActivityManager.getVisibleActivity());
        //}
        if (ActivityManager.isAnyActivityVisible()) {
            ActivityManager.launchOutOfRangeActivity(this);
        }

        //Clear all notifications
        MyNotificationManager notificationManager = new MyNotificationManager(this);
        notificationManager.clearAllNotifications();

    }

    @Override
    public void onBeaconServiceConnect() {
        log.debug("onBeaconServiceConnected");

        configureBeaconManager();

        SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(this);
        boolean rangingEnabled = sharedPrefs.getBoolean(this.getString(R.string.pref_key_scanning_enabled), true);

        if (rangingEnabled) {

            //Possible bug.. Need to start ranging because onEnterRegion is not called when app is started for first time when beacon in range
            try {
                beaconManager.startRangingBeaconsInRegion(Config.BEACON_REGION);
            } catch (RemoteException e) {
                log.error("RemoteException: ", e);
            }
        }

    }

    /**
     * Change scanning settings of beacon manager
     * Apparently only works after binding and after onBeaconServiceConnect is called
     */
    public void configureBeaconManager() {

        log.debug("configureBeaconManager");

        final int scanPeriod = Preferences.getScanPeriod(this);
        final int betweenScanPeriod = Preferences.getBetweenScanPeriond(this);
        final int regionTimeOut = Preferences.getRegionExitTimeOut(this);
        final int backgroundScanPeriod = Preferences.getBackgroundScanPeriod(this);

        log.debug("scanPeriod: " + scanPeriod + " betweenScanPeriod: " + betweenScanPeriod + " regionTimeOut: " + regionTimeOut);

        beaconManager.setBackgroundMode(false);
        beaconManager.setBackgroundScanPeriod(backgroundScanPeriod);
        beaconManager.setForegroundScanPeriod(scanPeriod);
        beaconManager.setBackgroundBetweenScanPeriod(betweenScanPeriod);
        beaconManager.setForegroundBetweenScanPeriod(betweenScanPeriod);



        try {
            beaconManager.updateScanPeriods();
        } catch (RemoteException e) {
            log.debug("RemoteException: ", e);
        }
    }

    /**
     * Set background mode of beaconmanager to false and save setting in shared preferences
     */
    private void leaveBackgroundMode() {
        if (Preferences.isBackgroundModeEnabled(BeaconScannerApplication.this)) {
            PreferenceManager.getDefaultSharedPreferences(BeaconScannerApplication.this)
                    .edit()
                    .putBoolean(BeaconScannerApplication.this.getString(R.string.pref_key_background_mode), false)
                    .apply();

            //update beaconmanager
            beaconManager.setBackgroundMode(false);
        }
    }

    /**
     * Implementation of a RangeNotifier
     * didRangeBeaconsInRegion will be called by altbeacon library when a beacon has been ranged
     */
    private class MyRangingNotifier implements RangeNotifier {

        @Override
        public void didRangeBeaconsInRegion(Collection<Beacon> collection, Region region) {

            Collection<Beacon> filteredCollection = new ArrayList<>();
            ArrayList<String> ids2ignore = new ArrayList<>();
            //ids2ignore.add("F8F6E2A02C22".toLowerCase()); //ICE
            //ids2ignore.add("E45656D632D4".toLowerCase()); //MINT
            ids2ignore.add("F1CCD0B13F64".toLowerCase());
            ids2ignore.add("E62FF24AE68B".toLowerCase());
            ids2ignore.add("EF64A92EB2D8".toLowerCase());
            //ids2ignore.add("E23ADEB331B6".toLowerCase()); //BLUEBERRY

            for (Beacon beacon : collection) {
                String id = beacon.getId2().toString().substring(2).toLowerCase();
                if (ids2ignore.contains(id)) {
                    //log.info("Ignoring: " + id);
                    continue;
                } else {
                    filteredCollection.add(beacon);
                }
            }

            collection = filteredCollection;

            //Evaluator.addTotalCount();
            //Collect instance ids in seperate arrays
            ArrayList<String> idArray = new ArrayList<>();
            for (Beacon beacon : collection) {
                idArray.add(beacon.getId2().toString().substring(2));
            }
            log.debug("didRangeBeaconsInRegion: " + idArray.toString());


            /**
            //Hotfix sometimes there is a bug that notification is not removed, check this here
            if (collection.isEmpty() && SmartSpaceProvider.getActiveSmartSpace() == null) {
                if (System.currentTimeMillis() - SmartSpaceProvider.getActiveSmartSpace().getLastSeenTimeStamp() < 60000) {
                    forceExit();
                }
            }
             */

            //check conditions and return early if conditions are not met
            if (!Utils.isInternetConnected(BeaconScannerApplication.this)) {
                log.warn("Conditions not met. Return.");
                return;
            }

            double distance = 0;

            SmartSpace activeSpace = SmartSpaceProvider.getActiveSmartSpace();

            if (collection.isEmpty()) {
                //TODO what to do if no beacons were ranged?
                //TODO go to background mode if nothing was ranged for x time?

            } else if (collection.size() >= 1) {

                //leave backgroundmode (if applicable)
                leaveBackgroundMode();


                ArrayList<Beacon> sortedBeaconList = new ArrayList<>(collection);
                Collections.sort(sortedBeaconList, new Comparator<Beacon>() {
                    @Override
                    public int compare(Beacon lhs, Beacon rhs) {
                        return Double.compare(lhs.getDistance(), rhs.getDistance());
                    }
                });



                for (Beacon beacon : sortedBeaconList) {

                    //Remove "0x" from beacon instance id
                    final String scannedId = beacon.getId2().toHexString().substring(2);

                    /*
                    //Filter out unwanted ids here (for debugging)
                    ArrayList<String> ids2ignore = new ArrayList<>();
                    ids2ignore.add("e62ff24ae68b".toLowerCase());
                    ids2ignore.add("ef64a92eb2d8".toLowerCase());
                    if (ids2ignore.contains(scannedId.toLowerCase())) {
                        log.info("Ignoring: " + scannedId);
                        continue;
                    }
                    */

                    //Do not initiate anything if we are in another smart space
                    if (SmartSpaceProvider.getActiveSmartSpace() != null) {
                        //if scanSuccess id == active space id
                        if (activeSpace.getBeaconInstanceId().equals(scannedId)) {
                            distance = beacon.getDistance();

                            //update distance textview iff it has been set
                            /*
                            MainActivity mainActivity = MainActivity.getINSTANCE();
                            if (mainActivity != null && mainActivity.isVisible()) {
                                mainActivity.updateDistanceTextView(distance);
                            }
                            */

                            activeSpace.setLastSeenTimeStamp(System.currentTimeMillis());
                            //Evaluator.scanSuccess();
                        } else {
                            //scanSuccess beacon id different from current space id, do sth?
                            //return because the closest beacon is not the one from currently joined space, i.e. don't reset lastseen timestamp
                            //FIXME should still check if this beacon id corresponds to a proper beacon

                            //Evaluator.scanMissed();;

                            break;
                        }
                    } else {
                        boolean joinedSmartSpace = tryToJoinSmartSpace(scannedId);
                        if (joinedSmartSpace) {

                            //get distance
                            distance = beacon.getDistance();
                            //Evaluator.scanSuccess();

                            //Do not process further ids if we already joined a space
                            break;
                        }
                    }
                }

            }

            if (SmartSpaceProvider.getActiveSmartSpace() != null) {
                //update distance textview iff it has been set
                MainActivity mainActivity = MainActivity.getINSTANCE();
                if (mainActivity != null && MainActivity.isVisible()) {
                    mainActivity.updateDistanceTextView(distance);
                }
            }

        }
    }

    /**
     * Provides callbacks for any activity lifecycle events of any activity
     * Currently not used
     */
    private class MyActivityLifeCycleCallbacks implements ActivityLifecycleCallbacks {

        private Logger log;

        private MyActivityLifeCycleCallbacks() {
            log = ALogger.getLogger(BeaconScannerApplication.this, MyActivityLifeCycleCallbacks.class);
        }

        @Override
        public void onActivityCreated(Activity activity, Bundle savedInstanceState) {
            log.debug("onActivityCreated " + activity.getClass().getSimpleName());
        }

        @Override
        public void onActivityStarted(Activity activity) {
            log.debug("onActivityStarted " + activity.getClass().getSimpleName());
        }

        @Override
        public void onActivityResumed(Activity activity) {
            log.debug("onActivityResumed " + activity.getClass().getSimpleName());
        }

        @Override
        public void onActivityPaused(Activity activity) {
            log.debug("onActivityPaused " + activity.getClass().getSimpleName());
        }

        @Override
        public void onActivityStopped(Activity activity) {
            log.debug("onActivityStopped " + activity.getClass().getSimpleName());
        }

        @Override
        public void onActivitySaveInstanceState(Activity activity, Bundle outState) {
            log.debug("onActivitySaveInstanceState " + activity.getClass().getSimpleName());
        }

        @Override
        public void onActivityDestroyed(Activity activity) {
            log.debug("onActivityDestroyed " + activity.getClass().getSimpleName());
        }
    }

}
