package org.biver.smartcampus.core;

import org.biver.smartcampus.entities.SmartSpace;

/**
 * Provider of statice variables and application global configurations
 */
public final class SmartSpaceProvider {

    /**
     * Private constructor, class cannot be instantiated
     */
    private SmartSpaceProvider() {}

    private static boolean visible;

    public static boolean isVisible() {
        return visible;
    }

    /**
     * Reference to the currently active smart space
     */
    private static SmartSpace activeSpace;


    public static void setActiveSpace(SmartSpace space) {
        activeSpace = space;
    }

    public static SmartSpace getActiveSmartSpace() {
        return activeSpace;
    }


}
