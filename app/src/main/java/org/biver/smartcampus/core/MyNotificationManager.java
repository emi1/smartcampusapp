package org.biver.smartcampus.core;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;

import org.biver.smartcampus.R;
import org.biver.smartcampus.activities.MainActivity;
import org.biver.smartcampus.services.chat.ChatMessage;
import org.biver.smartcampus.utils.ALogger;
import org.biver.smartcampus.utils.Config;
import org.biver.smartcampus.utils.Preferences;
import org.apache.log4j.Logger;
import org.jxmpp.jid.EntityJid;

import java.io.File;

/**
 * All notifications by the app should be handled here
 * Settings if notifications are enabled are checked in this class
 * Ie other class calls methods and it is decided here if the notification is posted
 */
public class MyNotificationManager {

    private Logger log;

    private Context context;

    private NotificationManager notificationManager;

    //old icon: android.R.drawable.ic_notification_overlay


    private static final class NotificationIds {
        public static final int SPACE_NOTIFICATION = 234;
        public static final int GROUP_CHAT_MESSAGE = 235;
        public static final int FILE_DOWNLOAD = 236;
        public static final int FILE_TRANSFER_REQUEST = 237;
        public static final int FILE_TRANSFER_SUCCESS = 238;
        public static final int PERSONAL_MESSAGE_ID = 240;

    }

    private static final class IntentRequestCodes {
        public static final int SPACE = 234234;
        public static final int GROUP_CHAT = 2363345;
        public static final int FILE_DOWNLOAD = 234237;
    }

    public MyNotificationManager(Context context) {
        this.context = context;
        this.log = ALogger.getLogger(context, MyNotificationManager.class);
        this.notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
    }

    /**
     * Display notification about current smart space iff it is enabled in settings
     * @param currentSmartSpaceName Name of the smart space that is displayed in notification
     */
    public void notifySmartSpace(String currentSmartSpaceName) {
        notifySmartSpace(currentSmartSpaceName, false);
    }

    /**
     * Notification about currently joind smart space
     * @param currentSmartSpaceName Name of the smart space that is displayed in notification
     * @param forced force notification regardless of setting in preference
     *               This is required because we want to post a notification as soon as the user enables it in SettingsActivity
     *               However onPreferenceChanged in listener is called before new value is propagated to preferences
     *               Ie. would return early without forced value in this case
     */
    public void notifySmartSpace(String currentSmartSpaceName, boolean forced) {
        log.info("Notifying: " + currentSmartSpaceName);

        if (!forced && !Preferences.hasSmartSpaceNotifications(context)) {
            return;
        }

        String notificationTitle = context.getString(R.string.notification_title_active_smart_space);
        String notificationMessage = context.getString(R.string.notification_mesage_active_smart_space, currentSmartSpaceName);

        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context)
                        .setSmallIcon(Config.DEFAULT_ICON_ID)
                        .setOngoing(true)
                        .setContentTitle(notificationTitle)
                        .setContentText(notificationMessage);

        // Creates an explicit intent for an Activity in your app
        Intent resultIntent = new Intent(context, MainActivity.class);

        PendingIntent resultPendingIntent = PendingIntent.getActivity(context, IntentRequestCodes.SPACE, resultIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        mBuilder.setContentIntent(resultPendingIntent);

        Notification notification = mBuilder.build();

        // mId allows you to update the notification later on.
        notificationManager.notify(NotificationIds.SPACE_NOTIFICATION, notification);
    }

    /**
     * Notify about a personal message received
     * @param message
     */
    public void notifyPersonalChatMessage(ChatMessage message, EntityJid senderJid) {
        log.info("notifyPersonalChatMessage");

        if (!Preferences.hasPersonalChatNotifications(context)) {
            return;
        }

        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context)
                .setSmallIcon(Config.DEFAULT_ICON_ID)
                .setContentTitle(context.getString(R.string.text_new_personal_message_notification, message.getSender()))
                .setContentText(message.getBody());

        mBuilder = maybeAddNotificationSound(mBuilder);


        // Creates an explicit intent for an Activity in your app
        Intent resultIntent = new Intent(context, MainActivity.class);
        resultIntent.putExtra(Config.INTENT_EXTRA_FRAGMENT_TO_OPEN, Config.SERVICE_TYPES.PERSONAL_CHAT);
        resultIntent.putExtra("personal_chat_receiver", senderJid.toString());
        resultIntent.putExtra("personal_chat_receiver_nick", message.getSender());

        PendingIntent resultPendingIntent = PendingIntent.getActivity(context, IntentRequestCodes.GROUP_CHAT, resultIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        mBuilder.setContentIntent(resultPendingIntent);

        notificationManager.notify(NotificationIds.PERSONAL_MESSAGE_ID, mBuilder.build());
    }

    /**
     * Notify about a new message in the public group chat of the smart space
     * @param groupChatMessage
     */
    public void notifyGroupChatMessage(ChatMessage groupChatMessage) {
        log.info("notifyGroupChatMessage");

        if (!Preferences.hasGroupChatNotifications(context)) {
            return;
        }

        final String spaceName = SmartSpaceProvider.getActiveSmartSpace().getName();
        final String notificationTitle = context.getString(R.string.text_new_message_notification, spaceName);

        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context)
                        .setSmallIcon(Config.DEFAULT_ICON_ID)
                        .setContentTitle(notificationTitle)
                        .setContentText(groupChatMessage.getBody());

        mBuilder = maybeAddNotificationSound(mBuilder);


        // Creates an explicit intent for an Activity in your app
        Intent resultIntent = new Intent(context, MainActivity.class);
        resultIntent.putExtra(Config.INTENT_EXTRA_FRAGMENT_TO_OPEN, Config.SERVICE_TYPES.CHAT);

        PendingIntent resultPendingIntent = PendingIntent.getActivity(context, IntentRequestCodes.GROUP_CHAT, resultIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        mBuilder.setContentIntent(resultPendingIntent);

        notificationManager.notify(NotificationIds.GROUP_CHAT_MESSAGE, mBuilder.build());
    }

    /**
     * Notify about incoming file transfer request
     */
    public void notifyIncomingFileTransferRequest() {
        log.info("notifyIncomingFileTransferRequest");

        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context)
                .setSmallIcon(Config.DEFAULT_ICON_ID)
                .setContentTitle("FILE TRANSFER REQUEST");

        //Incoming file transfer requests are high priority and should always have sound
        mBuilder = addNotificationSound(mBuilder);


        // Creates an explicit intent for an Activity in your app
        Intent resultIntent = new Intent(context, MainActivity.class);
        PendingIntent resultPendingIntent = PendingIntent.getActivity(context, IntentRequestCodes.GROUP_CHAT, resultIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        mBuilder.setContentIntent(resultPendingIntent);

        notificationManager.notify(NotificationIds.FILE_TRANSFER_REQUEST, mBuilder.build());
    }

    /**
     * Notification that file was successfully downloaded
     * @param downloadedFile The file that was downloaded
     *                       The location of the downloaded file should be in notification
     */
    public void notifyFileTransferSuccess(Context context, File downloadedFile) {
        log.info("notifyFileTransferSuccess");


        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context)
                .setSmallIcon(Config.DEFAULT_ICON_ID)
                .setContentTitle("Download successful: " + downloadedFile.getName())
                .setContentText(downloadedFile.getPath());

        mBuilder = maybeAddNotificationSound(mBuilder);

        Intent intent = new Intent();
        intent.setAction(android.content.Intent.ACTION_VIEW);
        intent.setDataAndType(Uri.fromFile(downloadedFile), "*/*");

        PendingIntent pIntent = PendingIntent.getActivity(context, IntentRequestCodes.FILE_DOWNLOAD, intent, PendingIntent.FLAG_UPDATE_CURRENT);

        mBuilder.setContentIntent(pIntent);

        notificationManager.notify(NotificationIds.FILE_TRANSFER_SUCCESS, mBuilder.build());
    }

    public void clearGroupChatMessageNotifications() {
        notificationManager.cancel(NotificationIds.GROUP_CHAT_MESSAGE);
    }

    public void clearPersonalChatNotifications() {
        notificationManager.cancel(NotificationIds.PERSONAL_MESSAGE_ID);
    }

    public void clearSmartSpaceNotification() {
        notificationManager.cancel(NotificationIds.SPACE_NOTIFICATION);
    }

    public void clearFileTransferRequstNotification() {
        notificationManager.cancel(NotificationIds.FILE_TRANSFER_REQUEST);
    }

    public void clearFileTransferSuccessNotification() {
        notificationManager.cancel(NotificationIds.FILE_TRANSFER_SUCCESS);
    }

    public void clearAllNotifications() {
        log.debug("clearAllNotifications");
        clearGroupChatMessageNotifications();
        clearSmartSpaceNotification();
        clearFileTransferRequstNotification();
        clearFileTransferSuccessNotification();
    }

    /**
     * Check if we need to add the notification sound and add it if necessary
     * @param builder the notificationbuilder
     * @return the modified notificationbuilder
     */
    private NotificationCompat.Builder maybeAddNotificationSound(NotificationCompat.Builder builder) {
        //Add notification sound unless it is disabled in settings
        if (!Preferences.areNotificationsSilent(context)) {
            return addNotificationSound(builder);
        } else {
            return builder;
        }
    }

    /**
     * Add the sound to the notification, iff this is enabled in the shared preferences
     * @param builder the notificationbuilder
     * @return the modified notificationbuilder
     */
    private NotificationCompat.Builder addNotificationSound(NotificationCompat.Builder builder) {
        Uri alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        builder.setSound(alarmSound);
        return builder;
    }

}
