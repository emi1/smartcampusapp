package org.biver.smartcampus.entities;

import org.biver.smartcampus.utils.Config;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by emi on 9/26/16.
 */

public class UserService implements CampusServiceInterface {

    private String url = "";
    private String name = "";

    private UserService(String url, String name) {
        this.url = url;
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }


    public static UserService fromJson(JSONObject jsonObject) throws JSONException {
        String name = jsonObject.keys().next();
        String url = jsonObject.getJSONObject(name).getString(Config.JSON_PROPERTIES.url);

        return new UserService(url, name);
    }
}
