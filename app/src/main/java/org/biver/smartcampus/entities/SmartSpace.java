package org.biver.smartcampus.entities;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;

import org.biver.smartcampus.core.BeaconScannerApplication;
import org.biver.smartcampus.utils.ALogger;
import org.biver.smartcampus.utils.Config;
import org.biver.smartcampus.utils.SmartCampusLogger;
import org.biver.smartcampus.utils.Preferences;
import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Timer;

/**
 * Abstraction of a smart space
 */
public class SmartSpace {

    private String name;
    private String identifier;
    private String beaconInstanceId;
    private ArrayList<CampusServiceInterface> services;

    private long lastSeen;

    private Logger log;

    /**
     * Timer used for the timeout for leaving this smart space
     */
    private Timer timer;

    /**
     * Handler to post delayed Runnables to exit SmartSpace after timeout
     */
    private Handler handler = new Handler(Looper.getMainLooper());


    /**
     * Private constructor, use createFromJson instead
     */
    private SmartSpace(Context context) {
        log = ALogger.getLogger(context, this.getClass().getName());
        timer = new Timer();
    }

    /**
     * Create a SmartSpace instance from a JsonObject (received from server)
     * @param jsonObject
     * @return
     * @throws JSONException
     */
    public static SmartSpace createFromJson(Context context, final JSONObject jsonObject) throws JSONException{

        SmartSpace space = new SmartSpace(context);

        space.identifier = jsonObject.getString(Config.JSON_PROPERTIES.identifier);
        space.name = jsonObject.getString(Config.JSON_PROPERTIES.readable_name);
        space.beaconInstanceId = jsonObject.getString(Config.JSON_PROPERTIES.instance_id);

        JSONArray jsonServices = jsonObject.getJSONArray(Config.JSON_PROPERTIES.services);
        space.services = new ArrayList<>();
        for (int i = 0; i< jsonServices.length(); i++) {
            space.services.add(CampusProximityService.createFromJSONObject(jsonServices.getJSONObject(i)));
        }

        //set timestamp
        space.setLastSeenTimeStamp(System.currentTimeMillis());
        return space;
    }

    /**
     * Get human readable name of space
     * @return
     */
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    /**
     * Get identifier of space
     * @return
     */
    public String getIdentifier() {
        return identifier;
    }

    public String getBeaconInstanceId() {
        return beaconInstanceId;
    }


    /**
     * Sets time the beacon id from this space has last been received
     * @param timeStamp last seen timestamp
     */
    public void setLastSeenTimeStamp(final long timeStamp) {
        SmartCampusLogger.log.debug("reset last seen timestamp " + timeStamp);

        lastSeen = timeStamp;

        final int timeOut = Preferences.getRegionExitTimeOut(BeaconScannerApplication.getINSTANCE());


        handler.removeCallbacks(exitSmartSpaceRunnable);
        handler.postDelayed(exitSmartSpaceRunnable, timeOut);

    }

    public void removeExitTimers() {
        handler.removeCallbacks(exitSmartSpaceRunnable);
    }

    private Runnable exitSmartSpaceRunnable = new Runnable() {
        @Override
        public void run() {
            final int timeOut = Preferences.getRegionExitTimeOut(BeaconScannerApplication.getINSTANCE());
            final long currentTime = System.currentTimeMillis();
            if (currentTime - SmartSpace.this.lastSeen > timeOut) {
                log.debug("forceExit after timeout.");
                BeaconScannerApplication.getINSTANCE().forceExit();
            } else {
                log.debug("Timer executed but SmartSpace should not be exited.");
            }
        }
    };

    /**
     * Get timestamp of when the beacon from this smart space was last seen
     * @return
     */
    public long getLastSeenTimeStamp() {
        return lastSeen;
    }

    public ArrayList<CampusServiceInterface> getServices() {
        return this.services;
    }

    public void setServices(ArrayList<CampusServiceInterface> services) {
        this.services = services;
    }

}
