package org.biver.smartcampus.entities;

import org.biver.smartcampus.utils.Config;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Abstraction of available localized services
 * Information services should use CampusProximityInformationService
 */
public class CampusProximityService implements CampusServiceInterface {

    private String name;

    /**
     * Protected constructor, user createFromJSONObject instead
     */
    protected CampusProximityService(String name) {
        this.name = name;
    }

    /**
     * Creates a CampusProximityServcie instance from JsonObject retrieved from server
     * @param jsonObject
     * @return
     * @throws JSONException
     */
    public static CampusServiceInterface createFromJSONObject(JSONObject jsonObject) throws JSONException {

        CampusServiceInterface result;

        //Namae of the service is equivalent to the first key of jsonObject
        String name = jsonObject.keys().next();

        JSONObject innerJsonObject = jsonObject.optJSONObject(name);

        //Distinguish services that rely on an web url to work
        if (innerJsonObject.has(Config.JSON_PROPERTIES.url)) {
        //if (name.equals(Config.SERVICE_TYPES.INFORMATION) || name.equals(Config.SERVICE_TYPES.MENSA_MENU)) {
            String url = innerJsonObject.getString(Config.JSON_PROPERTIES.url);

            //WIP not tested
            boolean userGenerated = (innerJsonObject.has(Config.JSON_PROPERTIES.user_generated) && innerJsonObject.getBoolean(Config.JSON_PROPERTIES.user_generated));
            result = new CampusProximityInformationService(url, name, userGenerated);
        } else {
            result = new CampusProximityService(name);
        }

        //result.setName(name);

        return result;
    }

    /**
     * Gets a human readable name of this service
     * @return
     */
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
