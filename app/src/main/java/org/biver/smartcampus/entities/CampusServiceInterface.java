package org.biver.smartcampus.entities;

/**
 * Created by emi on 9/22/16.
 */

public interface CampusServiceInterface {

    public String getName();

    public void setName(String name);
}
