package org.biver.smartcampus.entities;

import com.google.gson.Gson;

import org.biver.smartcampus.utils.Config;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Abstraction of information service
 */
public class CampusProximityInformationService implements CampusServiceInterface {

    /**
     * Url that leads to the web presence where information is displayed and maitained
     */
    private String url;

    private String name;

    private boolean userGenerated;

    /**Constructor to create a CampusProximityInformationService
     * @param url url pointing to web address that should be used
     */
    public CampusProximityInformationService(String url, String name, boolean userGenerated) {
        this.url = url;
        this.name = name;
        this.userGenerated = userGenerated;
    }

    /**
     * Gets the url of information service
     * @return The full url in String format
     */
    public String getUrl() {
        return url;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "CampusProximityInformationService: " + "name: " + this.getName() + " url: " + this.getUrl();
    }


    public static CampusProximityInformationService fromGson(final String gsonString) {
        return new Gson().fromJson(gsonString, CampusProximityInformationService.class);

    }

    /**
     * Returns a JSONObject containing all the attributes of this element
     * @return
     * @throws JSONException
     */
    public JSONObject toJSONObject() throws JSONException{

        JSONObject outer = new JSONObject();

        JSONObject jsonObject = new JSONObject();


        jsonObject.put(Config.JSON_PROPERTIES.url, this.url);
        outer.putOpt(name, jsonObject);

        return outer;
    }


    public boolean isUserGenerated() {
        return userGenerated;
    }
}
