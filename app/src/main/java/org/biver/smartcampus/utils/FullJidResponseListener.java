package org.biver.smartcampus.utils;

/**
 * Interface to be used to implement listener when requesting a full jid from another user
 */
public interface FullJidResponseListener {

    /**
     * This will be called when the response containing the full jid of the user has been received
     * @param fullJid The xmpp jid in String format
     */
    void onFullJIdReceived(String fullJid);
}
