package org.biver.smartcampus.utils;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.Settings;

import org.biver.smartcampus.R;
import org.biver.smartcampus.fragments.PersonalChatFragment;
import org.biver.smartcampus.services.chat.ChatUtils;
import org.biver.smartcampus.services.filespace.FileSpaceElement;
import org.jxmpp.jid.EntityFullJid;

import java.lang.reflect.Field;
import java.util.Calendar;

/**
 * Created by emi on 6/24/16.
 */
public class Utils {


    /**
     * Converts a Calendar.DAY_OF_WEEK to a readable string
     * @param dayOfWeek
     * @return
     */
    public static String getDayOfWeekString(int dayOfWeek) {
        String result = "";
        switch (dayOfWeek) {
            case (Calendar.MONDAY) : {
                result = "Monday";
                break;
            }
            case (Calendar.TUESDAY) : {
                result = "Tuesday";
                break;
            }
            case (Calendar.WEDNESDAY) : {
                result = "Wednesday";
                break;
            }
            case (Calendar.THURSDAY) : {
                result = "Thursday";
                break;
            }
            case (Calendar.FRIDAY) : {
                result = "Friday";
                break;
            }
            case (Calendar.SATURDAY) : {
                result = "Saturday";
                break;
            }
            case (Calendar.SUNDAY) : {
                result = "Sunday";
                break;
            }
            default:
                break;

        }
        return result;
    }

    /**
     * Check if we are connected to the internet
     * @param context
     * @return
     */
    public static boolean isInternetConnected(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return (activeNetworkInfo != null && activeNetworkInfo.isConnected());
    }

    /**
     * Check if bluetooth is enabled
     * @return
     */
    public static boolean isBluetoothOn() {
        return BluetoothAdapter.getDefaultAdapter().isEnabled();
    }

    /**
     * Check all conditions that need to be met for applications to do anything
     * @param context
     * @return
     */
    public static boolean checkConditions(Context context) {
        return isInternetConnected(context) && isBluetoothOn();
    }

    /**
     * Creates a random unique phone id that is associated with this particular phone and is used as id for the xmpp account
     * @param context
     * @return
     */
    public static String getUniquePhoneId(Context context) {
        /*
        String uniqueId = PreferenceManager.getDefaultSharedPreferences(context).getString("pref_key_unique_id", "");
        if (uniqueId.isEmpty()) {
            uniqueId = Integer.toString(new Random().nextInt(Integer.MAX_VALUE));
            PreferenceManager.getDefaultSharedPreferences(context)
                    .edit().putString("pref_key_unique_id", uniqueId)
                    .commit();
        }
        */
        return Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
    }

    /**
     * Checks if we (this phone and account) has the permissions to delete this FileSpaceElement
     * @param context
     * @param element
     * @return
     */
    public static boolean havePermissionToDelete(Context context, FileSpaceElement element) {
        //Can always delete if Config.IS_ADMIN is set to true
        if (Config.IS_ADMIN) {
            return true;
        }

        //This phone/account has permission to delte if the FileSpace element was created by this exact phone
        return Utils.getUniquePhoneId(context).equals(element.getAccountId());
    }


    /**
     * Launch a PersonalChatFragment
     * @param activity Activity context
     * @param fullJid Jid of the user that we want to create a personal chat fragment with
     */
    public static void launchPersonalChatFragment(Activity activity, String fullJid, String receiverNick) {
        Fragment fragment = new PersonalChatFragment();

        Bundle bundle = new Bundle();
        bundle.putString("receiver_nick", receiverNick);
        bundle.putString("receiver_jid", fullJid);
        fragment.setArguments(bundle);

        FragmentManager fm = activity.getFragmentManager();
        fm.beginTransaction()
                .replace(R.id.fragment_container, fragment)
                .addToBackStack(null)
                .commit();
    }

    /**
     * Overwrites the default chat nickname "testuser" to something else and unique
     * @param context
     */
    public static void setupInitialChatUserName(Context context) {
        final String defaultUserName = ChatUtils.getDefaultUserNameWithShortUniqueIdentifier(context);
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);

        String initialDoNotUse = context.getString(R.string.default_username_that_should_never_be_used);
        String resultValue = prefs.getString(context.getString(R.string.pref_key_xmpp_user_name), initialDoNotUse);

        //Overwrite the initialvalue with the default username, unless the user has changed it to something else
        if (resultValue.equals(initialDoNotUse)) {
            //If the chat user name is still the default value then put name in shared preferences
            prefs.edit()
                    .putString(context.getString(R.string.pref_key_xmpp_user_name), defaultUserName)
                    .commit();
        }

    }

    /**
     * Convert a constant value to the corresponding name of the constant via reflection
     * @param clazz The class to retrieve the constants from
     * @param constantStartsWith attribute name of constant should start with this String
     * @param constantValue the value of the constant
     * @return The name of the constant attribute as String
     * @throws IllegalAccessException
     */
    public static String getIntConstantName(final Class clazz, final String constantStartsWith, final int constantValue) throws IllegalAccessException {
        String result = "";
        for (Field f : clazz.getDeclaredFields()) {
            f.setAccessible(true);
            if (f.getName().startsWith(constantStartsWith)
                    &&  f.getType().getName().equals("int")
                    && f.getInt(null) == constantValue) {
                result = f.getName();
            }
        }
        return result;
    }

}
