package org.biver.smartcampus.utils;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.widget.Toast;

/**
 * Convenience class to display toasts to user
 */
public final class Toaster {

    private Toaster() {}

    /**
     * The same as using Toast.makeText(context, text, Toast.LENGTH_LONG).show()
     * but this makes sure that is it run in ui thread
     * @param context
     * @param text
     */
    public static void toast(final Context context, final String text) {
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(context, text, Toast.LENGTH_LONG).show();
            }
        });
    }

    /**
     * The same as using Toast.makeText(context, text, Toast.LENGTH_SHORT).show()
     * @param context
     * @param text
     */
    public static void toastShort(final Context context, final String text) {
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(context, text, Toast.LENGTH_SHORT).show();
            }
        });
    }
    
}
