package org.biver.smartcampus.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Environment;
import android.preference.PreferenceManager;

import org.biver.smartcampus.R;

import java.io.File;

/**
 * Easy access to preferences
 */
public class Preferences {

    /**
     * Private constructor, class should not be instantiated
     */
    private Preferences() {}

    /**
     * Should group chat notifications be displayed
     * @param context
     * @return
     */
    public static boolean hasGroupChatNotifications(Context context) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        return prefs.getBoolean(context.getString(R.string.pref_key_group_chat_notification), false);
    }

    /**
     * Check if we have personal chat message notifications enabled
     * @param context
     * @return
     */
    public static boolean hasPersonalChatNotifications(Context context) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        return prefs.getBoolean(context.getString(R.string.pref_key_personal_chat_notification), true);
    }

    /**
     * Should a (persistent) notification be shown when inside of a smart space
     * @param context
     * @return
     */
    public static boolean hasSmartSpaceNotifications(Context context) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        return prefs.getBoolean(context.getString(R.string.pref_key_smart_space_notification), true);
    }

    /**
     * Check if notifications should be silent or not
     * If not silent then a notification sound should be played
     * @param context
     * @return
     */
    public static boolean areNotificationsSilent(Context context) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        return prefs.getBoolean(context.getString(R.string.pref_key_silent_notifications), true);
    }

    /**
     * Gets the username defined by the user in settings
     * This is only a user name to be displayed to user, not the xmpp account name
     * If user has not modified the default value we return the phone model + some hash of the IMEI
     * @param context
     * @return
     */
    public static String getDisplayedChatUserName(Context context) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);

        String prefKeyUserName = context.getString(R.string.pref_key_xmpp_user_name);
        String defaultDoNotUse = context.getString(R.string.default_username_that_should_never_be_used);
        String resultValue = prefs.getString(prefKeyUserName, defaultDoNotUse);

        return resultValue;

    }

    /**
     * Get the base directory that should be used for any file the apps writes to the storage
     * @param context
     * @return
     */
    public static String getStorageDir(Context context) {

        final String dir = Environment.getExternalStorageDirectory() + "/" + Config.STORAGE_DIR + "/";

        //create directory if it doesn't exist
        File file = new File(dir);
        if (!file.exists()) {
            file.mkdirs();
        }
        return dir;

    }

    /**
     * Returns logfile name with absolute path as String
     * @param context
     * @return
     */
    public static String getLogFile(Context context) {
        return context.getExternalFilesDir(null) + "/" + Config.LOG_FILE_NAME;
    }

    /**
     * Check if the user enabled the distance indicator view
     * @param context
     * @return
     */
    public static boolean hasDistanceIndicator(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getBoolean(context.getString(R.string.pref_key_distance_indicator), false);
    }

    /**
     * Helper to get the chat history of the respective space
     * @return The smart space beacon id of the chat history that we want to get (widthout "0x" prefix)
     */
    public static String getChatHistoryKeyOfSpace(final String spaceBeaconId) {
        return "chat_history_" + spaceBeaconId;
    }

    /**
     * Get the key for the shared preferences of the personal user chat with one individual user
     * @param user The nickname (!) user that we want to retrieve the chat from
     * @return
     */
    public static String getChatHistoryKeyOfUserChat(final String user) {
        return "chat_hisory_" + user;
    }


    /**
     * Convenience method to get the region exit timeout from preferences
     * @param context
     * @return
     */
    public static int getRegionExitTimeOut(Context context) {
        return Integer.valueOf(
                PreferenceManager.getDefaultSharedPreferences(context)
                        .getString(context.getString(R.string.pref_key_region_exit_timeout),
                                context.getString(R.string.default_value_region_exit_timeout)));
    }

    /**
     * Convenience method to set the region exit timemout
     * @param context
     * @param newRegionExitTimeout
     */
    public static void setRegionExitTimeOut(Context context, final int newRegionExitTimeout) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        prefs.edit()
                .putString(context.getString(R.string.pref_key_region_exit_timeout), Integer.toString(newRegionExitTimeout))
                .apply();
    }

    /**
     * Convenience method to get the time between two scan periods from shared preferences
     * @param context
     * @return
     */
    public static int getBetweenScanPeriond(Context context) {
        return Integer.valueOf(
                PreferenceManager.getDefaultSharedPreferences(context)
                        .getString(context.getString(R.string.pref_key_between_scan_period),
                                context.getString(R.string.default_value_between_scan_period)));
    }

    public static void setBetweenScanPeriod(Context context, int newBetweenScanPeriod) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        prefs.edit()
                .putString(context.getString(R.string.pref_key_between_scan_period), Integer.toString(newBetweenScanPeriod))
                .apply();
    }

    /**
     * Convencience to get the length of one individual scan period
     * @param context
     * @return
     */
    public static int getScanPeriod(Context context) {
        return Integer.valueOf(
                PreferenceManager.getDefaultSharedPreferences(context)
                        .getString(context.getString(R.string.pref_key_scan_period),
                                context.getString(R.string.default_value_scan_period)));
    }

    public static void setScanPeriod(Context context, int newScanPeriod) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        prefs.edit()
                .putString(context.getString(R.string.pref_key_scan_period), Integer.toString(newScanPeriod))
                .apply();
    }

    /**
     * Convenience method to get the duration of scan period in background mode
     * @param context
     * @return
     */
    public static int getBackgroundScanPeriod(Context context) {
        return Integer.valueOf(
                PreferenceManager.getDefaultSharedPreferences(context)
                    .getString(context.getString(R.string.pref_key_background_scan_period),
                            context.getString(R.string.default_value_background_scan_period)));
    }

    /**
     * Convenience method to get the background between scan period from preferences
     * @param context
     * @return
     */
    public static int getBackgroundBetweenScanPeriod(Context context) {
        return Integer.valueOf(
                PreferenceManager.getDefaultSharedPreferences(context)
                        .getString(context.getString(R.string.pref_key_background_between_scan_period),
                                context.getString(R.string.default_value_background_between_scan_period)));
    }

    /**
     * Convenience method to check if background mode is enabled
     * @param context
     * @return
     */
    public static boolean isBackgroundModeEnabled(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context)
                .getBoolean(context.getString(R.string.pref_key_background_mode),
                        false);
    }

    public static boolean isRangingEnabled(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getBoolean(context.getString(R.string.pref_key_scanning_enabled), true);
    }
}
