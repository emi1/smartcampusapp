package org.biver.smartcampus.utils;

import android.app.DownloadManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.os.Handler;
import android.provider.Settings.Secure;

import org.biver.smartcampus.R;
import org.apache.http.HttpEntity;
import org.apache.http.ParseException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.util.EntityUtils;
import org.apache.log4j.Logger;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.security.MessageDigest;
import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Observable;
import java.util.Set;
import java.util.zip.CRC32;
import java.util.zip.Checksum;

/**
 * Class to automatically check for available updates on server
 * Used code from https://github.com/NDMAC/android-auto-updater-client but highly modified!
 */
public class AutoUpdateApk extends Observable {

    private  Logger log;


    /**
     *
     * @param ctx Application context
     * @param apiPath path for requests on server (without "/" at beginning or end)
     * @param server ip and port of server ie http://123.123.123.123:8080/
     */
    public AutoUpdateApk(Context ctx, String apiPath, String server) {
        setupVariables(ctx);
        this.server = server;
        this.apiPath = apiPath;

        try {
            PackageInfo packageInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), PackageManager.GET_PERMISSIONS);
            versionCode = packageInfo.versionCode;
        } catch (NameNotFoundException e) {
            log.error(e.getMessage(), e);
        }
    }

    // set name to display in notification popup (default = application label)
    //
    public static void setName(String name) {
        appName = name;
    }


    /**
     * Check for update of apk on server
     */
    public void checkUpdatesManually() {
        new CheckUpdateTask(true).execute();
    }

    public static final String AUTOUPDATE_CHECKING = "autoupdate_checking";
    public static final String AUTOUPDATE_NO_UPDATE = "autoupdate_no_update";
    public static final String AUTOUPDATE_GOT_UPDATE = "autoupdate_got_update";
    public static final String AUTOUPDATE_HAVE_UPDATE = "autoupdate_have_update";


    private final static String ANDROID_PACKAGE = "application/vnd.android.package-archive";

    protected final String server;
    protected final String apiPath;

    protected static Context context = null;
    protected static SharedPreferences preferences;
    private final static String LAST_UPDATE_KEY = "last_update";
    private static long last_update = 0;

    private static int versionCode; // as low as it gets
    private static String packageName;
    private static String appName;
    private static int device_id;

    public static final long MINUTES = 60 * 1000;
    public static final long HOURS = 60 * MINUTES;
    public static final long DAYS = 24 * HOURS;

    // 3-4 hours in dev.mode, 1-2 days for stable releases
    private long updateInterval = 3 * HOURS; // how often to check

    private static boolean mobile_updates = false; // download updates over wifi only

    private final static Handler updateHandler = new Handler();
    protected final static String UPDATE_FILE = "update_file";
    protected final static String SILENT_FAILED = "silent_failed";
    private final static String MD5_TIME = "md5_time";
    private final static String MD5_KEY = "md5";

    private static int NOTIFICATION_ID = 0xDEADBEEF;
    private static int NOTIFICATION_FLAGS = Notification.FLAG_AUTO_CANCEL | Notification.FLAG_NO_CLEAR;
    private static long WAKEUP_INTERVAL = 500;

    private void setupVariables(Context ctx) {

        log = ALogger.getLogger(ctx, AutoUpdateApk.class);

        context = ctx;

        packageName = context.getPackageName();
        preferences = context.getSharedPreferences(packageName + "_" + "AutoUpdateApk", Context.MODE_PRIVATE);
        device_id = crc32(Secure.getString(context.getContentResolver(), Secure.ANDROID_ID));
        last_update = preferences.getLong("last_update", 0);
        NOTIFICATION_ID += crc32(packageName);

        ApplicationInfo appinfo = context.getApplicationInfo();
        if (appinfo.labelRes != 0) {
            appName = context.getString(appinfo.labelRes);
        } else {
            log.warn("unable to find application label");
        }
        if (new File(appinfo.sourceDir).lastModified() > preferences.getLong(MD5_TIME, 0)) {
            SharedPreferences.Editor editor = preferences.edit();
            editor.putString(MD5_KEY, MD5Hex(appinfo.sourceDir));
            editor.putLong(MD5_TIME, System.currentTimeMillis());
            editor.apply();

            String update_file = preferences.getString(UPDATE_FILE, "");
            if (update_file.length() > 0) {
                if (new File(context.getFilesDir().getAbsolutePath() + update_file).delete()) {
                    preferences.edit().remove(UPDATE_FILE).remove(SILENT_FAILED).apply();
                }
            }
        }
        //maybeRaiseNotification();

        //context.registerReceiver(connectivity_receiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
    }


    private class CheckUpdateTask extends AsyncTask<Void, Void, String[]> {
        private DefaultHttpClient httpclient = new DefaultHttpClient();
        private HttpPost post;

        private List<String> retrieved = new LinkedList<String>();

        private boolean forced;

        public CheckUpdateTask(boolean forced) {
            if (server != null) {
                post = new HttpPost(server + apiPath);
            } else {
                post = new HttpPost(apiPath);
            }
            this.forced = forced;
        }


        protected void onPreExecute() {
            // show progress bar or something
            log.debug("onPreExecute");
        }

        protected String[] doInBackground(Void... v) {
            log.debug("doInBackground");
            long start = System.currentTimeMillis();

            HttpParams httpParameters = new BasicHttpParams();
            // set the timeout in milliseconds until a connection is established
            // the default value is zero, that means the timeout is not used
            int timeoutConnection = 3000;
            HttpConnectionParams.setConnectionTimeout(httpParameters, timeoutConnection);
            // set the default socket timeout (SO_TIMEOUT) in milliseconds
            // which is the timeout for waiting for data
            int timeoutSocket = 5000;
            HttpConnectionParams.setSoTimeout(httpParameters, timeoutSocket);

            httpclient.setParams(httpParameters);

            try {
                StringEntity params = new StringEntity("pkgname=" + packageName
                        + "&version=" + versionCode + "&md5="
                        + preferences.getString(MD5_KEY, "0") + "&id="
                        + String.format("%08x", device_id));
                post.setHeader("Content-Type", "application/x-www-form-urlencoded");
                post.setEntity(params);
                String response = EntityUtils.toString(httpclient.execute(post).getEntity(), "UTF-8");
                log.debug("got a reply from update server");
                String[] result = response.split("\n");
                log.debug(Arrays.toString(result));

                if (result.length > 1 && result[0].equalsIgnoreCase("have update")) {
                    if (!retrieved.contains(result[1])) {
                        synchronized (retrieved) {
                            if (!retrieved.contains(result[1])) {
                                retrieved.add(result[1]);
                                HttpGet get = new HttpGet(
                                        (server != null) ? server + result[1]
                                                : result[1]);
                                HttpEntity entity = httpclient.execute(get).getEntity();

                                log.debug("got a package from update server");
                                /*
                                if (entity.getContentType().getValue().equalsIgnoreCase(ANDROID_PACKAGE)) {
                                    String fname = result[1].substring(result[1].lastIndexOf('/') + 1) + ".apk";
                                    FileOutputStream fos = context.openFileOutput(fname, Context.MODE_WORLD_READABLE);
                                    entity.writeTo(fos);
                                    fos.close();
                                    result[1] = fname;
                                }
                                */

                                if (result.length > 2 && result[2] != null) {
                                    /*
                                    try {
                                        versionCode = Integer.parseInt(result[2]);
                                    } catch (NumberFormatException nfe) {
                                        log.error("Invalid version code", nfe);
                                    }
                                   */

                                    //TODO check if file exists
                                    int newVersionCode = Integer.parseInt(result[2]);

                                    //download file if version is more recent than versionCode of the currently installed app
                                    if (newVersionCode > versionCode) {

                                        //delete existing files in updates directory
                                        String apkDir = Environment.getExternalStorageDirectory().toString() + "/" + Config.STORAGE_DIR + "/updates/";
                                        File updatesDir = new File(apkDir);
                                        if (updatesDir.exists() && updatesDir.isDirectory()) {
                                            String[] children = updatesDir.list();
                                            for (int i = 0; i < children.length; i++) {
                                                new File(updatesDir, children[i]).delete();
                                            }
                                        }


                                        //download new file from server
                                        Uri uri = Uri.parse(Config.HOST + "apk_repo/" + "org.acis.smartcampus-" + newVersionCode + ".apk");
                                        DownloadManager.Request request = new DownloadManager.Request(uri);
                                        //Fixme create a folder for each instance id
                                        request.setDestinationInExternalPublicDir("/" + Config.STORAGE_DIR + "/updates/", "org.acis.smartcampus-" + newVersionCode + ".apk");
                                        request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);

                                        DownloadManager downloadManager = (DownloadManager) context.getSystemService(Context.DOWNLOAD_SERVICE);
                                        downloadManager.enqueue(request);
                                    }

                                }

                                //setChanged();
                                //notifyObservers(AUTOUPDATE_GOT_UPDATE);
                            }
                        }
                    }
                } else {
                    setChanged();
                    notifyObservers(AUTOUPDATE_NO_UPDATE);
                    log.debug("no update available");
                }
                return result;
            } catch (ParseException e) {
                log.error("ParseException:", e);
            } catch (IOException e) {
                log.error("IOException: ", e);
            } finally {
                httpclient.getConnectionManager().shutdown();
                long elapsed = System.currentTimeMillis() - start;
                log.debug("update check finished in " + elapsed + "ms");
            }
            return null;
        }

        protected void onPostExecute(String[] result) {
            log.debug("onPostExecute");
            // kill progress bar here
            if (result != null) {
                boolean haveUpdate = result[0].equalsIgnoreCase("have update");
                if (haveUpdate) {
                    preferences.edit()
                            .putString(UPDATE_FILE, result[1])
                            .apply();

                    String update_file_path = context.getFilesDir().getAbsolutePath()  + result[1];
                    preferences.edit()
                            .putString(MD5_KEY, MD5Hex(update_file_path))
                            .putLong(MD5_TIME, System.currentTimeMillis())
                            .apply();

                    if (forced) {
                        Toaster.toast(context, context.getString(R.string.toast_update_available));
                    }
                } else {
                    if (forced) {
                        Toaster.toast(context, context.getString(R.string.toast_no_update_available));
                    }
                }
                //maybeRaiseNotification();

            } else {
                log.debug("no reply from update server");
                if (forced) {
                    Toaster.toast(context, "Failed to connect to server");
                }
            }
        }
    }

    /**
     * Clear existing notification (if any)
     */
    public static void clearNotification(Context context) {
        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.cancel(NOTIFICATION_ID);
    }

    /**
     * Get md5 checksum of a file
     * @param filename Name of the file to get md5 hash from
     * @return md5 hash as String
     */
    private String MD5Hex(String filename) {
        final int BUFFER_SIZE = 8192;
        byte[] buf = new byte[BUFFER_SIZE];
        int length;
        try {
            FileInputStream fis = new FileInputStream(filename);
            BufferedInputStream bis = new BufferedInputStream(fis);
            MessageDigest md = java.security.MessageDigest.getInstance("MD5");
            while ((length = bis.read(buf)) != -1) {
                md.update(buf, 0, length);
            }
            bis.close();

            byte[] array = md.digest();
            StringBuffer sb = new StringBuffer();
            for (int i = 0; i < array.length; ++i) {
                sb.append(Integer.toHexString((array[i] & 0xFF) | 0x100)
                        .substring(1, 3));
            }
            log.debug("md5sum: " + sb.toString());
            return sb.toString();
        } catch (Exception e) {
            log.error("Exception: ", e);
        }
        return "md5bad";
    }

    private boolean haveInternetPermissions() {
        Set<String> required_perms = new HashSet<String>();
        required_perms.add("android.permission.INTERNET");
        required_perms.add("android.permission.ACCESS_WIFI_STATE");
        required_perms.add("android.permission.ACCESS_NETWORK_STATE");

        PackageManager pm = context.getPackageManager();
        String packageName = context.getPackageName();
        int flags = PackageManager.GET_PERMISSIONS;
        PackageInfo packageInfo = null;

        try {
            packageInfo = pm.getPackageInfo(packageName, flags);
            versionCode = packageInfo.versionCode;
        } catch (NameNotFoundException e) {
            log.error(e.getMessage(), e);
        }

        if (packageInfo.requestedPermissions != null) {
            for (String p : packageInfo.requestedPermissions) {
                // Log_v(TAG, "permission: " + p.toString());
                required_perms.remove(p);
            }
            if (required_perms.size() == 0) {
                return true; // permissions are in order
            }
            // something is missing
            for (String p : required_perms) {
                log.error("required permission missing: " + p);
            }
        }
        log.error("INTERNET/WIFI access required, but no permissions are found in Manifest.xml");
        return false;
    }

    private static int crc32(String str) {
        byte bytes[] = str.getBytes();
        Checksum checksum = new CRC32();
        checksum.update(bytes, 0, bytes.length);
        return (int) checksum.getValue();
    }

}
