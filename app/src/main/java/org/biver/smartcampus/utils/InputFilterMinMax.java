package org.biver.smartcampus.utils;

import android.text.InputFilter;
import android.text.Spanned;

/**
 * Inputfilter that can be used with TextView to only allow numbers between min and max values.
 */
public class InputFilterMinMax implements InputFilter {

    private int min, max;

    /**
     * Constructore
     * @param min The minimal value to allow.
     * @param max The maximum value to allow.
     */
    public InputFilterMinMax(int min, int max) {
        this.min = min;
        this.max = max;
    }

    @Override
    public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
        try {
            int input = Integer.parseInt(dest.toString() + source.toString());
            if (isInRange(min, max, input))
                return null;
        } catch (NumberFormatException nfe) {

        }
        return "";
    }

    private boolean isInRange(int a, int b, int c) {
        return b > a ? c >= a && c <= b : c >= b && c <= a;
    }
}