package org.biver.smartcampus.utils;

import android.content.Context;

import org.apache.log4j.Logger;

/**
 * Globally available logger.
 * Classes without their own loggers can use this
 */
public class SmartCampusLogger {
    public static Logger log;

    /**
     * Initialize the application wide logger with the application context
     * @param context
     */
    public static void init(Context context) {
        log = ALogger.getLogger(context, SmartCampusLogger.class);
    }

}
