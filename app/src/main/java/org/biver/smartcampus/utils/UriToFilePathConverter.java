package org.biver.smartcampus.utils;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.CursorLoader;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.provider.DocumentsContract;
import android.provider.MediaStore;

/**
 * Class to convert an android uri to an absolute file path.
 * This class exists because it is unnecessarily complex to convert an uri received in onActivityResult of Activity into an absolute filepath.
 */
public class UriToFilePathConverter {

    private UriToFilePathConverter() {}

    public static String getFilePathFromUri(Context context, Uri uri) {
        String result = "";
        result = getFilePathUsingDocumentsContract(context, uri);
        if (result.isEmpty()) {
            result = getPathFromUri(context, uri);
        }
        return result;
    }

    /**
     * Should transform an Uri into a full path as string, unfortunately doesn't work completely
     * Stolen from stackoverflow
     * @param uri
     * @return
     */
    private static String getPathFromUri(Context context, Uri uri) {

        String result = "";

        String path = null;
        String[] projection = { MediaStore.Files.FileColumns.DATA };
        Cursor cursor = context.getContentResolver().query(uri, projection, null, null, null);

        if (cursor == null) {
            path = uri.getPath();
        } else {
            cursor.moveToFirst();
            int column_index = cursor.getColumnIndexOrThrow(projection[0]);
            path = cursor.getString(column_index);
            cursor.close();
        }

        if (path == null || path.isEmpty()) {
            result = uri.getPath();
        } else {
            result = path;
        }
        //if (result.contains(":")) {
        //    result = result.substring(result.indexOf(':') + 1, result.length());
        //    result = Environment.getExternalStorageDirectory() + "/" +  result;
        //}
        return result;
        //return ((path == null || path.isEmpty()) ? (uri.getPath()) : path);
    }

    /**
     * Stolen from stackoverflow
     * Seems to work better than above method but can not handle uris when !DocumentsContract.isDocumentUri(context, uri)
     * In this case return empty string and try other method
     * @param context
     * @param uri
     * @return
     */
    @TargetApi(Build.VERSION_CODES.KITKAT)
    private static String getFilePathUsingDocumentsContract(Context context, Uri uri) {
        int currentApiVersion;
        try {
            currentApiVersion = android.os.Build.VERSION.SDK_INT;
        } catch(NumberFormatException e) {
            //API 3 will crash if SDK_INT is called
            currentApiVersion = 3;
        }
        if (currentApiVersion >= Build.VERSION_CODES.KITKAT) {
            String filePath = "";
            if (!DocumentsContract.isDocumentUri(context, uri)) {
                return "";
            }
            String wholeID = DocumentsContract.getDocumentId(uri);
            // Split at colon, use second item in the array
            String id = wholeID.split(":")[1];

            String[] column = {MediaStore.Images.Media.DATA};

            // where id is equal to
            String sel = MediaStore.Images.Media._ID + "=?";

            Cursor cursor = context.getContentResolver().query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                    column, sel, new String[]{id}, null);

            int columnIndex = cursor.getColumnIndex(column[0]);

            if (cursor.moveToFirst()) {
                filePath = cursor.getString(columnIndex);
            }
            cursor.close();
            return filePath;
        } else if (currentApiVersion <= Build.VERSION_CODES.HONEYCOMB_MR2 && currentApiVersion >= Build.VERSION_CODES.HONEYCOMB) {
            String[] proj = {MediaStore.Images.Media.DATA};
            String result = null;

            CursorLoader cursorLoader = new CursorLoader(
                    context,
                    uri, proj, null, null, null);
            Cursor cursor = cursorLoader.loadInBackground();

            if (cursor != null) {
                int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
                cursor.moveToFirst();
                result = cursor.getString(column_index);
            }
            return result;
        }
        else {
            String[] proj = {MediaStore.Images.Media.DATA};
            Cursor cursor = context.getContentResolver().query(uri, proj, null, null, null);
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        }
    }

}
