package org.biver.smartcampus.utils;

import android.content.Context;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import java.io.File;

import de.mindpipe.android.logging.log4j.LogConfigurator;

/**
 * Class providing a properly configured instance of a log4j logger
 * Logs to file and logcat in current configuration
 */
public class ALogger {

    /**
     * Creates a logger that writes to text to ExternalFilesDir
     * @param clazz Class that the logger should be associated with
     * @param context Context is only needed to check for file directory on sd card
     * @return log4j logger
     */
    public static Logger getLogger(Context context, Class clazz) {

        configure(context);

        Logger log2 = Logger.getLogger(clazz);
        return log2;

    }

    public static Logger getLogger(Context context, String clazz) {

        configure(context);

        Logger log2 = Logger.getLogger(clazz);
        return log2;
    }

    private static void configure(Context context) {

        final LogConfigurator logConfigurator = new LogConfigurator();

        //Write to file iff it is possible to do so
        boolean writeToFile = true;
        //FIXME directory needs to be consistent with Preferences.getStorageDir() ?
        File externalFilesDir = context.getExternalFilesDir(null);
        if (externalFilesDir != null) {
            String logFile = externalFilesDir.toString() + "/" + Config.LOG_FILE_NAME;
            logConfigurator.setFileName(logFile);
        } else {
            writeToFile = false;
        }

        logConfigurator.setRootLevel(Level.ALL);
        logConfigurator.setLevel("org.apache", Level.ALL);
        logConfigurator.setUseFileAppender(writeToFile);
        logConfigurator.setFilePattern("%d %-5p [%c{1}]-[%L] %m%n");
        logConfigurator.setMaxFileSize(Config.MAX_LOG_FILE_SIZE);
        logConfigurator.setImmediateFlush(true);
        logConfigurator.configure();
    }

}