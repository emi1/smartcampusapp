package org.biver.smartcampus.utils;

import org.biver.smartcampus.R;
import org.altbeacon.beacon.Identifier;
import org.altbeacon.beacon.Region;

/**
 * Configuration parameters are kept as static variables
 */
public class Config {

    /**
     * Determine if this build is for evaluation/demo or developing/testing
     * In demo mode a different namespace for beacons should be used
     * In non-demo more settings can be displayed in SettingsActivity
     */
    public static final boolean IS_DEMO = false;

    /**
     * This should hide buttons and settings only relevant to the technical evaluation
     */
    public static final boolean HAS_TECHICAL_EVALUATION = true;

    /**
     * Gives this phone/account admin rights so that it can always execute certain operations
     * (Currently only operations like deleting element from filespace are effected)
     */
    public static final boolean IS_ADMIN = false;

    public static final String IP_ADDRESS = "35.156.27.212";
    public static final String PORT = "8081";
    public static final String HOST = "http://" + IP_ADDRESS + ":" + PORT + "/";

    //FIXME use proper password!
    public static final String DEFAULT_PASSWORD = "testpassword";
    //public static final String DEFAULT_PASSWORD = "Pf0WHYBjrfP";

    public static final int PERMISSION_REQUEST_COARSE_LOCATION = 11;
    public static final int PERMISSION_REQUEST_WRITE_EXTERNAL_STORAGE = 12;
    public static final int ASK_MULTIPLE_PERMISSION_REQUEST_CODE = 23;

    public static final int REQUEST_ENABLE_BT = 134;

    public static final int FILE_SELECT_CODE_FILE_SHARING = 111;
    public static final int FILE_SELECT_CODE_FILE_SPACE = 112;


    public static final String LOG_FILE_NAME = "smart_campus_log.log";
    public static final String STORAGE_DIR = "Smart Campus";

    //Interval to check for updates with AutoUpdateApk
    public static final long AUTO_UPDATE_INTERVAL = AutoUpdateApk.HOURS * 12;



    /**
     * Definition of own requests that can be send via XMPP
     */
    public static final class XMPP_REQUESTS {
        public static final String RESPONSE_FULL_JID = "RESPONSE_FULL_JID";
        public static final String REQUEST_FULL_JID = "REQUEST_FULL_JID";
    }


    /**
     * Max number of chat messages that will be stored to shared preferences
     */
    public static final int CHAT_HISTORY_MAX_SIZE = 100;

    /**
     * Beware that some of those are not in use!
     */
    public static final class SERVICE_TYPES {
        public static final String INFORMATION = "Information";
        public static final String MENSA_MENU = "Mensa Menu";
        public static final String CHAT = "Chat";
        public static final String SEND_FILES = "Send files";
        public static final String FILESPACE = "Virtual Notes";
        public static final String USERS = "Users";
        public static final String PERSONAL_CHAT = "Personal Chat";
        public static final String STATISTICS = "Statistics";
        public static final String SHARED_EDITING = "Shared Editing";
        public static final String USER_SERVICES = "User services";

    }

    public static final String CAMPUS_BASE_URL = "http://www.campus.rwth-aachen.de/rwth";

    public static final int MAX_LOG_FILE_SIZE = 1024 * 1024 * 10; //10mb

    public static final int DEFAULT_ICON_ID = R.drawable.university;


    public static final int INTENT_ID_TAKE_PICTURE = 7;
    public static final int INTENT_ID_RESTART = 123456;

    /**
     * The namespace id of beacons is hardcoded here
     * Use different namespace for testing and deployment
     */
    private static final String TESTING_NAMESPACE = "4CE74DF22CC47F8DDEE7";
    private static final String DEMO_NAMESPACE = "5b483f7e4ae0d54b5077";
    private static final String NAME_SPACE = (Config.IS_DEMO) ? Config.DEMO_NAMESPACE : Config.TESTING_NAMESPACE;

    /**
     * Identifier for beacon regions to pass to AltBeacon library
     */
    private static Identifier nameSpaceId = Identifier.parse("0x" + Config.NAME_SPACE);

    /**
     * The beacon region that is used throughout the whole application.
     */
    public static Region BEACON_REGION = new Region("SmartCampus", nameSpaceId, null, null);


    private static Identifier idBlueBerry = Identifier.parse("0x" + "e23adeb331b6");

    public static Region EVALUATION_REGION = new Region("Evaluation", nameSpaceId, idBlueBerry, null);

    /**
     * Default expiry time of a virtual note
     */
    public static final int DEFAULT_EXPIRE_TIME_MINUTES = 1440; //24h

    public static final String INTENT_EXTRA_CAMPUS_PROXIMITY_INFORMATION_SERVICE = "campus_proximity_information_service";
    public static final String INTENT_EXTRA_FRAGMENT_TO_OPEN = "fragment_to_open";

    public static final int MUC_HISTORY_SINCE_IN_SECONDS = 60 * 60 * 24 * 7; //one week in seconds


    /**
     * Keys for properties used in JsonObjects received or sent to server should be stored here to provide easy renaming/refactoring.
     */
    public static final class JSON_PROPERTIES {
        public static final String user_generated = "user_generated";
        public static final String url = "url";
        public static final String identifier = "identifier";
        public static final String readable_name = "readable_name";
        public static final String instance_id = "instance_id";
        public static final String services = "services";


    }

    public static final boolean HAS_TOPIC_BASED_MUC = false;
}
